var sum;
var phone_counter=0;
var address_counter=0;
$(document).ready(function()
{
    $(window).load(captcha_disp);
    $("#refresh").on("click", captcha_disp );
    $("#captcha_text").on("blur",validate_captcha);
    $("#submit_button").on("click",check_valid);
    $(document).on("blur",".input_field",function(){
                validate_all_fields($(this));});
    $("#add_phn").on("click",add_phn_numbers);
    $(document).on("click",".remove_phn",remove_phn_number);
    $("#add_address").on("click",add_more_address);
    $(document).on("click",".remove_address",remove_address);
    $("#remove_img").on("click",remove_img);
    $("#uploading_img").change(function(){read_img(this);});
})
// validing on submit button
function check_valid()
{   
    var errors=[];
    $(".input_field").each(function(){
       errors.push(validate_all_fields($(this)));
    });
    errors.push(validate_captcha());
    for(var index in errors)               //iterating error from array
    {
        if(errors[index]==false)
        {
            return false;                 //returning false if any error in input
        }
    }   
    alert("Registration successfull");
    display_datails();
    return false;
}
//validating all fields on blur and on submit
function validate_all_fields(element)
{  
    var name_regex=/^[a-zA-Z]+[']?[a-zA-Z]+$/;
    var email_regex=/^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$/;
    var pan_regex=/^([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
    var aadhaar_regex=/^[0-9]{4}[-][0-9]{4}[-][0-9]{4}$/;
    var pin_regex=/^[0-9]{6,10}$/;
    var phone_regex=/^[^0-1][0-9]{9}$/;
    var city_regex=/^[a-zA-Z][A-Za-z\s]{1,29}$/;
    var value= element.val().trim();
    if(element.val()=="" && element.attr('name')!="middle_name")
    {
        element.css("border","1px solid #FF0B0B");
        element.siblings('.err_msg').html(element.attr("placeholder")+" can not be empty");
        return false;
    }
    else if((element.attr("name")=="first_name" || element.attr('name')=="last_name") && 
                (name_regex.test(value)==false || value.length>10 ))                        //first name && middle name
    {
        return push_error(element);
    }
    else if(element.attr('name')=='email' && email_regex.test(value)==false)                //email
    {
        return push_error(element);
    }
    else if(element.attr('name')=='phone_number' && phone_regex.test(value)==false)         //phone
    {
        return push_error(element);
    }
    else if(element.attr('name')=='aadhaar' && aadhaar_regex.test(value)==false)            //aadhaar
    {
        return push_error(element);
    }
    else if(element.attr('name')=='pan_number' && pan_regex.test(value)==false)             //pan number
    {
        return push_error(element);
    }
    else if(element.attr('name')=='aadhaar' && value.length>50)                             //address
    {
        return push_error(element);
    }
    else if(element.attr('name')=='pincode' && pin_regex.test(value)==false)                //pincode
    {
        return push_error(element);
    }
    else if(element.attr('name')=='middle_name')                                             //middle name
    {
        return validate_middle_name(element);
    }
    else if(element.attr('name')=='city' && city_regex.test(value)==false)                                             //middle name
    {
        return push_error(element);
    }
    else
    {
        element.css("border","1px solid #90ee90")
        element.siblings('.err_msg').html("");
        return true;
    }
}
//pushing error in array
function push_error(element)
{
    element.css("border","1px solid #FF0B0B");
    element.siblings('.err_msg').html("Invalid input");
    return false;
}
//validate on middle name
function validate_middle_name(element)
{
    var name_regex=/^[a-zA-Z]+[']?[a-zA-Z]+$/;
    var value=element.val().trim();
    if(value!="" && name_regex.test(value)==false)
    {
        return push_error(element);
    }
    else 
    {
        element.siblings('.err_msg').html("");
        return true;
    }
}
//adding more phone number 
function add_phn_numbers()
{
    phone_counter++;
    var phone=$("#phone_div"+(phone_counter-1)).clone().attr("id","phone_div"+phone_counter);
    phone.children(".label").html("alternative phone ");
    phone.find(".add").replaceWith("<p class=\"remove_phn\">-</p>");
    phone.children(".input_field").val("");
    phone.children(".err_msg").html("");
    phone.addClass(".adding_element");      
    $("#phone_div"+(phone_counter-1)).after(phone);
}
//remove phone number
function remove_phn_number()
{
    $(this).parent('div').remove();
    phone_counter--;
}
//add more address
function add_more_address()
{ 
    address_counter++;
    var $add=$("#address"+(address_counter-1)).clone().attr("id","address"+address_counter).val("");
    $add.find(".add_address").replaceWith("<p class=\"remove_address\">-</p>");
    $add.find(".err_msg").html("");
    $add.find(".input_field").val("");
    $add.find(".state").html("<option value=''>---select---</option>")
    $add.find(".details").replaceWith("<span class=\"details\">Alternate Address</span>");
    $("#address"+(address_counter-1)).after($add);
}
//remove address
function remove_address()
{
    $(this).parent('div').remove();
    address_counter--;
}
//adding image
function read_img(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#upload_img").attr("src", e.target.result);
        }
        reader.readAsDataURL(input.files[0]);  
    }
}
//removing image
function remove_img()
{
    $("#upload_img").attr("src","images/profile.png");
    $("#uploading_img").val("");
}
//captcha
function captcha_disp()
{
    canvas_captcha.width = canvas_captcha.width;
    var captcha=$("#canvas_captcha");
    var context=captcha[0].getContext("2d");
    var operator=["+","-","*","/"];
    var operand1=Math.floor(Math.random() * 90)+10; 
    var operand2=Math.floor(Math.random() * 90)+10;
    var symbol=operator[Math.floor(Math.random()*4)];
    if(symbol=='/' && operand1%operand2 != 0)
    {
        symbol="+";
    }
    var equal="=";
    context.font = "40px Arial";
    context.fillText(operand1,85,80);
    context.fillText(symbol,150,80);
    context.fillText(operand2,200,80);
    context.fillText(equal,260,80);
    switch(symbol)
    {
        case "+": sum=operand1+operand2;
                    break;
        case "-": sum=operand1-operand2;
                    break;
        case "/": sum=operand1/operand2;
                    break;
        case "*": sum=operand1*operand2;    
    }
    return sum;
}
//validate captcha
function validate_captcha()
{
    var value=$("#captcha_text").val().trim();
    if(value=="")
    {
        $("#captcha_err").html("calculate and fill the captcha");
        return false;
    }
    else if(value!=sum)
    {
        $("#captcha_err").html("Wrong input");
        return false;
    }
    else if(value==sum){
        $("#captcha_err").html("");
        return true;
    }
}
//inserting country,city,state
country_name=[];
state_name={};
$(document).ready(function(){   
    
    var country_options;
    var state_options;
    $.getJSON('./Json/countries.json',function(result){
        $.each(result, function(i,country){
            country_name.push(country.name);
            country_options+="<option value='"+country.id+"'>"+country.name+"</option>";
        });
        $(".country").html(country_options);
    });
    
    $(document).on("change",".country",function(){
        var country_element=$(this);
        $.getJSON('./Json/states.json',function(result){
           
            state_name=[];
            state_options="";
            $.each(result, function(i,state)
            {
               state_name[state.id]=state.name;
            if(country_element.val()==state.country_id)
            {
                state_options+="<option value='"+state.id+"'>"+state.name+"</option>";
            }
            });
            if(state_options=="")
            {
                country_element.parent().siblings('div').find(".state").html("<option value=''>---select---</option>");
            }
            else
            {
                country_element.parent().siblings('div').find(".state").html(state_options);
            }
        });
    });
})
//displaying details
function display_datails()
{
    // $(".input_field").each(function(){
    //     $(this).prop("disabled", true);
    // });
    var date = Date().toString();
    $(".input_field").each(function()
    {
        var element =$(this);
        if(element.attr('name')=='first_name' ||element.attr('name')=='middle_name' || element.attr('name')=='last_name')
        {
            $("#name").append(element.val().trim()+" ");
        }
        else if(element.attr('name')=='email')
        {
            $("#email").append(element.val().trim()+" ");
        }
        else if (element.attr('name')=='pan_number') 
        {
            $("#pan_number").append(element.val().trim()+" ");
        }
        else if (element.attr('name')=='aadhaar') 
        {
            $("#aadhaar").append(element.val().trim()+" ");
        }
        else if (element.attr('name')=='phone_number') 
        {
            $("#phone").append(element.val().trim()+"<br><label class='labeladd'></label>");
        }
        else if (element.attr('name')=='address') 
        {
            $("#add_details").append(element.val().trim()+"<br><label class='labeladd'></label>");
        }
        else if(element.attr('name')=='city')
        {
            $("#add_details").append(element.val().trim()+"<br><label class='labeladd'></label>");
        }
        else if(element.attr('name')=='state')
        {
            $("#add_details").append(state_name[element.val()]+"<br><label class='labeladd'></label>");
        }
        else if(element.attr('name')=='country')
        {
            $("#add_details").append(country_name[element.val().trim()]+"<br><label class='labeladd'></label>");
        }
        else if(element.attr('name')=='pincode')
        {
            $("#add_details").append(element.val().trim()+"<br><br><label class='labeladd'></label>");
        }
    });
    $("#date").append(date);
    $("#emp_reg").addClass("hidden");
    $(".right_div").removeClass("hidden");
    $("#display_img").attr('src',$("#upload_img").attr('src'));
    $(".left_div").addClass("hidden");
    $(".dis_details").removeClass("hidden");
}

