$(document).ready(function()
{
    $("#button_submit").on("click", validate);
    $(document).on("blur",".input_field",function()
    {
        validate_fields($(this));
    })
    $(document).on("blur",".non_field",function()
    {
        non_mandatory($(this));
    })
    $(".dob").on("blur",validate_dob)
    $("#term").on("click", validate_term_condition);
    $("#user_reg input[name=gender]").on("click", validate_gender);
    $("#ans").on("blur", validate_captcha);
    $(window).load(captcha_disp);
    $("#refresh").on("click", captcha_disp );
    $(".dob").on("blur",validate_dob);
    $(".match").on("blur",validate_confirm_password);
});

function validate()
{ 
    var errors=[];
    $(".input_field").each(function(){
       errors.push(validate_fields($(this)));
    });
    $(".non_field").each(function(){
        errors.push(non_mandatory($(this)));
    })
    errors.push(validate_captcha());
    errors.push(validate_confirm_password());
    errors.push(validate_dob());
    errors.push(validate_term_condition());
    errors.push(validate_gender());
    for(var index in errors)               //iterating error from array
    {
        if(errors[index]==false)
        {
            return false;
        }
    }   
    alert("Registration successfull");
}
//validate all field on blur and click
function validate_fields(element)
{
    var name_regex=/^[a-zA-Z]+[']?[a-zA-Z]+$/;
    var email_regex=/^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$/;
    var phone_regex=/^[^0-1][0-9]{9}$/;
    var city_regex=/^[a-zA-Z][A-Za-z\s]{1,29}$/;
    var pass_regex=/^(?=.*[0-9])(?=.*[&*$#%@])[a-zA-z0-9&*$#%@]{8,15}$/;
    var value= element.val();
    if(value=="")
    {
        element.siblings('.err_msg').html(element.attr("placeholder")+" can not be empty");
        element.css("border","1px solid #FF0B0B");
        return false;
    }
    else if((element.attr('name')=="first_name" || element.attr('name')=="last_name")
        && (name_regex.test(value.trim())==false || value.length>10))
    {
        return errors_msg(element);
    }
    else if(element.attr('name')=="email" && email_regex.test(value.trim())==false)
    {
        return errors_msg(element);
    }
    else if(element.attr('name')=="password" && pass_regex.test(value)==false)
    {
        return errors_msg(element);
    }
    else if (element.attr('name')=="phone_number" && phone_regex.test(value.trim())==false) 
    {
        return errors_msg(element);
    }
    else if (element.attr('name')=="current_address" && value.length>50) 
    {
        return errors_msg(element);
    }
    else if (element.attr('name')=="current_city" && city_regex.test(value.trim())==false) 
    {
        return errors_msg(element);
    }
    else
    {
        element.css("border","1px solid #90ee90")
        element.siblings('.err_msg').html("");
        return true;
    }    

}
//displaying error messages
function errors_msg(element)
{
    element.css("border","1px solid #FF0B0B");
    element.siblings('.err_msg').html("Invalid input");
    return false;
}
//displaying captcha
function captcha_disp()
{
    canvas.width = canvas.width;
    var captcha=$("#canvas");
    var context=captcha[0].getContext("2d");
    var operator=["+","-","*","/"];
    var operand1=Math.floor(Math.random() * 90)+10; 
    var operand2=Math.floor(Math.random() * 90)+10;
    var symbol=operator[Math.floor(Math.random()*4)];
    if(symbol=='/' && operand1%operand2 != 0)
    {
        symbol="+";
    }
    var equal="=";
    context.font = "40px Arial";
    context.fillText(operand1,85,80);
    context.fillText(symbol,150,80);
    context.fillText(operand2,200,80);
    context.fillText(equal,260,80);
    switch(symbol)
    {
        case "+": sum=operand1+operand2;
                    break;
        case "-": sum=operand1-operand2;
                    break;
        case "/": sum=operand1/operand2;
                    break;
        case "*": sum=operand1*operand2;    
    }
    return sum;
}
//validating captcha
function validate_captcha()
{
    var value=$("#ans").val().trim();
    if(value=="")
    {
        $(".captcha").animate({left: '50px'},100);
        $(".captcha").animate({left: '0px'},100);
        $(".captcha").animate({left: '50px'},100);
        $(".captcha").animate({left: '0px'},100);
        $("#captcha_err").html("calculate and fill the captcha");
        return false;
    }
    else if(value!=sum)
    {
        $(".captcha").animate({left: '50px'},100);
        $(".captcha").animate({left: '0px'},100);
        $(".captcha").animate({left: '50px'},100);
        $(".captcha").animate({left: '0px'},100);
        $("#captcha_err").html("Wrong input");
        return false;
    }
    else if(value==sum){
        $("#captcha_err").html("");
        return true;
    }
}
function non_mandatory(element)
{
    var name_regex=/^[a-zA-Z]+[']?[a-zA-Z]+$/;
    var city_regex=/^[a-zA-Z][A-Za-z\s]{1,29}$/;
    var phone_regex=/^[^0-1][0-9]{9}$/;
    var value=element.val().trim();
    if(value=="")
    {
        element.siblings('.err_msg').html("");
        element.css("border","1px solid #90ee90");
        return true;
    }
    else if(element.attr('name')=="middle_name" && (name_regex.test(value)==false || value.length>10))
    {
        return errors_msg(element);
    }
    else if(element.attr('name')=='alternate_phone' && phone_regex.test(value)==false )
    {
        return errors_msg(element);
    }
    else if(element.attr('name')=='permanent_address' && value.length>50 )
    {
        return errors_msg(element);
    }
    else if(element.attr('name')=='permanent_city' && city_regex.test(value)==false )
    {
        return errors_msg(element);
    }
    else{
        element.css("border","1px solid #90ee90");
        element.siblings('.err_msg').html("");
        return true;
    }
}
function validate_confirm_password()
{
    var value=$(".match").val();
    if(value!=$("#user_reg input[name='password']").val())
    {
        $(".match").css("border","1px solid #FF0B0B");
        return errors_msg($(".match"))
    }
    else
    {
        $(".match").css("border","1px solid #90ee90");
        $(".match").siblings(".err_msg").html("");
        return true;
    }
}
function validate_term_condition()
{
    var term_condition=$("#term");
    if(term_condition.prop("checked")==false)
    {
        $("#term_err").html("Please agree");
        return false;
    }
    else
    {
        $("#term_err").html("");
        return true;
    }
}
function validate_gender()
{
    var gender=$("#user_reg input[name=gender]");
    if(gender[0].checked==false && gender[1].checked==false && gender[2].checked==false)
    {
        $("#gender_err").html("select your Gender");
        return false;
    }
    else
    {
        $("#gender_err").html("");
        return true;
    }
}
function validate_dob()
{    
    var date_of_birth=$(".dob").val();
    if(date_of_birth=="")
    {   
       return errors_msg($(".dob"));
    }
    else 
    {
        var data=date_of_birth.split("-");
        var d=new Date();
        var year=d.getFullYear();
        var month=d.getMonth()+1;
        var day=d.getDate();
        if(year<data[0])
        {
           return errors_msg($(".dob"));
        }
        else if(year>=data[0])
        {  
            if(year>data[0])
            {
                return valid_dob();
            }
            else
            {
                if(month>data[1])
                {
                    return valid_dob();
                }
                else if(month<=data[1])
                {
                    if(data[1]>month)
                    {
                        return errors_msg($(".dob"));
                    }
                    else
                    {
                        if(day>data[2])
                        {
                            return valid_dob();
                        }
                        else if(day<=data[2])
                        {
                            if(day<data[2])
                            {
                                return errors_msg($(".dob"));
                            }
                            else
                            {
                                return valid_dob();
                            }
                        }
                    }
                }
            }   
        }  
    }    
}

function valid_dob()
{
    $(".dob").css("border","1px solid #90ee90");
    $(".dob").siblings(".err_msg").html("");
    return true;
}