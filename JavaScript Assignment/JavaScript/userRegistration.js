var sum;
function captcha_disp()
{

    canvas.width = canvas.width;
    var captcha=document.getElementById("canvas");
    var context=captcha.getContext("2d");
    var operator=["+","-","*","/"];

    var operand1=Math.floor(Math.random() * 90)+10; 
    var operand2=Math.floor(Math.random() * 90)+10;
    var symbol=operator[Math.floor(Math.random()*4)];
    if(symbol=='/' && operand1%operand2 != 0)
    {
        symbol="+";
    }
    var equal="=";
    context.font = "40px Arial";
    context.fillText(operand1,85,80);
    context.fillText(symbol,150,80);
    context.fillText(operand2,200,80);
    context.fillText(equal,260,80);
    
    switch(symbol)
    {
        case "+": sum=operand1+operand2;
                    break;
        case "-": sum=operand1-operand2;
                    break;
        case "/": sum=operand1/operand2;
                    break;
        case "*": sum=operand1*operand2;
        
    }

    return sum;

}

function display_error(element,err_element,field_name)
{
    err_element.innerHTML=field_name+" can not empty";
    element.style.borderColor="#FF0B0B"; 
    return false;
}
function check_valid(element,err_element,field_name)
{
    err_element.innerHTML="Enter a valid "+field_name;
    element.style.borderColor= "#FF0B0B";
    return false;
}

function valid_success(element,err_element)
{
    err_element.innerHTML="";
    element.style.borderColor="#B0C4DE";
    return true;
}

   
var arr_function=[
    function first_name_valid()
    {
        
        var first_name=document.forms["registration"]["first_name"];
        var name_exp=/^[a-zA-Z]+$/;
        if(first_name.value.trim()=="")
        {
            return display_error(first_name,document.getElementById("first_name_err"),"First name");
        }
        else if(name_exp.test(first_name.value)==false || first_name.value.trim().length>10)
        {
            return check_valid(first_name,document.getElementById("first_name_err"),"first name");
        }
    
        else{
            return valid_success(first_name,document.getElementById("first_name_err"));
        }
    },
    
    
    function middle_name_valid()
    {
        var name_exp=/^[a-zA-Z]+$/;
        var middle_name=document.forms["registration"]["middle_name"];
        if(middle_name.value.trim() !="" && name_exp.test(middle_name.value)==false || middle_name.value.trim().length>10)
            {
                return check_valid(middle_name,document.getElementById("middle_name_err"),"middle name");
            }
            else
            {
               return valid_success(middle_name,document.getElementById("middle_name_err"));
            }
    },
    
    function last_name_valid()
    {
        var name_exp=/^[a-zA-Z]+$/;
        var last_name=document.forms["registration"]["last_name"];
        
        if(last_name.value.trim()=="")
        {
           return display_error(last_name,document.getElementById("last_name_err"),"Last name");
        }
        else if(name_exp.test(last_name.value)==false || last_name.value.trim().length>10)
        {
            return check_valid(last_name,document.getElementById("last_name_err"),"last name");
        }
        else
        {
            return valid_success(last_name,document.getElementById("last_name_err"));
        }
    },
    
    function email_valid()
    {
        var email_address=document.forms["registration"]["email_address"];
       
        var email_exp=/^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$/;   
        if(email_address.value.trim()=="")
        {
           return display_error(email_address,document.getElementById("email_err"),"Email id");
        }
        else if(email_exp.test(email_address.value)==false || email_address.value.trim().length>25)
        {
           return check_valid(email_address,document.getElementById("email_err"),"Email id");
        }
        else
        {
            return valid_success(email_address,document.getElementById("email_err"));
        }
    },
    
    function password_valid()
    {
        var password=document.forms["registration"]["password"];
       
        var pass_exp=/^(?=.*[0-9])(?=.*[&*$#%@])[a-zA-z0-9&*$#%@]{8,15}$/;
        if(password.value=="")
        {
           return display_error(password,document.getElementById("password_err"),"Password");
        }
       else if(pass_exp.test(password.value)==false)
        {
            return check_valid(password,document.getElementById("password_err"),"Password");
        }
        else
        {
            return valid_success(password,document.getElementById("password_err"));
        }
    },
    
    function confirm_password_valid()
    {
        var confirm_password=document.forms["registration"]["confirm_password"];
        
        if(confirm_password.value=="")
        {
            return display_error(confirm_password,document.getElementById("confirm_password_err"),"Confirm password");
        }
       else if(document.forms["registration"]["password"].value!=confirm_password.value)
        {
            return check_valid(confirm_password,document.getElementById("confirm_password_err"),"Confirm password");
        }
        else
        {
            return valid_success(confirm_password,document.getElementById("confirm_password_err"));
        }
    },
    
    function dob_valid()
    {    
       
        var date_of_birth=document.forms["registration"]["date_of_birth"];
        var dob_exp=/^(0?[1-9]|[12]?[0-9]|3?[0-1])[\/](0?[1-9]|1?[0-2])[\/]\d{4}$/;
        if(date_of_birth.value.trim()=="")
        {
            return display_error(date_of_birth,document.getElementById("dob_err"),"Dob");
        }
        else if(dob_exp.test(date_of_birth.value)==false)
        {
            return check_valid(date_of_birth,document.getElementById("dob_err"),"Dob");
        }  
        else if(dob_exp.test(date_of_birth.value))
         {
            var data=date_of_birth.value.split("/");
            var d=new Date();
            var year=d.getFullYear();
            var month=d.getMonth()+1;
            var day=d.getDate();
            if(year<data[2])
            {
                return check_valid(date_of_birth,document.getElementById("dob_err"),"Dob");
            }
            else if(year>=data[2])
            {
                if(year>data[2])
                {
                    return valid_success(date_of_birth,document.getElementById("dob_err"));
                }
                else
                {
                    if(month>data[1])
                    {
                        return valid_success(date_of_birth,document.getElementById("dob_err"));
                    }
                    else if(month<=data[1])
                    {
                        if(data[1]>month)
                        {
                            return check_valid(date_of_birth,document.getElementById("dob_err"),"Dob");
                        }
                        else
                        {
                            if(day>data[0])
                            {
                                return valid_success(date_of_birth,document.getElementById("dob_err"));
                            }
                            else if(day<=data[0])
                            {
                                if(day<data[0])
                                {
                                    return check_valid(date_of_birth,document.getElementById("dob_err"),"Dob");
                                }
                                else
                                {
                                    return valid_success(date_of_birth,document.getElementById("dob_err"));
                                }
                            }
                        }
                    }
                }
               
            }  
         }
       
    },
    
    function gender_valid()
    {
        var gender=document.forms["registration"]["gender"];
        if(gender[0].checked==false && gender[1].checked==false && gender[2].checked==false)
        {
            document.getElementById("gender_err").innerHTML="select your Gender*";
            return false;
        }
        else
        {
            document.getElementById("gender_err").innerHTML="";
            return true;
        }
    },

    function phone_number_valid()
    {
        var phone_number=document.forms["registration"]["phone_number"];
        var phn_exp=/^[^0-1][0-9]{9}$/;
        if(phone_number.value.trim()=="")
        {
            return display_error(phone_number,document.getElementById("phone_number_err"),"Phone number");
        }
        else if(phn_exp.test(phone_number.value)==false)
        {
            return check_valid(phone_number,document.getElementById("phone_number_err"),"Phone number");
        }
        else
        {
            return valid_success(phone_number,document.getElementById("phone_number_err"));
        }
    },
    
    function alternate_phone_valid()
    {
        
        var alternate_phone=document.forms["registration"]["alternate_phone"];
        var phn_exp=/^[^0-1][0-9]{9}$/;
        if(alternate_phone.value!="" && phn_exp.test(alternate_phone.value)==false)
        {
            return check_valid(alternate_phone,document.getElementById("alternate_phone_err"),"phone number");
        }
        else
        {
            return valid_success(alternate_phone,document.getElementById("alternate_phone_err"));
        }
    },
    
    function current_address_valid()
    {
        var current_address=document.forms["registration"]["current_address"];
        if(current_address.value.trim()=="" || current_address.value.trim().length>50)
        {
           return display_error(current_address,document.getElementById("current_address_err"),"Current address");
        }
        else
        {
            return valid_success(current_address,document.getElementById("current_address_err"));
        }
    
    },
    
    function current_city_valid()
    {   var city_exp=/^[a-zA-Z][A-Za-z\s]{1,29}$/;
        var current_city=document.forms["registration"]["current_city"];
        if(current_city.value.trim()=="")
        {
            return display_error(current_city,document.getElementById("current_city_err"),"Current city");
        }
        else if(city_exp.test(current_city.value.trim())==false ) 
        {
            return check_valid(current_city,document.getElementById("current_city_err"),"current city");
        }
        else
        {
            return valid_success(current_city,document.getElementById("current_city_err"));
        }
    
    },
    
    function current_state_valid()
    {
        var current_state=document.forms["registration"]["current_state"];
        if(current_state.value=="")
        {
            return display_error(current_state,document.getElementById("current_state_err"),"Current State");
        }
        else
        {
            document.getElementById("current_state_err").innerHTML="";
            current_state.style.borderColor="#B0C4DE";
            return true;
        }
    },

    function permanemt_city_valid()
    {
        var city_exp=/^[a-zA-Z][A-Za-z\s]{1,29}$/;
        var permanent_city=document.forms["registration"]["permanent_city"];
        if(permanent_city.value.trim()!="" && city_exp.test(permanent_city.value.trim())==false )
        {
            return check_valid(permanent_city,document.getElementById("permanent_city_err"),"city name");
        }
        else
        {
           return valid_success(permanent_city,document.getElementById("permanent_city_err"));
        }
    },


    function term_condition_valid()
    {
        var term_condition=document.forms["registration"]["term_condition"];
        if(term_condition.checked==false)
        {
            document.getElementById("term_err").innerHTML="Please check terms & conditions*";
            term_condition.style.borderColor="#FF0B0B";
            return false;
        }
        else
        {
            return valid_success(term_condition, document.getElementById("term_err"));
        }
    },
    
    function captcha_valid()
    {
        var ans_captcha=document.forms["registration"]["ans_captcha"];
        if(ans_captcha.value=="")
        {
         document.getElementById("captcha_err").innerHTML="calculate and enter the result*";
         ans_captcha.style.borderColor="#FF0B0B";
         return false;
        }
        else if(ans_captcha.value!=sum)
        {
            document.getElementById("captcha_err").innerHTML=" wrong input";
            return false;
        }
        else
        {
            return valid_success(ans_captcha,document.getElementById("captcha_err"));
        }
    },

    function permanemt_address()
    {
        var permanent_address=document.forms["registration"]["permanent_address"];
        if(permanent_address.value.trim()!="" && permanent_address.value.trim().length>50 )
        {
            return check_valid(permanent_address,document.getElementById("permanent_address_err"),"Address");
        }
        else
        {
           return valid_success(permanent_address,document.getElementById("permanent_address_err"));
        }
    }
    ];
    
    
    function validateUserRegistration()
    {
    var function_call=new Array();
     for( var index=0;index<arr_function.length;index++ )
     {
         function_call[index]=arr_function[index]().toString();
     }
    
        
        for(var index in function_call)
        {
             if(function_call[index]=="false")
                 {
                      return false;
               }
         }
       alert("Registration Successfull");

      
    }
      
