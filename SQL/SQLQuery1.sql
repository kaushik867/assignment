use DbPractice

/*1. Write a single sql query with following information's:

a. Total number number of employees
b. Minimum salary received by any employees.
c. Total distinct ntLevel*/

select count(*) 'No of Employee',min(moSalary) 'min salary',count(distinct ntLevel) 'total nthLevel'
from [dbo].[tblEmp];

--2. Correct this query:
 select E.[ntEmpID], E.[vcName],E.[vcMobieNumer]
 from tblEmp E;

/*3. Write a single select query which satisfies the following conditions:
    a. If any employee does not have a phone number then select that employee if ntLevel  equal to 1
    b. else select those employees whose ntLevel is equal to 0 */

select * from [dbo].[tblEmp] 
where (vcMobieNumer is null and ntLevel=1) OR ntLevel=0;

--4.  Write a sql query which displays those employee data first, who knows javascript.

select * from [dbo].[tblEmp]
order by(case
			when vcSkills like '%JavaScript%' then vcSkills
		end) desc;

--6. Write at least two possible solutions. 

SELECT [vcName],[vcMobieNumer] 
FROM [dbo].[tblEmp] 
GROUP BY [vcName],[vcMobieNumer];

select [vcName],max([vcMobieNumer])
FROM [dbo].[tblEmp] 
GROUP BY [vcName];

--7. Write a sql query to get the ntLevel of the employees getting salary greater than average salary.

select ntLevel 
from [dbo].[tblEmp] 
where moSalary>(select avg(moSalary) from [dbo].[tblEmp]);

--8. Write a query to get the count of employees with a valid Suffix 

select count(*) 'no of employee' 
from [Person].[Person]
where Suffix is not null;

--9. Using BusinessEntityAddress table (and other tables as required), list the full name of people living in the city of Frankfurt.

select  
 PER.FirstName+space(1)+ isnull(PER.MiddleName,space(1))+space(1)+PER.LastName 'fullName'
 from [Person].[Person] "PER" inner join [Person].[BusinessEntityAddress] "BEA"
 on PER.[BusinessEntityID]=BEA.[BusinessEntityID]
 inner join [Person].[Address] "ADD"
 on BEA.[AddressID]=[ADD].[AddressID]
 where [ADD].[City]='Frankfurt';

 select
 PER.FirstName+' '+ coalesce(PER.MiddleName+' ','')+PER.LastName 'fullName'
 from [Person].[Person] "PER" inner join [Person].[BusinessEntityAddress] "BEA"
 on PER.[BusinessEntityID]=BEA.[BusinessEntityID]
 inner join [Person].[Address] "ADD"
 on BEA.[AddressID]=[ADD].[AddressID]
 where [ADD].[City]='Frankfurt';

--10. "Single Item Order" is a customer order where only one item is ordered. Show the SalesOrderID and the UnitPrice for every Single Item Order.

select [SalesOrderID], [OrderQty], [ProductID]
from [Sales].[SalesOrderDetail]
where [SalesOrderID] In (
select SalesOrderID
from [Sales].[SalesOrderDetail]
group by SalesOrderID 
having Count(SalesOrderID) =1) and OrderQty =1;


--11. Show the product description for culture 'fr' for product with ProductID 736.
select PDES.Description
from [Production].[ProductDescription] "PDES" inner join
[Production].[ProductModelProductDescriptionCulture] "PMPD"
on PDES.ProductDescriptionID = PMPD.ProductDescriptionID inner join
[Production].[ProductModel] "PM"
on PMPD.ProductModelID = PM.ProductModelID inner join
[Production].[Product] "PROD"
on PM.ProductModelID = PROD.ProductModelID where
PMPD.cultureID = 'fr'
and PROD.ProductID = '736';

--12. Show OrderQty, the Name and the ListPrice of the order made by CustomerID 635

select SOD.OrderQty ,PROD.ListPrice ,PROD.Name
from [Sales].[SalesOrderDetail] "SOD"
inner join [Sales].[SalesOrderHeader] "SOH"
on SOD.SalesOrderID = SOH.SalesOrderID
inner join [Production].[Product] "PROD"
on PROD.ProductID = SOD.ProductID
where CustomerID = 635;

--13. How many products in ProductSubCategory 'Cranksets' have been sold to an address in 'London'?

select sum(SOD.OrderQty)
from [Production].[ProductSubcategory] "PSC"
inner join [Production].[Product] "PROD" on PSC.ProductSubcategoryID = PROD.ProductSubcategoryID
inner join [Sales].[SalesOrderDetail] "SOD" on PROD.ProductID = SOD.ProductID
inner join [Sales].[SalesOrderHeader] "SOH" on SOD.SalesOrderID = SOH.SalesOrderID
inner join [Person].[Address] "ADD" on SOH.ShipToAddressID = [ADD].AddressID
where [ADD].City = 'London' and PSC.Name = 'Cranksets';


select * from [Sales].[SalesOrderDetail]


select * from [Production].[ProductListPriceHistory]