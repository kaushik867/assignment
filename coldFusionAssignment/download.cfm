<!---
  --- download.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/15/20
  --->


<cfif !structKeyExists(session,'stLoggedInUser')>
	<cflocation url = "Index.cfm">
</cfif>
<cftry>
	<cfif structKeyExists(url,"assignmentId")>
	<cfset  VARIABLES.assignment = application.dbOperation.assignment(url.assignmentId)>
	<cfelse>
	<cfset  VARIABLES.assignment = application.dbOperation.submissionAsgn(url.submissionId)>
	</cfif>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>
<cfif structkeyExists(VARIABLES.assignment,'error') OR VARIABLES.assignment.recordCount EQ 0>
		<cf_header>
	<div class = "wrapper">
		<cfinclude template = "includes/sidebar.cfm" />
		<div class = "contentBox">
			<div class = "showCourse" >
				<cfif VARIABLES.assignment.recordCount EQ 0>
					<h2>
						Please try Again there file for download
					</h2>
				<cfelse>
					<h2>
						Something went wrong please try again later
					</h2>
				</cfif>
			</div>
		</div>
	</div>
<!---footer--->
<cfinclude template = "includes/footer.cfm" />
<cfelse>
	<cftry>
	<cfsetting enablecfoutputonly = "yes">
	 <!--- <cfheader  name="Content-Disposition" value="attachment;  filename=#Url.FileName#"> --->
	<cfcontent type =  "application/pdf" file = "#VARIABLES.assignment.filepath#">
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.TagContext[1].template# line no: #cfcatch.TagContext[1].line#">
		<cflocation url="missingTemplate.cfm">
	</cfcatch>
	</cftry>
</cfif>
