<!---
  --- review.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/30/20
  --->

<cfif !structKeyExists(session,'stLoggedInUser') OR !isUserInRole('student')>
	<cflocation url = "Index.cfm">
</cfif>
<cftry>
	<cfset  VARIABLES.allReview = application.dbOperation.review("name",session.stLoggedInUser.userId)>
	<cfif structKeyExists(form,'filterBtn') AND !structKeyExists(VARIABLES.allReview,'error')>
		<cfset  VARIABLES.allReview = application.dbOperation.review(form.selectItem,session.stLoggedInUser.userId)>
	</cfif>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>

<cfparam name = "pageNum" default="1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 6>
<cfset VARIABLES.startRow = min( ( pageNum-1 ) * VARIABLES.maxRows+1, max( VARIABLES.allReview.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.allReview.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.allReview.recordCount/6 )>

<!---header--->
<cf_header>
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<cfinclude template = "includes/welcome.cfm" />
	</div>
<div class = "wrapper">
	<cfinclude template= "includes/sidebar.cfm" />
		<div class = "contentBox">
</cfif>
<cfinclude template = "includes/filter.cfm">
		<div class = "showCourse" >
			<cfif VARIABLES.allReview.recordCount EQ 0>
				<h2> You did not have any review yet</h2>
			<cfelseif structKeyExists(VARIABLES.allReview,'error')>
				<h2> Something went wrong please try again later</h2>
			<cfelse>
				<cf_heading headingTitle = "REVIEWS">
				<div class = "panel panel-default">
					<div class = "panel-body" >
						<table class = "table table-striped">
							<thead>
								<tr>
									<th align = "left"> Course Name </th>
									<th align = "left"> Assignment Name </th>
									<th align = "left"> Updated By </th>
									<th align = "left"> Marks </th>
									<th align = "right" >Comment </th>
									<th> Created Time </th>
								</tr>
							</thead>
						<cfoutput query = "VARIABLES.allReview" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#">
							<tr>
								<td align = "left"> #left(VARIABLES.allReview.courseName,20)# </td>
								<td align = "left"> #left(VARIABLES.allReview.name,20)# </td>
								<td align = "left"> #VARIABLES.allReview.teaName# </td>
								<td align class="hidden"> #VARIABLES.allReview.statusId# </td>
								<td align = "left"> #VARIABLES.allReview.marks# </td>
								<td align = "left"> #left(VARIABLES.allReview.description,20)# </td>
								<td align = "left"> #dateFormat(VARIABLES.allReview.createdTime,'dd-mmm')# #timeFormat(VARIABLES.allReview.createdTime,'hh:MM')# </td>
								<td align="left"><a href="" class="reviewDetails" data-target = "##reviewDetails" data-toggle = "modal">More Details</a></td>
							</tr>
						</cfoutput>
						</table>
							<!-- end form-horizontal -->
					</div>
				</div>
			</cfif>
			<!-- end size -->

			<div class = "pagination">
				<cfoutput>
					<cfif url.pageNum GT "1" >
						<a href = "?pageNum=#url.pageNum-1#"><button type = "button" class = "btn btn-secondary"> <<--previous </button></a>
					</cfif>
					<cfif url.pageNum LT totalPages>
						<a href = "?pageNum=#url.pageNum+1#"><button type = "button" class = "btn btn-secondary"> Next--> </button></a>
					</cfif>
				</cfoutput>
			</div>
		</div>
		<cfif structKeyExists(session,'stLoggedInUser')>
		</div>
	</div>
</cfif>

<cfinclude template = "includes/reviewDetails.cfm" />

<cfinclude template = "includes/footer.cfm" />