<!---
  --- NewCFComponent
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/07/20
  --->
<cfcomponent output = "false" description = "This component is used to fetch query from database">

	<cffunction name = "userName" returntype = "query" output = "false"
		description = "this function is used to return query with userName present in database">
		<cfargument name = "inputUserName" type = "string" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.userNameExist">
			      SELECT userId, firstName, middleName, lastName, dateOfBirth, userName, PersonType, password, gender
			      FROM [dbo].[userTbl]
	              WHERE userName = <cfqueryparam value = "#arguments.inputUserName#" CFSQLType = "CF_SQL_VARCHAR" />
			 </cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.userNameExist = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.userNameExist,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.userNameExist />
	</cffunction>

	<cffunction name = "email" returntype = "query" output = "false"
		description = "this function is used to return query with Email present in database">
		<cfargument name = "inputEmail" type = "string" required = "true">
		<cftry>
			<cfquery name = "LOCAL.emailExist">
	            SELECT commValue
	            FROM [dbo].[communication]
	            WHERE commValue = <cfqueryparam value = "#arguments.inputEmail#" CFSQLType = "CF_SQL_VARCHAR" />
	        </cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.emailExist = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.emailExist,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.emailExist />
	</cffunction>


	<cffunction name = "password" returntype = "query" output = "false"
		description = "this function is used to return query with password present in database">
		<cfargument name = "inputUserName" type = "string" required = "true" />
		<cfargument name = "inputPassword" type = "string" required = "true" />
		<cftry>
			<cfquery name="LOCAL.passwordExist">
	            SELECT  userId, firstName, middleName, lastName, dateOfBirth, userName, PersonType, password, gender
	            FROM [dbo].[userTbl]
		            WHERE userName = <cfqueryparam value = "#arguments.inputUserName#" CFSQLType = "CF_SQL_VARCHAR" />
		            AND password = <cfqueryparam value = "#hash(arguments.inputPassword,'SHA-256','UTF-8')#" CFSQLType = "CF_SQL_VARCHAR" />
	        </cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.passwordExist = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.passwordExist,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.passwordExist />
	</cffunction>


	<cffunction name = "getEmail" returntype = "query" output = "false"
		description = "this function is used to return query with Email and type in database">
		<cfargument name = "userId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.email">
	            SELECT commValue,communicationTypeName
	            FROM [dbo].[communication] comm
				INNER JOIN [dbo].[CommunicationType] commType
				ON comm.communicationTypeId=  commType.communicationTypeId
	            WHERE userId = <cfqueryparam value = "#userId#" CFSQLType = "CF_SQL_BIGINT" />
	        </cfquery>
			<cfcatch>
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.email = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.email,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.email />
	</cffunction>


	<cffunction name = "courseName" returntype = "query" output = "false"
		description = "this function is used to return query with course name present in database">
		<cfargument name = "inputCourseName" type = "string" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.courseNameExist">
		            SELECT courseId, name, description, tagName, userId
		            FROM [dbo].[course]
		            WHERE name = <cfqueryparam value = "#trim(arguments.inputCourseName)#" CFSQLType = "CF_SQL_VARCHAR" />
		    </cfquery>
			<cfcatch>
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.courseNameExist = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.courseNameExist,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.courseNameExist />
	</cffunction>


	<cffunction name = "coursesDetails" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all courses from database">
		<cfargument name = "orderCol" type = "string" required = "false" default = "name" />
		<cfargument name = "id" type = "numeric" required = "false" />
		<cftry>
			<cfquery name = "LOCAL.allCourses">
					SELECT courseId, name, description, tagName, course.userId, createdTime, firstName, lastName
					FROM [dbo].[course] course
					INNER JOIN [dbo].[userTbl] userTbl
					ON course.userId = userTbl.userId
					<cfif structKeyExists(arguments,'id')>
					where courseId = <cfqueryparam value = "#arguments.id#"  CFSQLType = "CF_SQL_BIGINT" />
					</cfif>
					ORDER BY #arguments.orderCol#
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.allCourses = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.allCourses,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.allCourses />
	</cffunction>


	<cffunction name = "coursesDetailsSearch" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all courses from database">
		<cfargument name = "searchItem" type = "string" required = "true" />
		<cfset LOCAL.word = application.validationReg.removeStopWords(arguments.searchItem) />
		<cfset LOCAL.courses = coursesDetails("name",0) />
		<cftry>
			<cfloop list = #LOCAL.word# index = "words" delimiters = " ">
			<cfquery name = "LOCAL.allCourses">
					SELECT courseId, name, description, tagName, course.userId, createdTime, firstName, lastName
					FROM [dbo].[course] course
					INNER JOIN [dbo].[userTbl] userTbl
					ON course.userId = userTbl.userId
					WHERE (DIFFERENCE('#words#',name) = <cfqueryparam value = 4 >
					OR name LIKE <cfqueryparam value="%#words#%">)
					<cfif LOCAL.courses.recordCount NEQ 0>
					AND courseId NOT IN (<cfqueryparam value = "#LOCAL.myList#" list = "yes" >)
					</cfif>
				</cfquery>
				<cfloop from = "1" to = "#LOCAL.allCourses.recordCount#" index = "i" >
				<cfset queryAddRow(LOCAL.courses,queryGetRow(LOCAL.allCourses,i)) />
				</cfloop>
				<cfset LOCAL.myList = ValueList(LOCAL.courses.courseId,",")>
			</cfloop>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.allCourses = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.allCourses,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.courses />
	</cffunction>


	<cffunction name = "allCoursesDet" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all courses from database">
		<cfargument name = "id" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.allCourses">
					SELECT *
					FROM [dbo].[course]
					where courseId=<cfqueryparam value="#arguments.id#"  CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.allCourses = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.allCourses,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.allCourses />
	</cffunction>


	<cffunction name = "coursesDetailsByUser" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all courses from database">
		<cfargument name = "orderCol" type = "string" required = "false" default = "name" />
		<cfargument name = "id" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.allCoursesbyUser">
					SELECT courseId, name, description, tagName, course.userId, createdTime, firstName, lastName
					FROM [dbo].[course] course
					INNER JOIN [dbo].[userTbl] userTbl
					ON course.userId = userTbl.userId
					where course.userId = <cfqueryparam value = "#arguments.id#"  CFSQLType = "CF_SQL_BIGINT" />
					ORDER BY #arguments.orderCol#
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.allCoursesbyUser = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.allCoursesbyUser,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.allCoursesbyUser />
	</cffunction>


	<cffunction name = "remCoursesDetails" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all courses from database">
		<cfargument name = "id" type = "numeric" required = "true">
		<cfargument name = "orderCol" type = "string" required = "false" default = "name" />
		<cftry>
			<cfset LOCAL.enrollCourseId = application.dbOperation.enrolledCourses(arguments.id)>
			<cfset LOCAL.courseId =  ValueList(LOCAL.enrollCourseId.courseId,",")>
			<cfquery name = "LOCAL.courses">
						SELECT course.courseId, name, description, course.userId, createdTime,firstName,lastName
						FROM [dbo].[course] course
						INNER JOIN  [dbo].[userTbl] userTbl
						ON userTbl.userId = course.userId
						where course.courseId
						NOT IN (<cfqueryparam value = "#LOCAL.courseId#" list = "yes" />)
						ORDER BY #arguments.orderCol#
					</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.courses = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.courses,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn courses />
	</cffunction>


	<cffunction name = "remCoursesDetailsSearch" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all courses from database">
		<cfargument name = "searchItem" type = "string" required = "true" />
		<cfargument name = "id" type = "numeric" required = "true" />
		<cftry>
			<cfset LOCAL.word = application.validationReg.removeStopWords(arguments.searchItem) />
			<cfset LOCAL.courses = coursesDetails("name",0) />
			<cfset LOCAL.enrollCourseId = application.dbOperation.enrolledCourses(arguments.id) />
			<cfset LOCAL.courseId =  ValueList(LOCAL.enrollCourseId.courseId,",")> /
			<cfloop list = #LOCAL.word# index = "words" delimiters = " ">
			<cfquery name = "LOCAL.allCourses">
					SELECT courseId, name, description, tagName, course.userId, createdTime, firstName, lastName
					FROM [dbo].[course] course
					INNER JOIN [dbo].[userTbl] userTbl
					ON course.userId = userTbl.userId
					WHERE (DIFFERENCE('#words#',name) = <cfqueryparam value = 4 >
					OR name LIKE <cfqueryparam value="%#words#%">)
					AND courseId NOT IN (<cfqueryparam value = "#LOCAL.courseId#" list = "yes" />)
					<cfif LOCAL.courses.recordCount NEQ 0>
					AND courseId NOT IN (<cfqueryparam value = "#LOCAL.myList#" list = "yes" >)
					</cfif>
				</cfquery>
				<cfloop from = "1" to = "#LOCAL.allCourses.recordCount#" index = "i" >
				<cfset queryAddRow(LOCAL.courses,queryGetRow(LOCAL.allCourses,i)) />
				</cfloop>
				<cfset LOCAL.myList = ValueList(LOCAL.courses.courseId,",")>
			</cfloop>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.allCourses = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.allCourses,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.courses />
	</cffunction>


	<cffunction name = "assignmentDetails" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all assignment from database">
		<cfargument name = "id" type = "numeric" required = "false" />
		<cftry>
			<cfquery name = "LOCAL.myAssignment">
					SELECT   firstName, middleName, lastName, dateOfBirth,
							 userName, personType, gender,
							 assignmentId, course.name, course.description, filePath,
							 isActive, lastUpdate, startDate, endDate, createdDate,
							 course.courseId,
							 asgn.name assignmentName,asgn.description asgnDesc, tagName,  createdTime

					FROM [dbo].[userTbl] usertbl
					INNER JOIN [dbo].[course] course
					ON [userTbl].userId = [course].userId
					INNER JOIN [dbo].[assignment] asgn
					ON course.courseId = asgn.courseId
					WHERE asgn.courseId = <cfqueryparam value = "#arguments.id#"  CFSQLType = "CF_SQL_BIGINT" />
					ORDER BY createdDate DESC

				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.myAssignment = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.myAssignment,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.myAssignment />
	</cffunction>


	<cffunction name = "assignment" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all assignment from database">
		<cfargument name = "id" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.assignmentById">
					SELECT assignmentId, name, description, filePath, courseId, userId,
							isActive, lastUpdate, startDate, endDate, createdDate
					FROM [dbo].[assignment]
					WHERE assignmentId = <cfqueryparam value = "#arguments.id#"  CFSQLType = "CF_SQL_BIGINT" />
					AND isActive = <cfqueryparam value = 1 CFSQLType = "CF_SQL_BIT" />
			</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.assignmentById = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.assignmentById,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.assignmentById />
	</cffunction>


	<cffunction name = "enrolledCourses" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all enrolled courses from database">
		<cfargument name = "id" type = "numeric" required = "true">
		<cfargument name = "orderCol" type = "string" required = "false" default = "name" />
		<cftry>
			<cfquery name = "LOCAL.courses">
					SELECT course.courseId, name, description, tagName, course.userId, createdTime, firstName, lastName
					FROM [dbo].[course] course
					INNER JOIN  [dbo].[userTbl] userTbl
					ON userTbl.userId = course.userId
					INNER JOIN [dbo].[courseEnrollment] enroll
					ON course.courseId =enroll.courseId
					where enroll.userId =  <cfqueryparam value = "#arguments.id#"  CFSQLType = "CF_SQL_BIGINT" />
					AND IsActive = <cfqueryparam value = 1  CFSQLType = "CF_SQL_BIGINT" />
					ORDER BY #arguments.orderCol#
			</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.courses = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.courses,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.courses />
	</cffunction>


	<cffunction name = "enrollmentDetails" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all ernrollment from database">
		<cfargument name = "courseId" type = "numeric" required = "true" />
		<cfargument name = "userId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name="LOCAL.enrollment">
					SELECT enrollmentId, userId, courseId, enrollmentDate
					FROM [dbo].[courseEnrollment]
					WHERE userId = <cfqueryparam value = "#arguments.userId#"  CFSQLType = "CF_SQL_BIGINT" />
					AND courseId = <cfqueryparam value = "#arguments.courseId#"  CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.enrollment = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.enrollment,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.enrollment />
	</cffunction>


	<cffunction name = "getTeacherId" access = "public" returntype = "query" output = "false">
		<cfargument name = "submissionId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.teacherId">
					SELECT assgn.userId
					FROM [dbo].[submission] sub
					INNER JOIN  [dbo].[assignment] assgn
					ON assgn.assignmentId=sub.assignmentId
					WHERE submissionId= <cfqueryparam value = "#arguments.submissionId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.teacherId = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.teacherId,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.teacherId />
	</cffunction>


	<cffunction name = "updateStatus" returnformat = "json" access = 'remote' returntype = "struct" output = "false"
		description = "this function is used to cahnge status of notifiaction read and unread">
		<cfargument name = "historyId" type = "numeric" required = "true" />
		<cfset LOCAL.msg = structNew() />
		<cfset LOCAL.msg.isCommit =false>
		<cftry>
			<cfquery name="LOCAL.statusUpdate">
					UPDATE [dbo].[submissionStatusChange]
					SET status = <cfqueryparam value =1 CFSQLType = "CF_SQL_BIT" />
					WHERE historyId=<cfqueryparam value = "#arguments.historyId#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cfset LOCAL.msg.isCommit = true>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.statusUpdate = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.statusUpdate,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.msg />
	</cffunction>


	<cffunction name = "assignmentByUser" access = "public" returntype = "query" output = "false">
		<cfargument name = "orderBy" type = "string" required = "true" default = "name" />
		<cfargument name = "userId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.myAssignment">
					SELECT assignment.*, firstName, lastName, startDate createdTime
					FROM [dbo].[assignment]
					INNER JOIN [dbo].[userTbl]
					ON userTbl.userId=assignment.userId
					WHERE assignment.userId=<cfqueryparam value="#arguments.userId#" CFSQLType="CF_SQL_BIGINT" />
					ORDER BY #arguments.orderBy#
				</cfquery>
				<cfcatch>
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfset LOCAL.myAssignment = queryNew("error","bit") />
					<cfset queryAddRow(LOCAL.myAssignment,{error = 1}) />
				</cfcatch>
			</cftry>
		<cfreturn LOCAL.myAssignment />
	</cffunction>


	<cffunction name = "submitAssignment" access = "public" returntype = "query" output = "false">
		<cfargument name = "assignmentId" type = "numeric" default = "false" />
		<cftry>
			<cfquery name = "LOCAL.submittedAssignments">
					SELECT name,sub.assignmentId,userTbl.userId,SubmissionTime,firstName,lastName,submissionId,marks
			  		FROM [dbo].[submission] sub
					INNER JOIN [dbo].[courseEnrollment] courseEn
					ON sub.enrollmentId = courseEn.enrollmentId
					INNER JOIN [dbo].[userTbl] userTbl
					ON userTbl.userId = courseEn.userId
					INNER JOIN [dbo].[assignment] assgn
					ON sub.assignmentId = assgn.assignmentId
		 			where sub.assignmentId = <cfqueryparam value = "#arguments.assignmentId#" CFSQLType = "CF_SQL_BIGINT" />
		 			AND  assgn.userId = <cfqueryparam value = "#session.stLoggedInUser.userId#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.submittedAssignments = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.submittedAssignments,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.submittedAssignments />
	</cffunction>


	<cffunction name = "getStudentId" access = 'public' returntype = "query" output = "false"
		description = "this function is used to notify user that review is added ">
		<cfargument name = "submissionId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.studentId">
					SELECT userId
					FROM [dbo].[submission]
					INNER join [dbo].[courseEnrollment] enroll
					ON submission.enrollmentId = enroll.enrollmentId
					WHERE submission.submissionId = <cfqueryparam value = "#arguments.submissionId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.studentId = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.studentId,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.studentId />
	</cffunction>


	<cffunction name = "enrollStudent" access = 'public' returntype = "query" output = "false"
		description = "this function is used to notify a new assginment is added">
		<cfargument name = "courseId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.studentDetails">
					SELECT enroll.userId
					FROM [dbo].[courseEnrollment] enroll
					INNER JOIN [dbo].[course] course
					ON course.courseId = enroll.courseId
					where enroll.courseId = <cfqueryparam value = "#arguments.courseId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.studentDetails = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.studentDetails,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.studentDetails />
	</cffunction>


	<cffunction name = "review" access = 'public' returntype = "query" output = "false"
		description = "this function is used to return the all ernrollment from database">
		<cfargument name = "orderBy" type = "string" required = "true" default="name" />
		<cfargument name = "userId" type = "numeric" required = "true" />
		<cfargument name = "statusId" type = "numeric" required = "false" />
		<cftry>
			<cfquery name = "LOCAL.reviewData">
					select notify.statusId, course.name courseName, assgn.name name, firstName+' '+lastName teaName, marks,
					notify.description, notify.createdDate createdTime
					from [dbo].[notification] notify
					INNER JOIN [dbo].[submissionStatusChange] status
					ON notify.statusId = status.statusId
					INNER JOIN [dbo].[submission] sub
					ON notify.submissionId = sub.submissionId
					INNER JOIN [dbo].[userTbl] userTbl
					On userTbl.userId = notify.updatedBy
					INNER JOIN [dbo].[assignment] assgn
					ON assgn.assignmentId = sub.assignmentId
					INNER JOIN [dbo].[course] course
					ON course.courseId = assgn.courseId
					where notify.description is NOT NULL
					AND status.notificationFor = <cfqueryparam value = "#arguments.userId#" CFSQLType = "CF_SQL_BIGINT" />
					<cfif structkeyExists(arguments,'statusId')>
					AND notify.statusId = <cfqueryparam value = "#arguments.statusId#" CFSQLType = "CF_SQL_BIGINT" />
					</cfif>
					ORDER BY #arguments.orderBy#
				</cfquery>
			<cfcatch type ="any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.reviewData = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.reviewData,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.reviewData />
	</cffunction>


	<cffunction name = "pieData" returnformat = "json" access = "remote" returntype = "struct" output = "false"
		description = "this function return data for the pi chart">
		<cfset LOCAL.details = structNew() />
			<cftry>
				<cfquery name = "LOCAL.totalcreatedCourses">
							SELECT COUNT(name) myCreatedCourse
							FROM [dbo].[course]
							WHERE userId = <cfqueryparam value = "#session.stLoggedInUser.userId#" CFSQLType = "CF_SQL_BIGINT" />
						</cfquery>
				<cfquery name = "LOCAL.totalCourse">
							SELECT COUNT(name) totalCourses
							FROM [dbo].[course]
						</cfquery>
				<cfquery name = "LOCAL.totalEnrollCourse">
							SELECT COUNT(distinct course.courseId) totalEnroll
							FROM [dbo].[course]
							INNER JOIN courseEnrollment
							ON courseEnrollment.courseId=course.courseId
						</cfquery>
				<cfquery name = "LOCAL.myEnrollCourse">
							SELECT COUNT(distinct course.courseId) MyCourseEnroll
							FROM [dbo].[course]
							INNER JOIN courseEnrollment
							ON courseEnrollment.courseId=course.courseId
							WHERE course.userId = <cfqueryparam value = "#session.stLoggedInUser.userId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
				<cfset LOCAL.details.totalCourse = totalCourse.totalCourses />
				<cfset LOCAL.details.totalEnroll = totalEnrollCourse.totalEnroll />
				<cfset LOCAL.details.myCourse = totalcreatedCourses.myCreatedCourse />
				<cfset LOCAL.details.myEnrollCourse = myEnrollCourse.myCourseEnroll />
				<cfcatch type = "any">
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfset LOCAL.details.error = true>
					<cfreturn LOCAL.details />
				</cfcatch>
			</cftry>
		<cfreturn LOCAL.details />
	</cffunction>


	<cffunction name = "barData1" returnformat = "json" access = "remote" returntype = "struct" output = "false"
		description = "this function return data for the pi chart">
		<cfset LOCAL.details= structNew() />
			<cftry>
				<cfquery name="LOCAL.enrollInMonDetails">
							SELECT TOP 5 DATENAME(MONTH,enrollmentDate) month,count(*) "totalCourse"
							FROM [dbo].[course]
							INNER join [dbo].[courseEnrollment]
							ON course.courseId = courseEnrollment.courseId
							GROUP BY DATENAME(MONTH,enrollmentDate),DATEPART(M, enrollmentDate)
							ORDER by DATEPART(M, enrollmentDate)
						</cfquery>
				<cfloop from = "1" to = "5" index = "i">
					<cfset LOCAL.details.month[i] = enrollInMonDetails.month[i] />
					<cfset LOCAL.details.course[i] = enrollInMonDetails.totalCourse[i] />
				</cfloop>
				<cfcatch type = "any">
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfset LOCAL.details.error = true>
					<cfreturn LOCAL.details />
				</cfcatch>
			</cftry>
		<cfreturn LOCAL.details />
	</cffunction>


	<cffunction name = "submissionAsgn" returnformat = "json" access = "remote" returntype = "query" output = "false"
		description = "this function return data submission">
		<cfargument name = "submissionId" type = "numeric" required = "true">
		<cftry>
			<cfquery name = "LOCAL.submitAssignment">
							SELECT answerPath filePath
							FROM [dbo].[submission]
							WHERE submissionId = <cfqueryparam value = "#arguments.submissionId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.submitAssignment = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.submitAssignment,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.submitAssignment />
	</cffunction>


	<cffunction name = "pieData1" returnformat = "json" access = "remote" returntype = "struct" output = "false"
		description = "this function return data for the pi chart">
		<cfset LOCAL.details =  structNew() />
			<cftry>
				<cfquery name = "LOCAL.totalAssignment">
							SELECT COUNT(*) totalAsgn
							FROM [dbo].[course]
							INNER JOIN [dbo].[assignment]
							ON course.courseId = assignment.courseId
							INNER join [dbo].[courseEnrollment] enroll
							ON enroll.courseId = course.courseId
							WHERE enroll.userId = <cfqueryparam value = "#session.stLoggedInUser.userId#" CFSQLType = "CF_SQL_BIGINT" />
							AND enroll.isActive = <cfqueryparam value = 1 CFSQLType = "CF_SQL_BIT" />
				</cfquery>
				<cfquery name = "LOCAL.submittedAssigment">
							SELECT COUNT(distinct assignmentId) subAsgn
							FROM submission
							INNER JOIN courseEnrollment enroll
							on submission.enrollmentId=enroll.enrollmentId
							where userId =<cfqueryparam value= "#session.stLoggedInUser.userId#" CFSQLType="CF_SQL_BIGINT" />
				</cfquery>
				<cfquery name = "LOCAL.notSubmittedAssignment">
					select count(*) totalAsgn
					from [dbo].[courseEnrollment] enroll
					INNER JOIN [dbo].[submission] sub
					ON sub.enrollmentId = enroll.enrollmentId
					where enroll.userId = <cfqueryparam value= "#session.stLoggedInUser.userId#" CFSQLType="CF_SQL_BIGINT" />
					AND enroll.isActive = <cfqueryparam value = 1 CFSQLType = "CF_SQL_BIT" />
				</cfquery>
				<cfset LOCAL.details.totalAssignment = totalAssignment.totalAsgn />
				<cfset LOCAL.details.submittedAssignment = submittedAssigment.subAsgn />
				<cfset LOCAL.details.notSubmitted =  totalAssignment.totalAsgn-notSubmittedAssignment.totalAsgn />
				<cfcatch type = "any">
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfset LOCAL.details.error = true>
					<cfreturn LOCAL.details />
				</cfcatch>
			</cftry>

		<cfreturn LOCAL.details />
	</cffunction>


	<cffunction name = "linegraph" returnformat = "json" access = "remote" returntype = "struct" output = "false"
		description = "this function return data for the pi chart">
		<cfset LOCAL.details = structNew() />
			<cftry>
				<cfquery name = "LOCAL.myMarks" result="marks">
							SELECT TOP 10 sub.assignmentId, marks, name ,enrollmentDate,assignment.assignmentId asgnId, submissionTime
							FROM [dbo].[submission] sub
							INNER JOIN [dbo].[courseEnrollment] enroll
							ON sub.enrollmentId = enroll.enrollmentId
							INNER JOIN [dbo].[assignment] assignment
							ON sub.assignmentId = assignment.assignmentId
							WHERE enroll.userId = <cfqueryparam value = "#session.stLoggedInUser.userId#" CFSQLType = "CF_SQL_BIGINT" />
							AND marks IS NOT NULL
							ORDER BY sub.submissionTime DESC
						</cfquery>
				<cfset LOCAL.assignmentId = valueList(LOCAL.myMarks.assignmentId,",")>
				<cfquery name = "LOCAL.highestMarks">
							SELECT MAX(marks) maxMarks
							FROM [dbo].[submission]
							WHERE assignmentId IN(<cfqueryparam value="#LOCAL.assignmentId#" list = "yes" />)
							AND marks IS NOT NULL
							GROUP BY submissionTime
							ORDER BY submissionTime DESC
						</cfquery>
				<cfloop from = "1" to = "10" index = "i">
					<cfset details.asgnName[i] = myMarks.name[i] />
					<cfset details.myMarks[i] = myMarks.marks[i] />
					<cfset details.MaxMarks[i] = highestMarks.maxMarks[i] />
				</cfloop>
				<cfcatch typ e ="any">
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfset LOCAL.details.error = true>
					<cfreturn LOCAL.details />
				</cfcatch>
			</cftry>
		<cfreturn LOCAL.details />
	</cffunction>


	<cffunction name = "uploadAssignmentUser" returnformat = "json" access = "public" returntype = "query" output = "false">
		<cfargument name = "assignmentId" type = "numeric" required = "true">
		<cftry>
			<cfquery name = "LOCAL.assgn">
					SELECT firstName, lastName
					FROM [dbo].[userTbl] userTbl
					INNER JOIN [dbo].[assignment] assgn
					ON assgn.userId = userTbl.userId
					where assgn.assignmentId = <cfqueryparam value = "#arguments.assignmentId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.assgn = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.assgn,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn assgn>
	</cffunction>


	<cffunction name = "checkSubmission" returnformat = "json" access = "public" returntype = "query" output = "false">
		<cfargument name = "assignmentId" type = "numeric" required = "true" />
		<cfargument name = "enrollmentId" type = "numeric" required = "true" />
		<cftry>
			<cfquery name = "LOCAL.submission">
					select * from [dbo].[submission]
					INNER JOIN [dbo].[assignment]
					on assignment.assignmentId = submission.assignmentId
					where assignment.assignmentId = <cfqueryparam value = "#arguments.assignmentId#" CFSQLType = "CF_SQL_BIGINT" />
					AND submission.enrollmentId = <cfqueryparam value = "#arguments.enrollmentId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.submission = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.submission,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.submission />
	</cffunction>


	<cffunction name = "checkExist" returnformat = "json" access = "public" returntype = "query" output = "false">
		<cfargument name = "courseId" type = "numeric" required="true" />
		<cftry>
			<cfquery name = "LOCAL.submission">
				SELECT *
				FROM [dbo].[courseEnrollment] enroll
				INNER JOIN [dbo].[submission] sub
				ON sub.enrollmentId = enroll.enrollmentId
				WHERE enroll.courseId = <cfqueryparam value ="#arguments.courseId#" CFSQLType = "CF_SQL_BIGINT" />
				AND enroll.userId = <cfqueryparam value = "#session.stLoggedInUser.userId#"  CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfset LOCAL.submission = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.submission,{error = 1}) />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.submission />
	</cffunction>


	<cffunction name = "notification" returnformat = "json" access = "public" returntype = "query" output = "false">
			<cfargument name = "userId" type = "numeric" required = "true" />
			<cftry>
			<cfquery name = "LOCAL.notificationDetails">
				SELECT *, firstName+' '+lastName name
				FROM [dbo].[notification] notify
				INNER JOIN [dbo].[submissionStatusChange] status
				ON status.statusId = notify.statusId
				INNER JOIN [dbo].[userTbl] userTbl
				ON userTbl.userId = notify.updatedBy
				WHERE notificationFor = <cfqueryparam value = "#arguments.userid#" CFSQLType = "CF_SQL_BIGINT" />
				AND status = <cfqueryparam value = 0 CFSQLType= "CF_SQL_BIT" />
				ORDER BY notify.createdDate DESC
			</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.TagContext[1].template# line no: #cfcatch.TagContext[1].line#">
				<cfset LOCAL.notificationDetails = queryNew("error","bit") />
				<cfset queryAddRow(LOCAL.notificationDetails,{error = 1}) />
			</cfcatch>
			</cftry>
			<cfreturn LOCAL.notificationDetails />
	</cffunction>

</cfcomponent>

