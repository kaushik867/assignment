<!---
  --- dataInsertion
  --- -------------
  ---
  --- author: mindfire
  --- date:   5/8/20
  --->
<cfcomponent accessors = "false" output = "false" description = "this component is used to insertion of data into into database">

	<cffunction name = "registrationData" access = "public" output = "false" returntype = "boolean"
		description = "this function is used to insert data into database at the time of registration" >
		<cfset LOCAL.isCommit = false />
		<cftransaction>
			<cftry>
				<cfquery name = "LOCAL.dataInsert" result = "userDetails">
				<!---Inserting user details in UserTbl--->
				INSERT INTO [dbo].[UserTbl] (firstName, middleName, lastName, dateOfBirth, userName, personType, password, gender)
				VALUES (
				<cfqueryparam value = "#trim(form.inputFirstName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#trim(form.inputMiddleName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#trim(form.inputLastName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#trim(form.inputDob)#" CFSQLType = "CF_SQL_DATE" />,
				<cfqueryparam value = "#trim(form.inputUserName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#trim(form.inputProfession)#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#hash(form.inputPassword,'SHA-256','UTF-8')#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#trim(form.inputGender)#" CFSQLType = "CF_SQL_CHAR" />
				)
			</cfquery>
				<!---retrive the userId by UserName--->
				<cfset  LOCAL.getUserId = #userDetails.GENERATEDKEY#/>
				<!---Inserting communication details--->
				<cfquery name="LOCAL.InsertCommDetails">
				INSERT INTO [dbo].[Communication] (commValue, userId, communicationTypeId)
				VALUES (
				<cfqueryparam value = "#trim(form.inputEmail)#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = "#LOCAL.getUserId#" CFSQLType = "CF_SQL_VARCHAR" />,
				<cfqueryparam value = 1 CFSQLType = "CF_SQL_BIGINT" />
				)
	        </cfquery>
				<cftransaction action = "commit" />
				<cfset LOCAL.isCommit = true />
				<cfcatch type = "any">
					<cftransaction action = "rollback" />
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfreturn LOCAL.isCommit />
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn LOCAL.isCommit />
	</cffunction>

	<cffunction name = "update" returnFormat = "json" access = "remote" output = "false" returntype = "boolean"
		description = "this function is used to update the details of the users">
		<cfset LOCAL.msg = false />
		<cftry>
			<cfquery name = "LOCAL.dataUpdate">
					UPDATE [dbo].[UserTbl]
					SET
						firstName = <cfqueryparam value = "#trim(form.inputFirstName)#" CFSQLType = "CF_SQL_VARCHAR" />,
						middleName = <cfqueryparam value = "#trim(form.inputMiddleName)#" CFSQLType = "CF_SQL_VARCHAR" />,
						lastName = <cfqueryparam value = "#trim(form.inputLastName)#" CFSQLType = "CF_SQL_VARCHAR" />
					WHERE userName = <cfqueryparam value = "#trim(form.inputUserName)#" CFSQLType = "CF_SQL_VARCHAR" />
					<cfset LOCAL.msg = true>
					</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfreturn LOCAL.msg />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.msg/>

	</cffunction>

	<cffunction name = "updateWithPass" returnFormat = "json" access = "remote" output = "false" returntype = "boolean"
		description = "this function is used to update the details of the users">
		<cfset LOCAL.msg = false />
		<cftry>
			<cfquery name = "LOCAL.dataUpdate">
				UPDATE [dbo].[UserTbl]
				SET firstName = <cfqueryparam value = "#trim(form.inputFirstName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				middleName = <cfqueryparam value = "#trim(form.inputMiddleName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				lastName = <cfqueryparam value = "#trim(form.inputLastName)#" CFSQLType = "CF_SQL_VARCHAR" />,
				password = <cfqueryparam value = "#hash(form.inputPassword,'SHA-256','UTF-8')#" CFSQLType = "CF_SQL_VARCHAR" />
				where userName = <cfqueryparam value = "#trim(form.inputUserName)#" CFSQLType = "CF_SQL_VARCHAR" />
				<cfset LOCAL.msg = true>
				</cfquery>
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfreturn LOCAL.msg />
			</cfcatch>
		</cftry>
		<cfreturn msg/>
	</cffunction>

	<cffunction name = "courseDataInsertion" access = "public" output = "false" returntype = "boolean"
		description = "this function is used to insert data into course Table" >
		<cfset LOCAL.isCommit = false />
		<cftry>
			<cfquery name = "LOCAL.insertCourse">
				INSERT INTO [dbo].[course](name,description,userId, createdTime)
				VALUES(
						<cfqueryparam value = "#trim(form.inputCourseName)#" CFSQLType = "CF_SQL_VARCHAR" />,
						<cfqueryparam value = "#trim(form.inputCourseDescription)#" CFSQLType = "CF_SQL_VARCHAR" />,
						<cfqueryparam value = "#session.stLoggedInUser.userId#"  CFSQLType = "CF_SQL_BIGINT" />,
						<cfqueryparam value ="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
				)
			</cfquery>
			<cfset LOCAL.isCommit=true>
			<cfcatch>
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfthrow detail="#cfcatch.Message#" type="error">
				<cfreturn LOCAL.isCommit />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.isCommit />
	</cffunction>

	<cffunction name = "courseUpdate" access = "public" output = "false" returntype = "boolean"
		description = "this function is used to insert data into course Table" >
		<cfargument name = "courseId" type = "numeric" required = "true" />
		<cfset LOCAL.isCommit = false />
		<cftry>
			<cfquery name = "LOCAL.courseDataUpdate">
					UPDATE [dbo].[course]
						SET name = <cfqueryparam value = "#trim(form.inputCourseName)#" CFSQLType = "CF_SQL_VARCHAR" />,
						description = <cfqueryparam value = "#trim(form.inputCourseDescription)#" CFSQLType = "CF_SQL_VARCHAR" />,
						createdTime = <cfqueryparam value ="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
						where courseId = <cfqueryparam value = "#arguments.courseId#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfset LOCAL.isCommit=true>
			<cfcatch>
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfthrow detail = "#cfcatch.Message#" type="error">
				<cfreturn LOCAL.isCommit />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.isCommit>
	</cffunction>

	<cffunction name = "assignmentDataInsertion" access = "public" output = "false" returntype = "boolean"
		description = "this function is used to insert data into assignment Table" >
		<cfargument name = "fileDirectory" type = "string" required = "true" />
		<cfset LOCAL.isCommit = false />
		<cftransaction>
			<cftry>
				<cfquery name = "LOCAL.insertAssignment">
						INSERT INTO [dbo].[assignment]
								(name, description, filePath, courseId, userId,isActive, lastUpdate, startDate, endDate, createdDate)
						VALUES(
								<cfqueryparam value = "#trim(form.inputAssignmentName)#" CFSQLType = "CF_SQL_VARCHAR" />,
								<cfqueryparam value = "#trim(form.description)#" CFSQLType = "CF_SQL_VARCHAR" />,
								<cfqueryparam value = "#arguments.fileDirectory#" CFSQLType = "CF_SQL_VARCHAR" />,
								<cfqueryparam value = "#trim(form.inputCourseName)#" CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = "#session.stLoggedInUser.userId#"  CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = 1 CFSQLType = "CF_SQL_BIT" />,
								<cfqueryparam value ="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />,
								<cfqueryparam value= "#trim(form.inputStartDate)#" CFSQLType = "CF_SQL_DATE" />,
								<cfqueryparam value = "#trim(form.inputEndDate)#" CFSQLType = "CF_SQL_DATE" />,
								<cfqueryparam value ="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
						)
				</cfquery>
				<cfquery name = "LOCAL.newAssgAdded" result = "added">
						INSERT INTO [dbo].[notification]
								(comment, courseId, updatedBy, createdDate)
						VALUES(
								<cfqueryparam value = "#trim(form.inputAssignmentName)# is added in #LOCAL.course.name# " CFSQLtype = "CF_SQL_VARCHAR" />,
								<cfqueryparam value = "#trim(form.inputCourseName)#" CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = "#session.stLoggedInUser.userId#"  CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = "#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
							)
				</cfquery>
				<cfset LOCAL.statusId = #added.GENERATEDKEY#>
				<cfloop query = "user">
					<cfquery name="LOCAL.status">
						INSERT INTO [dbo].[submissionStatusChange]
								(statusId, status, notificationFor)
						VALUES(
								<cfqueryparam value = "#LOCAL.statusId#" CFSQLtype = "CF_SQL_BIGINT" />,
								<cfqueryparam value = 0 CFSQLType = "CF_SQL_BIT" />,
								<cfqueryparam value = "#LOCAL.user.userId#" CFSQLType = "CF_SQL_BIGINT" />
							)
					</cfquery>
				</cfloop>
				<cftransaction action ="commit" />
				<cfset LOCAL.isCommit=true />
				<cfcatch type = "any">
					<cftransaction action ="rollback" />
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfreturn LOCAL.isCommit />
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn LOCAL.isCommit />
	</cffunction>

	<cffunction name = "insertUploadAssig" returnFormat = "json" access = "public" output = "false" returntype = "boolean"
		description = "this function is used to insert the data in submission table">
		<cfargument name = "assignmentId" type = "numeric" required = "true" />
		<cfargument name = "enrollmentId" type = "numeric" required = "true" />
		<cfargument name = "fileDirectory" type = "string" required = "true" />
		<cfset LOCAL.isCommit=false >
		<cftransaction>
			<cftry>
				<cfset LOCAL.assignment = application.dbOperation.assignment(arguments.assignmentId) />
				<cfif structKeyExists(LOCAL.assignment,'error')>
					<cfthrow message = "Error Executing Database Query" />
				</cfif>
				<cfquery name = "LOCAL.uploadAssg" result = "submission">
							INSERT INTO [dbo].[submission]
									(enrollmentId, assignmentId, answerPath, submissionTime)
							VALUES(
									<cfqueryparam value = "#trim(arguments.enrollmentId)#" CFSQLType = "CF_SQL_BIGINT" />,
									<cfqueryparam value = "#trim(arguments.assignmentId)#" CFSQLType = "CF_SQL_BIGINT" />,
									<cfqueryparam value = "#trim(arguments.fileDirectory)#" CFSQLType = "CF_SQL_VARCHAR" />,
									<cfqueryparam value = "#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
									)
				</cfquery>
				<cfset LOCAL.submissionId = #submission.GENERATEDKEY#/>
				<cfset LOCAL.teacherId = application.dbOperation.getTeacherId(LOCAL.submissionId) />
				<cfif structKeyExists(LOCAL.teacherId,'error')>
					<cfthrow message = "Error Executing Database Query haha" />
				</cfif>
				<cfquery name = "LOCAL.status" result = "notificationRes">
							INSERT INTO [dbo].[notification]
									(comment, updatedBy, createdDate,submissionId)
							VALUES(
									<cfqueryparam value = "#LOCAL.assignment.name# is uploaded" CFSQLType = "CF_SQL_VARCHAR">,
									<cfqueryparam value = "#session.stLoggedInUser.userId#" CFSQLType = "CF_SQL_BIGINT">,
									<cfqueryparam value = "#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />,
									<cfqueryPAram value = "#LOCAL.submissionid#" CFSQLType = "CF_SQL_BIGINT">
							)
				</cfquery>
				<cfset LOCAL.statusId = #notificationRes.GENERATEDKEY# />
				<cfquery name="LOCAL.statusChnange">
							INSERT INTO [dbo].[submissionStatusChange]
										( statusId, NotificationFor, status)
							VALUES(
									<cfqueryparam value = "#LOCAL.statusId#" CFSQLType = "CF_SQL_BIGINT" />,
									<cfqueryparam value = "#LOCAL.teacherId.userId#" CFSQLType = "CF_SQL_BIGINT" />,
									<cfqueryparam value =0 CFSQLType = "CF_SQL_BIT" />
							)
				</cfquery>
				<cftransaction action = "commit" />
				<cfset LOCAL.isCommit = true>
				<cfcatch type = "any">
					<cftransaction action = "rollback" />
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn LOCAL.isCommit />
	</cffunction>

	<cffunction name = "enrollment" access = "remote" returnFormat = "json" output = "false" returntype = "struct">
		<cfargument name = "userId" type = "numeric" required = "true" />
		<cfargument name = "courseId" type = "numeric" required = "true" />
		<cfset LOCAL.isCommit = structNew() />
		<cfset LOCAL.isCommit.success=false />
		<cftransaction>
		<cftry>
			<cfset LOCAL.courseEnroll = application.dbOperation.enrollmentDetails(arguments.courseId,arguments.userId) />
			<cfif LOCAL.courseEnroll.recordCount EQ 1>
				<cfif structKeyExists(LOCAL.courseEnroll,'error') >
					<cfthrow message = "Error Executing Database Query">
				</cfif>
				<cfquery name = "LOCAL.enroll">
						UPDATE [dbo].[courseEnrollment]
						SET isActive = <cfqueryparam value = 1 CFSQLType = "CF_SQL_BIT" />,
							 enrollmentDate = <cfqueryparam value ="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
						WHERE userId = <cfqueryparam value = "#trim(arguments.userId)#" CFSQLType = "CF_SQL_BIGINT" />
						AND courseId = <cfqueryparam value = "#trim(arguments.courseId)#" CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfelse>
				<cfquery name = "LOCAL.enroll">
						INSERT INTO [dbo].[courseEnrollment](userId, courseId, enrollmentDate)
						VALUES(
								<cfqueryparam value = "#trim(arguments.userId)#" CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = "#trim(arguments.courseId)#" CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value ="#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />
						)
				</cfquery>
			</cfif>
			<cfset LOCAL.isCommit.success = true />
			<cftransaction action = "commit" />
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cftransaction action = "rollback" />
				<cfset LOCAL.isCommit.response = false />
				<cfreturn LOCAL.isCommit />
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn LOCAL.isCommit />
	</cffunction>

	<cffunction name = "withdrawCourse" access = "remote" returnFormat = "json" output = "false" returntype = "struct">
		<cfargument name = "courseId" type = "numeric" required = "true" />
		<cfset LOCAL.isCommit.success = false />
		<cftry>
			<cfquery name="LOCAL.deleteEnrollCourse">
					UPDATE [dbo].[courseEnrollment]
					SET isActive = <cfqueryparam value = 0 CFSQLType = "CF_SQL_BIT" />
					WHERE courseId = <cfqueryparam value = "#arguments.courseId#" CFSQLType = "CF_SQL_BIGINT" />
					AND userId = <cfqueryparam value = "#session.stLoggedInUser.userId#"  CFSQLType = "CF_SQL_BIGINT" />
				</cfquery>
			<cfset LOCAL.isCommit.success = true />
			<cfcatch type = "any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
				<cfthrow detail = "#cfcatch.Message#" type = "error" />
				<cfreturn LOCAL.isCommit />
			</cfcatch>
		</cftry>
		<cfreturn LOCAL.isCommit>
	</cffunction>

	<cffunction name = "reviewInsertion" access = "public" output = "false" returntype = "boolean"
		description = "this function is used to insert data into review Table" >
		<cfargument name = "submissionId" type = "numeric" required = "true" />
		<cfargument name = "assignmentId" type = "numeric" required = "true" />
		<cfargument name = "inputMarks" type = "string" required = "true" />
		<cfargument name = "inputReview" type = "string" required = "true" />
		<cfset LOCAL.isCommit = false />
		<cftransaction>
			<cftry>
				<cfset LOCAL.assignment = application.dbOperation.assignment(arguments.assignmentId)>
				<cfset LOCAL.studentId = application.dbOperation.getStudentId(arguments.submissionId) />
				<cfif structKeyExists(LOCAL.assignment,'error') OR structKeyExists(LOCAL.studentId,'error')>
					<cfthrow message = "Error Executing Database Query" />
				</cfif>
				<cfquery name = "LOCAL.addReview" result = "notificationRes">
				INSERT INTO  [dbo].[notification]
						( updatedBy, comment, description, createdDate, submissionId)
				VALUES(
						<cfqueryparam value = "#session.stLoggedInUser.userId#"  CFSQLType = "CF_SQL_BIGINT" />,
						<cfqueryparam value = "#LOCAL.assignment.name# review added" CFSQLType = "CF_SQL_VARCHAR" />,
						<cfqueryparam value = "#arguments.inputReview#" CFSQLType = "CF_SQL_VARCHAR" />,
						<cfqueryparam value = "#dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss")#" CFSQLType = "CF_SQL_TIMESTAMP" />,
						<cfqueryPAram value = "#arguments.submissionid#" CFSQLType = "CF_SQL_BIGINT" />
				)
			</cfquery>
				<cfset LOCAL.statusId = #notificationRes.GENERATEDKEY#>
					<cfquery name = "LOCAL.status">
					INSERT INTO [dbo].[submissionStatusChange]
							(statusId, notificationFor, status)
					VALUES(
							<cfqueryparam value = "#LOCAL.statusId#" CFSQLType = "CF_SQL_BIGINT" />,
							<cfqueryparam value = "#LOCAL.studentId.userId#" CFSQLType = "CF_SQL_BIGINT" />,
							<cfqueryparam value = 0 CFSQLType = "CF_SQL_BIT" />
					)
			</cfquery>
				<cfquery name = "LOCAL.addMarks">
					UPDATE [dbo].[submission]
					SET marks = <cfqueryparam value = "#arguments.inputMarks#" CFSQLType = "CF_SQL_VARCHAR" />
					WHERE submissionId=<cfqueryparam value = "#arguments.submissionId#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
				<cftransaction action = "commit" />
				<cfset LOCAL.isCommit = true>
				<cfcatch type = "any">
					<cftransaction action = "rollback" />
					<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
					<cfreturn LOCAL.isCommit />
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn LOCAl.isCommit />
	</cffunction>

	<cffunction name = "deleteFiles" access = "remote" output = "false" >
		<cfset thirtyDays = dateAdd("d", -3, now())>
		<cftry>
			<cfquery name = "LOCAL.oldFiles">
				SELECT *
				FROM [dbo].[assignment]
				WHERE  endDate < <cfqueryparam value = "#dateFormat(thirtyDays,'yyyy-mm-dd')#" CFSQLType = "CF_SQL_DATE">
			</cfquery>
			<cfloop query = "LOCAL.oldFiles">
				<cfif FileExists(oldFiles.filePath)>
					<cffile action = "DELETE" file = "#LOCAL.oldFiles.filePath#" />
				</cfif>
			</cfloop>
			<cfquery name = "LOCAL.oldSubmittedFiles">
				SELECT *
				FROM [dbo].[submission]
				WHERE  submissionTime < <cfqueryparam value = "#dateFormat(thirtyDays,'yyyy-mm-dd')#" CFSQLType = "CF_SQL_DATE">
			</cfquery>
			<cfloop query = "LOCAL.oldSubmittedFiles">
				<cfif FileExists(oldSubmittedFiles.answerPath)>
					<cffile action = "DELETE" file = "#LOCAL.oldSubmittedFiles.answerPath#" />
				</cfif>
			</cfloop>
			<cfcatch tyep="any">
				<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>