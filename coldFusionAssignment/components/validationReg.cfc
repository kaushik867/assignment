<!---
  --- NewCFComponent
  --- --------------
  ---
  --- author: kaushik
  --- date:   3/24/20
  --->
<cfcomponent output = "false" description = "This component is used to valdate all form data and insert into database" >

	<!---validateUser() method--->
<cffunction name = "validateUser" returnformat = "json" access = "remote" output = "false"  returntype = "struct"
	description="this function is used to vaidate all form data">
	<cfargument name = "userNameValid" type = "boolean" required = "true" />
	<cfargument name = "emailValid" type = "boolean" required = "true" />
	<cfargument name = "formName" type = "string" required = "true" />
	<cfargument name = "inputFirstName" type = "string" required = "true" />
	<cfargument name = "inputMiddleName" type = "string" required = "true" />
	<cfargument name = "inputLastName" type = "string" required = "true" />
	<cfargument name = "inputGender" type = "string" required = "true" />
	<cfargument name = "inputEmail" type = "string" required = "true" />
	<cfargument name = "inputDob" type = "String" required = "true" />
	<cfargument name = "inputProfession" type = "string" required = "true" />
	<cfargument name = "inputUserName" type = "string" required = "true">
	<cfargument name = "inputPassword" type = "string" required = "true" />
	<cfargument name = "inputConfirmPassword" type="string" required = "true" />
	<cfargument name = "oldPasswordValid" type = "string" required = "false" />
	<cfargument name = "oldPassword" type = "string" required = "false" />

	<cfset  LOCAL.errorMessages = structNew() />
	<cfset LOCAL.errorMessages.success = false />
	<cfset LOCAL.errorMessages.response = true />
	<cfif (trim(arguments.inputFirstName) EQ '' OR NOT isValid('regex', trim(arguments.inputFirstName), "^[a-zA-Z]+[']?[a-zA-Z]+$")) OR len(trim(arguments.inputFirstName)) GT 30  >
		<cfset LOCAL.errorMessages.errFirstName = "Please provide a valid first name" />
	</cfif>
	<cfif (trim(arguments.inputMiddleName) NEQ '' AND NOT isValid('regex', trim(arguments.inputMiddleName), "^[a-zA-Z]+[']?[a-zA-Z]+$")) OR len(trim(arguments.inputMiddleName)) GT 30>
		<cfset LOCAL.errorMessages.errMiddleName = "Please provide a valid middle name" />
	</cfif>
	<cfif (trim(arguments.inputLastName) EQ '' OR NOT isValid('regex', trim(arguments.inputLastName), "^[a-zA-Z]+[']?[a-zA-Z]+"))  OR len(trim(arguments.inputLastName)) GT 30>
		<cfset LOCAL.errorMessages.errLastName = 'Please provide a valid last name' />
	</cfif>
	<cfif (trim(arguments.inputEmail) EQ '' OR NOT isValid('regex', trim(arguments.inputEmail), "^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$")) OR len(trim(arguments.inputEmail)) GT 40 >
		<cfset LOCAL.errorMessages.errEmail = 'Please provide a valid email'/>
	</cfif>
	<cfif arguments.inputDob EQ ''  >
		<cfset LOCAL.errorMessages.errDob = 'Please provide a valid date of birth '/>
	</cfif>
	<cfif arguments.inputGender EQ ''>
		<cfset LOCAL.errorMessages.errGender = 'Please select above option' />
	</cfif>
	<cfif trim(arguments.inputUserName) EQ '' OR len(trim(arguments.inputUserName)) GT 20>
		<cfset LOCAL.errorMessages.errUserName = 'Please provide a valid user name '/>
	</cfif>
	<cfif arguments.inputProfession EQ ''  >
		<cfset LOCAL.errorMessages.errProfession = 'Please select above option'/>
	</cfif>
	<cfif arguments.formName EQ "updateForm">
		<cfif arguments.inputPassword NEQ '' AND NOT isValid('regex',arguments.inputPassword, "^(?=.*[0-9])(?=.*[&*$'#'%@])[a-zA-z0-9&*$'#'%@]{8,15}$" ) >
			<cfset LOCAL.errorMessages.errPassword = 'Please provide a valid new password '/>
		</cfif>
		<cfif arguments.inputConfirmPassword NEQ arguments.inputPassword >
			<cfset LOCAL.errorMessages.errConfirmPassword = "password does'nt match"/>
		</cfif>
		<cfif arguments.oldPassword EQ ''  AND NOT isValid('regex',arguments.inputPassword, "^(?=.*[0-9])(?=.*[&*$'#'%@])[a-zA-z0-9&*$'#'%@]{8,15}$" ) AND NOT arguments.oldPasswordValid >
			<cfset LOCAL.errorMessages.errOldPass = "please provide a valid old password" />
		<cfelseif !arguments.oldPasswordValid>
			<cfset LOCAL.errorMessages.errOldPass = "old password does't match" />
		</cfif>
		<cfif StructCount(LOCAL.errorMessages) EQ 2 AND arguments.userNameValid AND arguments.emailValid AND arguments.oldPasswordValid AND arguments.inputPassword EQ ''>
			<cflog text="1">
			<cfset LOCAL.errorMessages.success = application.dataInsertion.update() />
		</cfif>
		<cfif StructCount(LOCAL.errorMessages) EQ 2 AND arguments.userNameValid AND arguments.emailValid AND arguments.oldPasswordValid AND arguments.inputPassword NEQ ''>
			<cflog text="2">
			<cfset LOCAL.errorMessages.success = application.dataInsertion.updateWithPass() />
		</cfif>
	<cfelse>
		<cfif arguments.inputPassword EQ '' OR NOT isValid('regex',arguments.inputPassword, "^(?=.*[0-9])(?=.*[&*$'#'%@])[a-zA-z0-9&*$'#'%@]{8,15}$" ) >
			<cfset LOCAL.errorMessages.errPassword = 'Please provide a valid password '/>
		</cfif>
		<cfif arguments.inputConfirmPassword EQ ''>
			<cfset LOCAL.errorMessages.errConfirmPassword = "Please provide a valid confirm password" />
		<cfelseif arguments.inputConfirmPassword NEQ arguments.inputPassword >
			<cfset LOCAL.errorMessages.errConfirmPassword = "password does'nt match"/>
		</cfif>
		<cfif StructCount(LOCAL.errorMessages) EQ 2 AND arguments.userNameValid AND arguments.emailValid  >
			<cfset LOCAL.errorMessages.success = application.dataInsertion.registrationData() />
		</cfif>
	</cfif>

	<cfif StructCount(LOCAL.errorMessages) EQ 2 AND !LOCAL.errorMessages.success>
		<cfset LOCAL.errorMessages.response = false />
	</cfif>
	<cfreturn LOCAL.errorMessages />
</cffunction>

<!--- validateUserName() exist in dataBase or not--->
<cffunction name = "validateUserName" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate the userName in registration page is unique or not">
	<cfargument name = "inputUserName" type = "string" required = "true" />
	<cfset LOCAL.errorMsg = {"error" = true,"response" = false}/>
	<cftry>
		<cfset LOCAL.userNameExist = application.dbOperation.userName(arguments.inputUserName)>
		<cfif LOCAL.userNameExist.recordCount EQ 1>
			<cfif structKeyExists(LOCAL.userNameExist,'error')>
				<cfset LOCAL.errorMsg.response = true>
				<cfset LOCAL.errorMsg.error = false>
			<cfelse>
				<cfset LOCAL.errorMsg.error = false>
			</cfif>
		</cfif>
		<cfcatch type = "any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfset LOCAL.errorMsg.response = true />
			<cfreturn LOCAL.errorMsg />
		</cfcatch>
	</cftry>
	<cfreturn LOCAL.errorMsg />
</cffunction>

<!--- validateEmail() --->
<cffunction name = "validateEmail" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate email present into database or not">
	<cfargument name = "inputEmail" type = "string" required = "true">
	<cfset LOCAL.errorMsg = {"error" = true,"response" = false}/>
	<cftry>
		<cfset LOCAL.emailExist = application.dbOperation.email(arguments.inputEmail)>
		<cfif LOCAL.emailExist.recordCount EQ 1>
			<cfif structKeyExists(LOCAL.emailExist,'error')>
				<cfset LOCAL.errorMsg.response = true />
				<cfset LOCAL.errorMsg.error = false />
			<cfelse>
				<cfset LOCAL.errorMsg.error = false />
			</cfif>
		</cfif>
		<cfcatch type = "any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfset LOCAL.errorMsg.response = true />
			<cfreturn LOCAL.errorMsg />
		</cfcatch>
	</cftry>
	<cfreturn LOCAL.errorMsg />
</cffunction>

<cffunction name = "validatePassword" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate email present into database or not">
	<cfargument name = "inputUserName" type = "string" required = "true">
	<cfargument name = "inputPassword" type = "string" required = "true">
	<cfset LOCAL.errorMsg = {"error" = true,"response" = false}/>
	<cftry>
		<cfset LOCAL.passwordExist = application.dbOperation.password(arguments.inputUserName,arguments.inputPassword) />
		<cfif LOCAL.passwordExist.recordCount EQ 1>
			<cfif structKeyExists(LOCAL.passwordExist,'error')>
				<cfset LOCAL.errorMsg.response = true />
				<cfset LOCAL.errorMsg.error = false />
			<cfelse>
				<cfset LOCAL.errorMsg.error = true />
			</cfif>
		</cfif>
		<cfcatch type = "any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfset LOCAL.errorMsg.response = true />
			<cfreturn LOCAL.errorMsg />
		</cfcatch>
	</cftry>
	<cfreturn errorMsg/>
</cffunction>

<cffunction name = "validateOldPassword" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate email present into database or not">
	<cfargument name = "inputPassword" type = "string" required = "true" />
	<cfset LOCAL.inputUserName = session.stLoggedInUser.userName />
	<cfset LOCAL.errorMsg = {"error" = false,"response" = false} />
	<cftry>
		<cfset LOCAL.passwordExist = application.dbOperation.password(LOCAL.inputUserName,arguments.inputPassword) />
		<cfif LOCAL.passwordExist.recordCount EQ 1>
			<cfif structKeyExists(LOCAL.passwordExist,'error')>
				<cfset LOCAL.errorMsg.response = true />
				<cfset LOCAL.errorMsg.error = false />
			<cfelse>
				<cfset LOCAL.errorMsg.error = true />
			</cfif>
		</cfif>
		<cfcatch type = "any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfset LOCAL.errorMsg.response = false />
			<cfreturn LOCAL.errorMsg />
		</cfcatch>
	</cftry>
	<cfreturn errorMsg />
</cffunction>

<cffunction name = "getCourseDetails" returnformat = "json" access = "remote" output = "false" returnType = "struct" >
	<cfargument name = "id" type = "numeric" required = "true">
	<cfset LOCAL.courseDetails = application.dbOperation.coursesDetails("name",arguments.id) />
	<cfreturn queryGetRow(LOCAL.courseDetails,1) >
</cffunction>

<cffunction name = "getReviewDetails" returnformat = "json" access = "remote" output = "false" returnType = "struct" >
	<cfargument name = "id" type = "numeric" required = "true" />
	<cfset LOCAL.reviewDetails = application.dbOperation.review("name",session.stLoggedInUser.userId,arguments.id) />
	<cfreturn queryGetRow(LOCAL.reviewDetails,1) >
</cffunction>

<cffunction name = "getAssignmentDetails" returnformat = "json" access = "remote" output = "false" returnType = "struct" >
	<cfargument name = "id" type = "numeric" required = "true" />
	<cfset LOCAL.assignmentDetails = application.dbOperation.assignment(arguments.id) />
	<cfreturn queryGetRow(LOCAL.assignmentDetails,1) >
</cffunction>

<cffunction name = "validateCourse" returnFormat = "json" access = "remote" output = "false" returntype = "struct">
	<cfargument name = "formName" type = "string" required = "true" />
	<cfargument name = "courseNameValid" type = "boolean" required = "true" />
	<cfargument name = "inputCourseName" type = "string" required = "true" />
	<cfargument name = "inputCourseDescription" type = "string" required = "true" />
	<cfargument name = "courseId" type = "string" required = "false" />
	<cfset LOCAL.errorMessages = structNew() />
	<cfset LOCAL.errorMessages.success = false />
	<cfset LOCAL.errorMessages.response = true />
	<cfif arguments.inputCourseName EQ '' OR len(trim(arguments.inputCourseName)) GT 50 >
		<cfset LOCAL.errorMessages.errCourseName = 'please enter a valid course name' />
	</cfif>
	<cfif arguments.inputCourseDescription EQ '' OR len(trim(arguments.inputCourseDescription)) GT 500>
		<cfset LOCAL.errorMessages.errCourseDescription = 'please enter a valid course name' />
	</cfif>
	<cfif StructCount(errorMessages) EQ 2 AND arguments.courseNameValid>
		<cfif arguments.formName EQ "updateCourseForm">
			<cfset LOCAL.errorMessages.success = application.dataInsertion.courseUpdate(arguments.courseId) />
		<cfelse>
			<cfset LOCAL.errorMessages.success = application.dataInsertion.courseDataInsertion() />
		</cfif>
	</cfif>
	<cfif StructCount(LOCAL.errorMessages) EQ 2 AND !LOCAL.errorMessages.success>
		<cfset LOCAL.errorMessages.response = false />
	</cfif>
	<cfreturn LOCAL.errorMessages />
</cffunction>

<cffunction name = "validateCourseName" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate the course name is unique or not">
	<cfargument name = "inputCourseName" type = "string" required = "true" />
	<cfset LOCAL.errorMsg = {"error" = true,"response" = false} />
	<cftry>
		<cfset LOCAL.courseNameExist = application.dbOperation.courseName(arguments.inputCourseName) />
		<cfif LOCAL.courseNameExist.recordCount EQ 1>
			<cfif structKeyExists(LOCAL.courseNameExist,'error')>
				<cfset LOCAL.errorMsg.response = true />
			<cfelse>
				<cfset LOCAL.errorMsg.error = false />
			</cfif>
		</cfif>
		<cfcatch type = "any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfset LOCAL.errorMsg.response = false />
			<cfreturn LOCAL.errorMsg />
		</cfcatch>
	</cftry>
	<cfreturn LOCAL.errorMsg />
</cffunction>

<cffunction name = "getDetails" returnFormat = "json" access = "remote" output = "false" returntype = "struct">
	<cfargument name = "courseId" type = "numeric" required = "true">
	<cftry>
		<cfset LOCAL.myCourses = application.dbOperation.coursesDetails("name",arguments.courseId)>
		<cfset LOCAL.details = structNew() />
		<cfset LOCAL.details.inputCourseName = myCourses.name />
		<cfset LOCAL.details.inputCourseDescription = myCourses.description />
		<cfset LOCAL.details.courseId = myCourses.courseId />
		<cfset LOCAL.details.response = true />
	<cfcatch>
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfif structKeyExists(LOCAL.courseNameExist,'error')>
				<cfset LOCAL.errorMsg.response = false />
			</cfif>
	</cfcatch>
	</cftry>
	<cfreturn LOCAL.details />
</cffunction>

<cffunction name = "validateAssignment" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate assignment data befor inserting database">
	<cfargument name = "inputCourseName" type="string" required="true" />
	<cfargument name = "inputAssignmentName" type="string" required="true" />
	<cfargument name = "description" type="string" required="true" />
	<cfargument name = "inputStartDate" type="string" required="true" />
	<cfargument name = "inputEndDate" type="string" required="true" />
	<cfset LOCAL.errorMessages = structNew() />
	<cfset LOCAl.errorMessages.success = false />
	<cfset LOCAl.errorMessages.response = true />
	<cfif trim(arguments.inputCourseName) EQ '' OR len(trim(arguments.inputCourseName)) GT 50>
		<cfset LOCAL.errorMessages.errCourseName = 'please provide a valid course name' />
	</cfif>
	<cfif trim(arguments.inputAssignmentName) EQ '' OR len(trim(arguments.inputAssignmentName)) GT 50>
		<cfset LOCAL.errorMessages.errAssignmentName = 'please provide a valid assgnment name' />
	</cfif>
	<cfif trim(arguments.description) EQ '' OR len(trim(arguments.description)) GT 500>
		<cfset LOCAL.errorMessages.errDescription = 'please provide a valid description' />
	</cfif>
	<cfif trim(arguments.inputStartDate) EQ '' >
		<cfset LOCAL.errorMessages.errStartDate = 'please provide a valid start date' />
	</cfif>
	<cfif trim(arguments.inputEndDate) EQ '' OR len(trim(arguments.description)) GT 500>
		<cfset LOCAL.errorMessages.errEndDate = 'please provide a valid end date' />
	</cfif>
	<cfif arguments.inputStartDate NEQ '' AND arguments.inputEndDate NEQ ''>
		<cfif datecompare(arguments.inputEndDate, arguments.inputStartDate ) EQ 0 OR  datecompare(arguments.inputEndDate, arguments.inputStartDate ) LT 1 >
			<cfset LOCAL.errorMessages.errEndDate = 'please provide a valid end date' />
		</cfif>
	</cfif>
	<cfset LOCAL.dest = expandPath("./files")>
	<cfif not directoryExists(LOCAL.dest)>
		<cfdirectory action="create" directory="#LOCAL.dest#">
	</cfif>
	<cftry>
		<cffile action = "upload" fileField = "form.fileupload" destination = "#LOCAL.dest#"  nameConflict = "makeUnique" result = "thisResult">
		<cfset LOCAL.size = #LSNumberFormat(thisResult.fileSize /1024 /1024,0.00)#>
		<cfif thisResult.CONTENTSUBTYPE NEQ 'pdf'>
			<cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
			<cfset LOCAL.errorMessages.errUpload = "upload a valid pdf">
		<cfelseif size LT 0.02>
			<cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
			<cfset LOCAL.errorMessages.errUpload = "upload file is too small">
		<cfelseif size GT 5>
			<cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
			<cfset LOCAL.errorMessages.errUpload = "upload file is too big">
		<cfelseif structCount(LOCAL.errorMessages) NEQ 2>
			<cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
		<cfelse>
			<cffile action = "rename" source = "#LOCAL.dest#./#thisResult.ServerFile#" destination = "#LOCAL.dest#./#hash(thisResult.ServerFile,'MD5','UTF-8')##hash(timeformat(now(),'hh_mm_ss'),'MD5','UTF-8')#.pdf">
			<cfset LOCAL.fileDirectory= "#LOCAL.dest#\#hash(thisResult.ServerFile,'MD5','UTF-8')##hash(timeformat(now(),'hh_mm_ss'),'MD5','UTF-8')#.pdf" />
		</cfif>
		<cfcatch type = "any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
			<cfset LOCAL.errorMessages.errUpload = "please provide a valid upload file">
		</cfcatch>
	</cftry>
	<cfif StructCount(LOCAL.errorMessages) EQ 2 >
		<cfset LOCAL.errorMessages.success = application.dataInsertion.assignmentDataInsertion(LOCAL.fileDirectory) />
	</cfif>
	<cfif StructCount(LOCAL.errorMessages) EQ 2 AND !LOCAL.errorMessages.success>
		<cfset LOCAL.errorMessages.response = false />
	</cfif>
	<cfreturn LOCAL.errorMessages />
</cffunction>

<cffunction name = "enrollmentDetails" access = "remote" returnFormat = "json" output = "false" return type = "struct">
	<cfargument name = "assignmentId" type = "numeric" required = "true" />
	<cfargument name = "enrollmentId" type = "numeric" required = "true" />
	<cfset LOCAL.ids = structNew()>
	<cfset LOCAL.ids.assignmentId = arguments.assignmentId>
	<cfset LOCAl.ids.enrollmentId = arguments.enrollmentId>
	<cfreturn LOCAl.ids />
</cffunction>

<cffunction name = "validateUpload" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function used to vlidate the upload file">
	<cfargument name = "assignmentId" type = "numeric" required = "true">
	<cfargument name = "enrollmentId" type = "numeric" required = "true">

	<cfset LOCAL.errorMessage = structNew() />
	<cfset LOCAL.errorMessages.success = false />
	<cfset LOCAL.errorMessages.response = true />
	<cfset LOCAL.dest = expandPath("./files")>
    <cfif not directoryExists(LOCAL.dest)>
          <cfdirectory action = "create" directory = "#LOCAL.dest#">
     </cfif>
	<cftry>
	     <cffile action = "upload" fileField = "form.fileupload" destination = "#dest#"  nameConflict = "makeUnique" result = "thisResult">
	     <cfset LOCAL.size=#LSNumberFormat(thisResult.fileSize /1024 /1024,0.00)#>
	     <cfif thisResult.CONTENTSUBTYPE NEQ 'pdf'>
	         <cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
	         <cfset LOCAL.errorMessages.errUpload = "upload a valid pdf" />
	     <cfelseif size LT 0.02>
	         <cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
	         <cfset LOCAL.errorMessages.errUpload = "upload file is too small" />
	     <cfelseif size GT 5>
	         <cffile action="DELETE" file="#LOCAL.dest#./#thisResult.ServerFile#" />
	         <cfset LOCAL.errorMessages.errUpload="upload file is too big" />
	     <cfelseif structCount(errorMessages) NEQ 2>
	     	 <cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
	     <cfelse>
	     	<cffile action = "rename" source = "#LOCAL.dest#./#thisResult.ServerFile#" destination = "#LOCAL.dest#./#hash(thisResult.ServerFile,'MD5','UTF-8')##hash(timeformat(now(),'hh_mm_ss'),'MD5','UTF-8')#.pdf">
	     	<cfset LOCAL.fileDirectory = "#LOCAL.dest#./#hash(thisResult.ServerFile,'MD5','UTF-8')##hash(timeformat(now(),'hh_mm_ss'),'MD5','UTF-8')#.pdf" />
	     </cfif>
	    <cfcatch type = "any">
		    <cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cffile action = "DELETE" file = "#LOCAL.dest#./#thisResult.ServerFile#" />
		    <cfset LOCAL.errorMessages.errUpload = "please provide a valid upload file">
	    </cfcatch>
	</cftry>
	<cfif StructCount(errorMessages) EQ 2 >
		<cfset LOCAL.errorMessages.success = application.dataInsertion.insertUploadAssig(arguments.assignmentId,arguments.enrollmentId,LOCAL.fileDirectory) />
	</cfif>
	<cfif StructCount(errorMessages) EQ 2 AND !LOCAL.errorMessages.success>
		<cfset LOCAL.errorMessages.response = false />
	</cfif>
	<cfreturn LOCAL.errorMessages />
</cffunction>

<cffunction name = "validateReview" returnFormat = "json" access = "remote" output = "false" returntype = "struct"
	description = "this function is used to validate review fields">
	<cfargument name = "submissionId" type = "numeric" required = "true" />
	<cfargument name = "assignmentId" type = "numeric" required = "true" />
	<cfargument name = "inputMarks" type = "string" required = "true" />
	<cfargument name = "inputReview" type = "string" required = "true" />
	<cfset LOCAL.errorMessages.success=false />
	<cfset LOCAL.errorMessages.response = true />
	<cfif trim(arguments.inputMarks) EQ '' OR NOT isValid('regex', trim(arguments.inputMarks), "^([1-9][0-9]?|100)$") >
		<cfset LOCAL.errorMessages.errMarks = 'please provide a valid marks' />
	</cfif>
	<cfif trim(arguments.inputReview) EQ '' OR len(trim(arguments.inputReview)) GT 500 >
		<cfset LOCAL.errorMessages.errReview = 'please provide a valid course name' />
	</cfif>
	<cfif StructCount(LOCAL.errorMessages) EQ 2 >
		<cfset LOCAL.errorMessages.success = application.dataInsertion.reviewInsertion(arguments.submissionId, arguments.assignmentId, arguments.inputMarks, arguments.inputReview) />
	</cfif>
	<cfif StructCount(LOCAL.errorMessages) EQ 2 AND !LOCAL.errorMessages.success>
		<cfset LOCAL.errorMessages.response = false />
	</cfif>
	<cfreturn LOCAL.errorMessages />
</cffunction>

<cffunction name = "submissionDetails" access = "remote" returnFormat = "json" output = "false" returntype = "struct">
	<cfargument name = "submissionId" type = "numeric" required = "true" />
	<cfargument name = "assignmentId" type = "numeric" required = "true" />
	<cfset LOCAL.ids= structNew()>
	<cfset LOCAL.ids.submissionId = arguments.submissionId />
	<cfset LOCAL.ids.assignmentId = arguments.assignmentId />
	<cfreturn LOCAL.ids />
</cffunction>

<cffunction name = "removeStopWords" access = "remote" output = "false" returntype = "string">
	<cfargument name = "input" required = "true" />
	<cfset arguments.input = ListChangeDelims(arguments.input," ",",|-_'")>
	<cfset LOCAL.stopWords = "a,able,about,across,after,all,almost,also,am,among,an," &
			"and,any,are,as,at,be,because,been,but,by,can,cannot,could,dear,did," &
			"do,does,either,else,ever,every,for,from,get,got,had,has,have,he,her," &
			"hers,him,his,how,however,i,if,in,into,is,it,its,just,least,let,like," &
			"likely,may,me,might,most,must,my,neither,no,nor,not,of,off,often,on," &
			"only,or,other,our,own,rather,said,say,says,she,should,since,so,some," &
			"than,that,the,their,them,then,there,these,they,this,tis,to,too,twas," &
			"us,wants,was,we,were,what,when,where,which,while,who,whom,why,will," &
			"with,would,yet,you,your">
	<cfset LOCAL.stripped = arguments.input & " " />
	<cfloop list = "#arguments.input#" index = "word" delimiters = " ">
		<cfif ListFind(LOCAL.stopWords, word)>
			<cfset LOCAL.stripped = Replace(LOCAL.stripped, " " & word & " ", " ", "ALL") />
		</cfif>
	</cfloop>
	<cfset LOCAL.stripped = REReplaceNoCase(LOCAL.stripped, "[\s]+", " ", "ALL") />
	<cfset LOCAL.stripped = listRemoveDuplicates(LOCAL.stripped, " ", true) />
	<cfreturn trim(LOCAL.stripped) />
</cffunction>

</cfcomponent>