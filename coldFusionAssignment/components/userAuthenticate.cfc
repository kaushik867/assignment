<!---
  --- NewCFComponent
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/05/20
  --->
<!---log in authrnticate --->

<cfcomponent output = "false" description = "This function used to vaidate the login Form data">
	<cffunction name = "validateLogInUser" returnFormat = "json" access="remote" output = "false" returntype = "struct">
		<cfargument name = "inputLogInUserName" type = "string" required = "true" />
		<cfargument name = "inputLogInPassword" type = "string" required = "true" />
		<cfset LOCAL.errorMessages = structNew() />
		<cfset LOCAL.errorMessages.success = false />
		<cfset LOCAL.errorMessages.response = true />
		<cfif trim(arguments.inputLogInUserName) EQ '' OR len(trim(arguments.inputLogInUserName)) GT 20>
			<cfset LOCAL.errorMessages.errLogInUserName = 'please provide a valid user name '/>
		</cfif>
		<cfif arguments.inputLogInPassword EQ '' OR NOT isValid('regex',arguments.inputLogInPassword, "^(?=.*[0-9])(?=.*[&*$'#'%@])[a-zA-z0-9&*$'#'%@]{8,15}$" ) >
			<cfset LOCAL.errorMessages.errLogInPassword = 'please provide a valid password '/>
		</cfif>
		<cfif StructCount(LOCAL.errorMessages) EQ 2>
			<cfset LOCAL.logInUserError = application.dbOperation.password(arguments.inputLogInUserName,arguments.inputLogInPassword)>
			<cfset LOCAL.errorMessages.success=doLogin(arguments.inputLogInUserName,arguments.inputLogInPassword) />

		<cfif !LOCAL.errorMessages.success AND structKeyExists(LOCAL.logInUserError,'error')>
			<cfset LOCAL.errorMessages.response = false />
		<cfelseif !LOCAL.errorMessages.success OR StructCount(LOCAL.errorMessages) GT 2>
			<cfset LOCAL.errorMessages.error = 'Invalid User name or password'/>
		</cfif>
		</cfif>
		<cfreturn LOCAL.errorMessages />
	</cffunction>

	<!---doLogin() method--->
	 <cffunction name = "doLogin" access = "public" output = "false" returntype = "boolean">

 		<cfargument name = "inputLogInUserName" type = "string" required = "true" />
  		<cfargument name = "inputLogInPassword" type = "string" required = "true" />

 		<cfset LOCAL.isUserLoggedIn=false />
		<cftry>
 			<cfset LOCAL.userLogIn=application.dbOperation.password(arguments.inputLogInUserName,arguments.inputLogInPassword)>
 	 		<cfif LOCAL.userLogIn.recordCount EQ 1 AND !structKeyExists(LOCAL.userLogIn,'error')>
			<cflogin>
				<cfloginuser name="#LOCAL.userLogIn.USERNAME#" password="#LOCAL.userLogIn.PASSWORD#" roles="#LOCAL.userLogIn.PERSONTYPE#" >
			</cflogin>

			<!---Save user data in the session scope--->
			<cfset session.stLoggedInUser = {'userFirstName' = LOCAL.userLogIn.firstName, 'userLastName' = LOCAL.userLogIn.lastName, 'userName' = LOCAL.userLogIn.userName, 'userId' = LOCAL.userLogIn.userId} />
			<!---change the isUserLoggedIn variable to true--->
			<cfset LOCAL.isUserLoggedIn = true />
 			</cfif>
		<cfcatch type="any">
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
			<cfreturn LOCAL.isUserLoggedIn />
		</cfcatch>
		</cftry>
 		<cfreturn LOCAL.isUserLoggedIn />
 	</cffunction>

 	<!---doLogout() method--->
 	<cffunction name = "doLogout" access = "public" output = "false" returntype = "void">
 		<!---delete user data from the session scope--->
 		<cfset structdelete(session,'stLoggedInUser') />
 		<!---Log the user out--->
 		<cflogout />
 	</cffunction>

</cfcomponent>