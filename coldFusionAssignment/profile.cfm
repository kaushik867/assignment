<!---
  --- profile.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/10/20
  --->
<!---header--->
<cfif !structKeyExists(session,'stLoggedInUser')>
	<cflocation url = "Index.cfm">
</cfif>
<cf_header>
<cftry>
<cfset VARIABLES.user = application.dbOperation.userName(session.stloggedinuser.userName)>
<cfset VARIABLES.email = application.dbOperation.getEmail(variables.user.userId)>
<cfcatch>
	<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
</cfcatch>
</cftry>
<div class = "welcome">
	<cfinclude template = "includes/welcome.cfm">
</div>
<div class = "wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
<div class = "container con">
		<div class = "panel panel-default profile">
			<div class = "panel-heading" style = "font-weight:bold;">
				Update Profile
			</div>
 			<div class = "alert alert-success">
				<a href = "#" class = "close" data-dismiss = "alert" aria-label = "close">
					&times;
 				</a>
				<strong>
 					Success!
 				</strong>
 				Profile successfully saved
 			</div>
			<div class = "alert alert-danger">
				<strong>
						(*) Editing data password is required
				</strong>
 			</div>
 			<div class = "panel-body">
 				<div class = "form-horizontal">
					<form id = "updateForm" method = "POST">
						<cfoutput>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								First Name
							</label>
							<div class = "col-sm-6">
								<input class = "form-control" type = "text" name = "inputFirstName"
									placeholder = "first name" value = "#VARIABLES.user.firstName#">
								<span class="errMsg <cfoutput>#uCase('errFirstName')#</cfoutput>"></span>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Middle Name
							</label>
							<div class = "col-sm-6">
								<input class = "form-control" type = "text" name = "inputMiddleName"
									placeholder = "middle name" value = "#VARIABLES.user.middleName#">
								<span class = "errMsg <cfoutput>#uCase('errMiddleName')#</cfoutput>"></span>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Last Name
							</label>
							<div class = "col-sm-6">
								<input class = "form-control" type = "text" name = "inputLastName"
									placeholder = "last name" value="#VARIABLES.user.lastName#" >
								<span class = "errMsg <cfoutput>#uCase('errLastName')#</cfoutput>"></span>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Gender
							</label>
							<div class = "col-sm-6">
								<input class = "form-control disable" type = "text" name="inputGender"
									placeholder = "gender" value = "#VARIABLES.user.gender#" disabled>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Email
							</label>
							<div class = "col-sm-6">
								<input class = "form-control disable" type = "text" name = "inputEmail"
									placeholder = "email" value = "#VARIABLES.email.commValue#" disabled>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Date of Birth
							</label>
							<div class = "col-sm-6">
								<input class = "form-control disable" type = "text" name = "inputDob"
									placeholder = "date of birth" value = "#VARIABLES.user.dateOfBirth#" disabled>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Profession
							</label>
							<div class = "col-sm-6">
								<input class = "form-control disable" type = "text" name = "inputProfession"
									placeholder = "profession" value = "#VARIABLES.user.personType#" disabled>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								User Name
							</label>
							<div class = "col-sm-6">
								<input class = "form-control disable" type = "text" name = "inputUserName"
									placeholder = "profession" value = "#VARIABLES.user.userName#" disabled>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Old password
							</label>
							<div class = "col-sm-6">
								<input class = "form-control pass" type = "password" name = "oldPassword"
									placeholder = "old password" >
							<span class = "errMsg <cfoutput>#uCase('errOldPass')#</cfoutput>"></span>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								New password
							</label>
							<div class = "col-sm-6">
								<input class = "form-control" type = "password" name = "inputPassword"
									placeholder = "new password" id = "newPass" class = "pass">
							<span class="errMsg <cfoutput>#uCase('errPassword')#</cfoutput>"></span>
							</div>
						</div>
						<div class = "form-group">
							<label class = "col-sm-4 control-label">
								Confirm password
							</label>
							<div class = "col-sm-6">
								<input class = "form-control" type = "password" name = "inputConfirmPassword"
									placeholder = "confirm password" class = "pass">
							<span class = "errMsg <cfoutput>#uCase('errConfirmPassword')#</cfoutput>"></span>
							</div>
						</div>
						<div class = "form-group">
 							<div class = "col-sm-offset-4 col-sm-6">
 								<button type = "submit" class = "btn btn-primary" id = "updateBtn" >
 									Update
 								</button>
								<button class = "btn btn-primary editBtn"  type = "button" >
									Edit
 								</button>
							</div>
						</div>
						</cfoutput>
 					</form>
				</div>
				<!-- end form-horizontal -->
 			</div>
 			<!-- end panel-body -->
		</div>
 		<!-- end panel -->
</div>
 	<!-- end size -->
</div>
<!---footer--->
<cfinclude template = "includes/footer.cfm" />
