<!---
  --- dashboard.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/15/20
  --->

<!---header--->
<cfif !structKeyExists(session,'stLoggedInUser')>
	<cflocation url = "Index.cfm">
</cfif>

<cfif isUserInRole('teacher')>
	<cf_header jsFile = "Javascript/teacherGraph.js">
<cfelse>
	<cf_header jsFile = "Javascript/studentGraph.js">
</cfif>
<div class = "welcome">
	<cfinclude template = "includes/welcome.cfm" />
</div>
<div class = "wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
	<cfif isUserInRole('student')>
		<div class = "side">
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					Available Courses
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/add-property.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href = "course.cfm">
						click here
					</a>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end size -->
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					Enrolled Courses
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/edit-property.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href = "enrolledCourses.cfm">
						click here
					</a>
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					Performance
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/test-passed.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href = "performance.cfm">
						click here
					</a>
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					Review
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/popular-topic.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href = "review.cfm">
						click here
					</a>
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default stats" >
				<div class = "panel-heading" style = "font-weight:bold;">
					Graph
				</div>
				<div class = "panel-body dashIcon">
					<canvas id = "pie-chart1"></canvas>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default stats" >
				<div class = "panel-heading" style = "font-weight:bold;">
					Total course enroll in month
				</div>
				<div class = "panel-body dashIcon">
					<canvas class="bar-chart"></canvas>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	</div>
<cfelseif isUserInRole('teacher')>
		<div class = "side">
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					Add Courses
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/add-property.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href="" data-target = "#addCourse" data-toggle = "modal">Click here</a>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end size -->
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					Add Assignmets
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/edit-property.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href="" data-target = "#addAssignment" data-toggle = "modal">Click here</a>
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					My courses
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/test-passed.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href = "mycourse.cfm">
						click here
					</a>
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default dashboard">
				<div class = "panel-heading" style = "font-weight:bold;">
					my Assignments
				</div>
				<div class = "panel-body dashIcon">
					<img src = "https://img.icons8.com/color/96/000000/popular-topic.png"/>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
					<a href = "myAssignment.cfm">
						click here
					</a>
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default stats" >
				<div class = "panel-heading" style = "font-weight:bold;">
					 Total uploaded courses and enroll details
				</div>
				<div class = "panel-body dashIcon">
					<canvas id = "pie-chart2"></canvas>
					<!-- end form-horizontal -->
				</div>
				<div class="panel-footer" style="font-weight:bold;">
				</div>
				<!-- end panel-body -->
			</div>
			<div class = "panel panel-default stats" >
				<div class = "panel-heading" style = "font-weight:bold;">
					Total course enroll in month
				</div>
				<div class = "panel-body dashIcon">
					<canvas class = "bar-chart"></canvas>
					<!-- end form-horizontal -->
				</div>
				<div class = "panel-footer" style = "font-weight:bold;">
				</div>
				<!-- end panel-body -->
			</div>
		</div>
	</div>
</cfif>
<cftry>
	<cfinclude template = "includes/addCourse.cfm" />
	<cfinclude template = "includes/addAssignment.cfm" />
<cfcatch>
	<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
</cfcatch>
</cftry>

<cfinclude template = "includes/footer.cfm" />