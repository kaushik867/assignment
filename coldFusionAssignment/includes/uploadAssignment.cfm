<div class = "modal" id = "uploadAssignmentDialog">
	<div class = "modal-dialog">
		<div class = "modal-content">
			<!-- Modal Header -->
			<div class = "modal-header">
				<h2 class = "modal-title">
					Upload Assignment
				</h2>
				<button type = "button" class = "close" data-dismiss = "modal">
					&times;
				</button>
			</div>
			<!-- Modal body -->
			<div class = "modal-body">
			<span class = "important">(*) astrick indicate mandatory fields</span>
				<form id = "uploadAssignmentForm" method = "POST" >
					<cfoutput>
						<input type = "text" name = "<cfoutput>#uCase('enrollmentId')#</cfoutput>" hidden>
						<input type = "text" name = "<cfoutput>#uCase('assignmentId')#</cfoutput>" hidden>
					</cfoutput>
					<div class = "form-group">
						<p class = "info">
							<i class = "fa fa-info-circle">
							</i>
							<span class = "tool">
								* Uploaded file must be in pdf fromat
								<br>
								* Size should be in between 10 kb-5 MB
							</span>
						</p>
						<label for = "exampleFormControlFile1">
							Upload file <span class = "important">*</span>
						</label>
						<input type = "file" class = "form-control-file" name = "fileUpload" id = "uploadFile" placeholder = "upload">
						<span class = "errMsg <cfoutput>#uCase('errUpload')#</cfoutput>">
						</span>
					</div>
					<div class = "modal-footer">
						<button type = "button" class = "btn btn-secondary" data-dismiss = "modal">
							No
						</button>
						<button type = "submit" class = "btn btn-primary" id = "upload">
							Yes
						</button>
					</div>
				</form>
			</div>
			<!-- Modal footer -->
		</div>
	</div>
</div>
