<div class = "filterDiv">
	<form name = "filterForm" method = "POST">
		<select name = "selectItem" class = "selectBox fa" id = "filterForm">
				<option class = "fa" value = ""> &#xf0b0 Filter </option>
				<option class = "fa" value = "name"> &#xf062 By name asc(default) </option>
				<option class = "fa" value = "name desc"> &#xf063 By name desc </option>
				<option class = "fa" value = "createdTime"> &#xf062 By date asc </option>
				<option class = "fa" value = "createdTime desc"> &#xf063 By date Desc </option>
		</select>
		<button type = "submit" name = "filterBtn" class = "btn btn-primary"> Filter </button>
	</form>
	</div>