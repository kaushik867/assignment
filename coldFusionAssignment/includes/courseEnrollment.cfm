<div class = "modal" id = "CourseEnrollDialog">
	<div class = "modal-dialog">
		<div class = "modal-content">
			<!-- Modal Header -->
			<div class = "modal-header">
				<h2 class = "modal-title w-100 text-center">
					Confirm
				</h2>
				<button type = "button" class = "close" data-dismiss = "modal">
					&times;
				</button>
			</div>
			<!-- Modal body -->
			<div class = "modal-body">
				<form id = "courseEnrollForm" method = "POST">
					<cfoutput>
						<input tyep = "text" name = "userId" value = "#session.stLoggedInUser.userId#" hidden>
						<input type = "text" name = "<cfoutput>#uCase('courseId')#</cfoutput>" hidden>
					</cfoutput>
					<div class = "modal-footer">
						<button type = "button" class = "btn btn-secondary" data-dismiss = "modal">
							No
						</button>
						<button type = "submit" class = "btn btn-primary" id = "yes">
							Yes
						</button>
					</div>
				</form>
			</div>
			<!-- Modal footer -->
		</div>
	</div>
</div>
