<!---
  --- addcourse.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/17/20
  --->
<div class = "modal" id = "addCourse">
    <div class = "modal-dialog">
      <div class = "modal-content">
        <!-- Modal Header -->
	        <div class = "modal-header">
	          <h4 class = "modal-title"> Add course </h4>
	          <button type = "button" class = "close" data-dismiss = "modal"> &times; </button>
	        </div>

	        <!-- Modal body -->
	        <div class = "modal-body">
	         <span class = "important">(*) astrick indicate mandatory fields</span>
	         <form id = "addCourseForm" method = "POST">
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Course name can contains any character <br>
						* Length must be in between 1-50 characters long
					</span></p>
					<label for = "inputCourseName">
						Course Name <span class = "important">*</span>
					</label>
					<input type = "text" class = "input-control" name = "inputCourseName" placeholder = "course name">
					<span class = "errMsg <cfoutput>#uCase('errCourseName')#</cfoutput>" id = "errCourseName">
					</span>
				</div>
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Desription can contains any character <br>
						* Length must be in between 1-500 characters long
					</span></p>
				  <label for = "comment">
					  Description <span class = "important">*</span>
				  </label>
				  <textarea class = "input-control" rows = "3" name = "inputCourseDescription" placeholder = "course description"></textarea>
				  <span class = "errMsg <cfoutput>#uCase('errCourseDescription')#</cfoutput>" id = "errCourseDescription">
				</div>
				<div class = "modal-footer">
	          		<button type = "button" class = "btn btn-secondary" data-dismiss ="modal"> Close </button>
			  		<button type = "submit" id = "addCourseBtn" class = "btn btn-primary"> Submit </button>
	        	</div>
			</form>
	        </div>
	        <!-- Modal footer -->
      </div>
	</div>
</div>