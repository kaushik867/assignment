<!---
	--- welcome.cfm
	--- --------------
	---
	--- author: kaushik
	--- date:   4/15/20
	--->
<cftry>
	<cfset user = application.dbOperation.userName(session.stloggedinuser.userName)>
	<cfset notification = application.dbOperation.notification(session.stloggedinuser.userId)>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>
<div class = "header-right welcomeName">
	<a class = "name" href="Index.cfm">
		<cfoutput>
			Welcome #user.firstName# #user.lastName#!
		</cfoutput>
	</a>
	<div class = "btn-group dropleft">
		<button class = "btn btn-dark notification dropdown-toggle" type = "button" id = "dropdownMenu2" data-toggle = "dropdown"
		aria-haspopup = "true" aria-expanded = "false">
			<span>
				<i class = "fa fa-bell fa-2x">
				</i>
			</span>
			<cfif notification.recordCount NEQ 0 >
				<span class = "badge">
					<cfoutput>
						#notification.recordCount#
					</cfoutput>
				</span>
			</cfif>
		</button>
		<div class = "dropdown-menu" aria-labelledby = "dropdownMenu2">
			<cfif notification.recordCount EQ 0 >
				<button class = "dropdown-item" type = "button">
					No new notifiaction is available now
				</button>
			</cfif>
			<cfoutput query = "notification" startrow = "1" maxrows = "#5#">
				<span class = "notificationBlock">
					<input type = "text" value = "#notification.historyId#" name = "history" class = "historyId"  hidden>
					<cfif notification.description NEQ "">
						<a href = "review.cfm">
							<button class = "dropdown-item notify" type = "button">
								#notification.comment# by #notification.name#
								<br><br>
								#dateFormat(notification.createdDate, 'dd-mmm')# #LSTimeFormat(notification.createdDate)#
							</button>
						</a>
					<cfelseif notification.courseId NEQ "">
						<a href = "enrolledCourses.cfm">
							<button class = "dropdown-item notify" type = "button">
								#notification.comment# by #notification.name#
								<br><br>
								#dateFormat(notification.createdDate, 'dd-mmm')# #LSTimeFormat(notification.createdDate)#
							</button>
						</a>
					<cfelse>
						<a href = "myAssignment.cfm">
							<button class = "dropdown-item notify" type = "button">
								#notification.comment# by #notification.name#
								<br><br>
								#dateFormat(notification.createdDate, 'dd-mmm')# #LSTimeFormat(notification.createdDate)#
							</button>
						</a>
					</cfif>

				</span>
			</cfoutput>
		</div>
	</div>
</div>
