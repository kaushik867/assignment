<div class = "modal details" id = "assignmentDetails">
	<div class = "modal-dialog">
		<div class = "modal-content">
			<!-- Modal Header -->
			<div class = "modal-header">
				<h2 class = "modal-title text-center">
					Assignment Details
				</h2>
				<button type = "button" class = "close" data-dismiss = "modal">
					&times;
				</button>
			</div>
			<!-- Modal body -->
			<div class = "modal-body">
			<div>
				<strong> Name </strong><br>
				<p class = "NAME content"></p>
			</div>
			<div>
				<strong> Description </strong><br>
				<p class = "DESCRIPTION content"></p>
			</div>
			</div>
			<!-- Modal footer -->
			<div class = "modal-footer">
			</div>
		</div>
	</div>
</div>