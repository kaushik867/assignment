<!---
  --- addReview.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/30/20
  --->
<div class = "modal" id = "addReviewDialog">
    <div class = "modal-dialog">
      <div class = "modal-content">
        <!-- Modal Header -->
	        <div class = "modal-header">
	          <h4 class = "modal-title"> Add review </h4>
	          <button type = "button" class = "close" data-dismiss="modal"> &times; </button>
	        </div>

	        <!-- Modal body -->
	        <div class = "modal-body">
	        <span class = "important">(*) astrick indicate mandatory fields</span>
	         <form id = "addReviewForm" method = "POST">
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"> </i><span class="tool">
						* Marks can contains only integer <br>
						* Length must be in between 1-3 characters long
					</span></p>
					<label for = "inputMarks">
						Marks <span class = "important">*</span>
					</label>
					<input type = "text" class = "input-control" name = "inputMarks" placeholder = "marks">
					<span class = "errMsg <cfoutput> #uCase('errMarks')# </cfoutput>" >
					</span>
				</div>
				<input type = "text" name = "<cfoutput>#uCase('submissionId')#</cfoutput>" hidden>
				<input type = "text" name = "<cfoutput>#uCase('assignmentId')#</cfoutput>" hidden>
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Desription can contains any character <br>
						* Length must be in between 1-500 characters long
					</span></p>
				  <label for = "inputReview">
					  Review <span class = "important">*</span>
				  </label>
				  <textarea class = "input-control" rows = "3" name = "inputReview" placeholder = "review"></textarea>
				  <span class = "errMsg <cfoutput>#uCase('errReview')#</cfoutput>" >
				</div>
				<div class = "modal-footer">
	          		<button type = "button" class = "btn btn-secondary" data-dismiss = "modal"> Close </button>
			  		<button type = "submit" id = "addReviewBtn" class = "btn btn-primary"> Submit </button>
	        	</div>
			</form>
	        </div>
	        <!-- Modal footer -->
      </div>
	</div>
</div>