<div class = "modal details" id = "reviewDetails">
	<div class = "modal-dialog">
		<div class = "modal-content">
			<!-- Modal Header -->
			<div class = "modal-header">
				<h2 class = "modal-title w-100 text-center">
					Review Details
				</h2>
				<button type = "button" class = "close" data-dismiss = "modal">
					&times;
				</button>
			</div>
			<!-- Modal body -->
			<div class = "modal-body">
			<div>
				<strong> Course name </strong><br>
				<p class = "COURSENAME content"></p>
			</div>
			<div>
				<strong> Assignment name </strong><br>
				<p class = "NAME content" ></p>
			</div>
			<div>
				<strong> Description </strong><br>
				<p class = "DESCRIPTION content"></p>
			</div>
			<div>
				<strong> Marks </strong><br>
				<p class = "MARKS content"></p>
			</div>

			<div>
				<strong> Added by </strong><br>
				<p class = "TEANAME content"></p>
			</div>
			</div>
			<!-- Modal footer -->
			<div class = "modal-footer">
			</div>
		</div>
	</div>
</div>