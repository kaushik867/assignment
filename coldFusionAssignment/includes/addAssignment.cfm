<!---
  --- addassignment.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/15/20
  --->

<cftry>
	<cfset  VARIABLES.allCourses = application.dbOperation.coursesDetails()>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>
<!--- <cfif structkeyExists(VARIABLES.allCourses.'error')> --->
<!--- 	<h2> --->
<!--- 		Something went wrong please try later --->
<!--- 	<h2> --->
<!--- <cfelse> --->
<div class = "modal" id = "addAssignment">
    <div class = "modal-dialog">
      <div class = "modal-content">
        <!-- Modal Header -->
	        <div class = "modal-header">
	          <h4 class = "modal-title"> Add Assignment</h4>
	          <button type = "button" class = "close" data-dismiss = "modal"> &times; </button>
	        </div>

	        <!-- Modal body -->
	        <div class = "modal-body">
	        <span class = "important">(*) astrick indicate mandatory fields</span>
	         <form name = "addAssignment" id = "addAssignmentForm" method = "POST">
		         <div class="form-group">
				 	<label>
						Course Name <span class = "important">*</span>
					</label>
					<select class = "form-control" name = "inputCourseName" placeholder = "course name">
						<option value = "" selected>
						----select----
						</option>
						<cfoutput query = "allCourses">
						<option value = "#allCourses.courseId#">
							#allCourses.name#
						</option>
						</cfoutput>
					</select>
					<span class = "errMsg <cfoutput>#uCase('errCourseName')#</cfoutput>">
					</span>
				</div>
				<div class="form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Assignment name can contains any character <br>
						* Length must be in between 1-50 characters long
					</span></p>
					<label for = "inputConfirmPassword">
						Assignment Name <span class = "important">*</span>
					</label>
					<input type = "text" class = "form-control" name = "inputAssignmentName" placeholder = "assignment name">
					<span class = "errMsg <cfoutput>#uCase('errAssignmentName')#</cfoutput>">
					</span>
				</div>
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Description name can contains any character <br>
						* Length must be in between 1-500 characters long
					</span></p>
				  <label for = "comment">
					  Description <span class = "important">*</span>
				  </label>
				  <textarea class = "form-control" rows = "3" name = "description" placeholder = "description"></textarea>
				  <span class = "errMsg <cfoutput>#uCase('errDescription')#</cfoutput>"> </span>
				</div>
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Start date select from above<br>
						* It should be smaller than end Date
					</span></p>
					<label for = "inputDob">
						Start date <span class = "important">*</span>
					</label>
					<input type = "text" class = "form-control datePicker" id = "startDate" name = "inputStartDate" placeholder = "start date" autocomplete = "off">
					<span class = "errMsg <cfoutput>#uCase('errStartDate')#</cfoutput>"></span>
				</div>
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Start date select from above<br>
						* It should be greater than end Date
					</span></p>
					<label for = "inputDob">
						End date <span class = "important">*</span>
					</label>
					<input type = "text" class = "form-control datePicker" id = "endDate" name = "inputEndDate" placeholder = "end date" autocomplete = "off">
					<span class="errMsg <cfoutput>#uCase('errEndDate')#</cfoutput>"></span>
				</div>
				<div class = "form-group">
					<p class = "info"><i class = "fa fa-info-circle"></i><span class = "tool">
						* Uploaded file must be in pdf fromat<br>
						* Size should be in between 10kb -5 MB
					</span></p>
    				<label for = "exampleFormControlFile1">
						Upload file <span class = "important">*</span>
					</label>
    				<input type = "file" class="form-control-file" name = "fileUpload" id = "uploadFile" placeholder="upload">
					<span class = "errMsg <cfoutput>#uCase('errUpload')#</cfoutput>"></span>
  				</div>
				<div class = "modal-footer">
	          		<button type = "button" class = "btn btn-secondary" data-dismiss = "modal"> Close </button>
			  		<button type = "submit" id = "addAssignmentBtn" class = "btn btn-primary"> Submit </button>
	       		</div>
			</form>
	        </div>
	        <!-- Modal footer -->
      </div>
	</div>
</div>
<!--- </cfif> --->