<div class = "modal details" id = "details">
	<div class = "modal-dialog">
		<div class = "modal-content">
			<!-- Modal Header -->
			<div class = "modal-header">
				<h2 class = "modal-title text-center">
					Course Details
				</h2>
				<button type = "button" class = "close" data-dismiss = "modal">
					&times;
				</button>
			</div>
			<!-- Modal body -->
			<div class = "modal-body">
			<div>
				<strong>Course name </strong><br>
				<p class = "NAME content"></p>
			</div>
			<div>
				<strong> Course description </strong><br>
				<p class = "DESCRIPTION content"></p>
			</div>

			<div>
				<strong> Created by </strong><br>
				<p class= "FIRSTNAME content"></p>
				<p class="LASTNAME"></p>
			</div>
			</div>
			<!-- Modal footer -->
			<div class = "modal-footer">

			</div>
		</div>
	</div>
</div>