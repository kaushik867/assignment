<!---
   --- footer.cfm
   --- --------------
   ---
   --- author: kaushik
   --- date:   3/24/20 --->

				<div class = "footer">
					<div class = "links">
							<a> contact us </a> <br>
							 <i class = "fa fa-envelope" aria-hidden="true"></i>e_assignment@gmail.com <br>
							 <i class = "fa fa-phone" aria-hidden="true"></i> 1800119988 <br>
							 <i class = "fa fa-skype" style="font-size:20px" aria-hidden="true">live:.cid.550f1793bdb088f9</i>
					</div>
					<div class = "links">
							<a> About us </a><br>
							<p>
							School teachers, college instructors, and university professors have many
							things in common - and one of them is homework. If you're looking for a
							simple way to handle student homework assignments, then our Online
							Homework Submission Form is the solution you've been searching
							</p>
					</div>
					<div class = "icons">
						<i class = "fa fa-facebook"></i>
						<i class = "fa fa-twitter"></i>
						<i class="fa fa-instagram"></i>
						<p>
							follow us on
						</p>
					</div>
					<div class = "copyRights">
						<span>
							&copy; Copyright 2020, E-Assignment Tour Corporation
						</span>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
