<!---
	--- header.cfm
	--- --------------
	---
	--- author: kaushik
	--- date:   3/24/20--->
<cfparam name="attributes.jsFile" default="">
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name = "viewport" content = "width=device-width, initial-scale = 1, shrink-to-fit = no">
		<title>
			index
		</title>
		<meta name = "viewport" content="width=device-width, initial-scale=1">
		<link rel = "stylesheet" href = "CSS/Style.css">
		<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel = "stylesheet" href = "//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src = "https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src = "https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
		<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src = "https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<script src = "https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
		<script src = "Javascript/validation.js"></script>
		<script src = "<cfoutput>#attributes.jsFile#</cfoutput>"></script>
		<noscript>
			<input type = "checkbox" class = "modal-closing-trick" id = "modal-closing-trick">
			<!-- lightbox container hidden with CSS -->
			<div id = "nojs" class = "modal2" id = "text">
				<div class = "box1">
					<p class = "title3">
						Website requires javaScrip for work.
					</p>
					<div class = "content2">
						You seem to have Javascript disabled. This site uses safe scripts. So please enable it to be able to use this site.
						<p>
							Need to know how to enable it?
							<a href = "http://enable-javascript.com/" target="_blank">
								Go here.
							</a>
						</p>
					</div>
				</div>
			</div>
			</input>
		</noscript>
	</head>
	<body>
		<div class = "webPage">
		<div class = "header">
			<a href = "Index.cfm" class="logo">
				<img class = "logoImage" src="images/logo.png" alt = "logo.png">
				E-Assignment Tour
			</a>
			<div class = "header-right">
				<a class = "active" href = "Index.cfm">
					Home
				</a>
				<a class = "active" href = "course.cfm">
					Courses
				</a>
				<a class = "active" href = "">
					Contact
				</a>
				<a class = "active" href = "about.cfm">
					About
				</a>
				<cfif structKeyExists(session,'stLoggedInUser') >
					<a class = "btnLink" href = "Index.cfm?logout">
						Logout
					</a>
				<cfelse>
					<a class = "btnLink" href = "Index.cfm#signUp">
						SignUp
					</a>
					<a class = "btnLink" href = "Index.cfm#logIn">
						LogIn
					</a>
				</cfif>
			</div>
		</div>
