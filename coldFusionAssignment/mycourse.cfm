<!---
  --- mycourse.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/19/20
  --->
<cfif !structKeyExists(session,'stLoggedInUser') OR  !isUserInRole('teacher')>
	<cflocation url = "Index.cfm">
</cfif>
<cftry>
	<cfset  VARIABLES.myCourses = application.dbOperation.coursesDetailsByUser("name",session.stLoggedInUser.userId)>
	<cfif structKeyExists(form,'filterBtn') AND !structkeyExists(VARIABLES.myCourses,'error')>
		<cfset  VARIABLES.myCourses = application.dbOperation.coursesDetailsByUser(form.selectItem,session.stLoggedInUser.userId)>
	</cfif>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>

<cfparam name = "pageNum" default = "1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 7>
<cfset VARIABLES.startRow = min( ( pageNum-1 ) * VARIABLES.maxRows+1, max( VARIABLES.myCourses.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.myCourses.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.myCourses.recordCount/7 )>

<!---header--->
<cf_header>
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<cfinclude template = "includes/welcome.cfm" />
	</div>
<div class = "wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
		<div class = "contentBox">
</cfif>
<cfinclude template = "includes/filter.cfm">
		<div class = "showCourse" >
			<cfif VARIABLES.myCourses.recordCount EQ 0>
				<h2> You did not added any course yet</h2>
			<cfelseif structkeyExists(VARIABLES.myCourses,'error')>
				<h2> Something went wrong please try later</h2>
			<cfelse>
				<cf_heading headingTitle = "MY COURSES">
				<div class = "panel panel-default">
					<div class = "panel-body" >
						<table class = "table table-striped">
							<thead>
								<tr>
									<th align = "left"> Course Name </th>
									<th align = "left"> Description </th>
									<th> last Update On </th>
									<th> Created by </th>
								</tr>
							</thead>
							<cfoutput query = "VARIABLES.myCourses" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#">
							<tr>
								<td align = "left"> #left(VARIABLES.myCourses.name,20)# </td>
								<td align = "left"> #left(VARIABLES.myCourses.description,20)# </td>
								<td class = "hidden"> #VARIABLES.myCourses.courseId# </td>
								<td align = "left" width="20%"> #dateFormat(VARIABLES.myCourses.createdTime,'dd-mmm-yy')# #timeFormat(VARIABLES.myCourses.createdTime,'hh:MM')# </td>
								<td align="left"><a href="" class="courseDetails" data-target = "##details" data-toggle = "modal">More Details</a></td>
								<td align = "left"> Created By: #VARIABLES.myCourses.firstName# #VARIABLES.myCourses.lastName# </td>
								<td align = "right">
								<a class = "btn update" data-target = "##updateCourseDialog" data-toggle = "modal"><button type = "button" class = "btn btn-primary">
								Update </button></a>
							</tr>
							</cfoutput>
						</table>
							<!-- end form-horizontal -->
					</div>
				</div>
			</cfif>
					<!-- end size -->

			<div class = "pagination">
				<cfoutput>
					<cfif url.pageNum GT "1" >
						<a href = "?pageNum=#url.pageNum-1#"><button type = "button" class = "btn btn-secondary"> <<--previous </button></a>
					</cfif>
					<cfif url.pageNum LT totalPages>
						<a href = "?pageNum=#url.pageNum+1#"><button type = "button" class = "btn btn-secondary"> Next--> </button></a>
					</cfif>
				</cfoutput>
			</div>
		</div>
		<cfif structKeyExists(session,'stLoggedInUser')>
		</div>
</div>
</cfif>

<div class = "modal updateCourse" id = "updateCourseDialog">
    <div class = "modal-dialog">
      <div class = "modal-content">
        <!-- Modal Header -->
	        <div class = "modal-header">
	          <h4 class = "modal-title"> Update course </h4>
	          <button type = "button" class = "close" data-dismiss = "modal"> &times; </button>
	        </div>
	        <!-- Modal body -->
	        <div class = "modal-body">
	         <form id = "updateCourseForm" method = "POST">
				<div class = "form-group">
					<p class = "info"><i class="fa fa-info-circle"></i><span class = "tool">
						* Confirm password must be same as password
					</span></p>
					<label for = "inputConfirmPassword">
						Course Name
					</label>
					<input type = "text" class = "input-control" name = "<cfoutput>#uCase('inputCourseName')#</cfoutput>" placeholder = "course name" >
					<span class = "errMsg <cfoutput>#uCase('errCourseName')#</cfoutput>" id="errCourseName">
					</span>
				</div>
				<div class = "form-group">
				  <label for = "comment"> Description: </label>
				  <textarea class = "input-control" rows = "3" name = "<cfoutput>#uCase('inputCourseDescription')#</cfoutput>" placeholder = "course description"> </textarea>
				  <span class="errMsg <cfoutput>#uCase('errCourseDescription')#</cfoutput>" id="errCourseDescription">
				</div>
				<input type = "text" class = "input-control" name = "<cfoutput>#uCase('courseId')#</cfoutput>" placeholder = "course name" hidden >
				<div class="modal-footer">
	          		<button type = "button" class = "btn btn-secondary editBtnInput"> Edit </button>
			  		<button type = "submit" class = "btn btn-primary" id = "updateCourseBtn"> Update </button>
	        	</div>
			</form>
	        </div>
	        <!-- Modal footer -->
      </div>
	</div>
</div>
<cfinclude template = "includes/courseDetails.cfm" >
<cfinclude template = "includes/footer.cfm" />