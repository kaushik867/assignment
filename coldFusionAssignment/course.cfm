<!---
	--- course.cfm
	--- --------------
	---
	--- author: kaushik
	--- date:   4/15/20
	--->
<cftry>
	<cfif !structKeyExists(session,'stLoggedInUser') AND !structKeyExists(form,'filterBtn')>
		<cfset  VARIABLES.allCourses = application.dbOperation.CoursesDetails()>
	<cfelseif structKeyExists(session,'stLoggedInUser')>
		<cfset  VARIABLES.allCourses = application.dbOperation.remCoursesDetails(session.stLoggedInUser.userId)>
	</cfif>
		<cfif !structKeyExists(session,'stLoggedInUser') AND structKeyExists(form,'searchBtn') AND form.searchItem NEQ "">
			<cfset VARIABLES.allCourses = application..dbOperation.coursesDetailsSearch(form.searchItem)>
		<cfelseif structKeyExists(session,'stLoggedInUser') AND structKeyExists(form,'searchBtn') AND form.searchItem NEQ "">
			<cfset VARIABLES.allCourses = application.dbOperation.remCoursesDetailsSearch(form.searchItem,session.stLoggedInUser.userId)>
		<cfelseif structKeyExists(session,'stLoggedInUser') AND structKeyExists(form,'filterBtn')>
			<cfset VARIABLES.allCourses = application.dbOperation.remCoursesDetails(session.stLoggedInUser.userId,form.selectItem)>
		<cfelseif !structKeyExists(session,'stLoggedInUser') AND structKeyExists(form,'filterBtn')>
			<cfset  VARIABLES.allCourses = application.dbOperation.CoursesDetails(form.selectItem)>
		</cfif>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>

<cfparam name = "pageNum" default="1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 7>
<cfset VARIABLES.startRow = min( ( url.pageNum-1 ) * VARIABLES.maxRows+1, max( VARIABLES.allCourses.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.allCourses.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.allCourses.recordCount/7 )>
<!---header--->
<cf_header>
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<cfinclude template = "includes/welcome.cfm" />
	</div>
	<div class="wrapper">
		<cfinclude template = "includes/sidebar.cfm" />
		<div class = "contentBox">
</cfif>
<div id = "toolbar">
	<cfinclude template = "includes/filter.cfm">
	<div class = "searchDiv">
			<form name = "search" method = "POST">
				<input type = "text" name = "searchItem" class = "searchBox" placeholder = "Search by course name">
				<button type = "submit" name = "searchBtn" class = "btn btn-primary"> search </button>
			</form>
	</div>
</div>
<div class = "showCourse" >
<cfif VARIABLES.allCourses.recordCount EQ 0>
<h2>
	Please try Again there is No Availabe Course
</h2>
<cfelseif structKeyExists(VARIABLES.allCourses,'error')>
<h2>
	Something went wrong please try again later
</h2>
<cfelse>
	<cf_heading headingTitle = "COURSES">
		<div class = "panel panel-default">
			<div class = "panel-body" >
					<table class = "table table-striped">
						<thead>
							<tr>
								<th> Course Name </th>
								<th> Description </th>
								<th> last Update On </td>
								<th> created by </td>
							</tr>
						</thead>
						<cfoutput query = "VARIABLES.allCourses" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#" group ="name">
						<tr>
							<td class = "clickable" align = "left"> #left(VARIABLES.allCourses.name,20)# </td>
							<td class = "clickable" align = "left" > #left(VARIABLES.allCourses.description,20)# </td>
							<td class = "hidden"> #VARIABLES.allCourses.courseId# </td>
							<td align = "left" width = "20%"> #dateFormat(VARIABLES.allCourses.createdTime,'dd-mmm-yy')# #timeFormat(VARIABLES.allCourses.createdTime,'hh:MM')# </td>
							<td class = "clickable" align = "left"> #VARIABLES.allCourses.firstName# #allCourses.lastName# </td>
							<td align="left"><a href="" class="courseDetails" data-target = "##details" data-toggle = "modal">More Details</a></td>
							<cfif  !structKeyExists(session,'stLoggedInUser') >
								<td align = "right">
								<a class = "btn alert" data-toggle = "modal"><button type = "button" class = "btn btn-primary error">
								Enroll</button></a></td>
							<cfelseif isUserInRole('student')>
							<td align = "right">
							<a class = "btn enroll" data-toggle = "modal"><button type = "button" class = "btn btn-primary">
								Enroll</button></a></td>
							</cfif>
						</tr>
						</cfoutput>
					</table>
					<!-- end form-horizontal -->
				</div>
			</div>

		</cfif>
				<!-- end size -->
				<div class = "pagination">
					<cfoutput>
						<cfif url.pageNum GT "1" >
							<a href = "?pageNum=#url.pageNum-1#">
								<button type = "button" class = "btn btn-secondary">
									<<--previous
								</button>
							</a>
						</cfif>
						<cfif url.pageNum LT totalPages>
							<a href = "?pageNum=#url.pageNum+1#">
								<button type = "button" class = "btn btn-secondary">
									Next-->>
								</button>
							</a>
						</cfif>
					</cfoutput>
				</div>
				</div>
	<cfif structKeyExists(session,'stLoggedInUser')>
			</div>
		</div>
	</div>
		<cftry>
			<cfinclude template = "includes/courseEnrollment.cfm">
		<cfcatch>
			<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
		</cfcatch>
		</cftry>
	</cfif>

	<cfinclude template = "includes/courseDetails.cfm">

	<cfinclude template = "includes/footer.cfm" />
