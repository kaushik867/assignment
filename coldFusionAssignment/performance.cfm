<!---
  --- performance.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/15/20
  --->

<!---header--->
<cfif !structKeyExists(session,'stLoggedInUser') OR !isUserInRole('student')>
	<cflocation url = "Index.cfm">
</cfif>
<cf_header jsFile = "Javascript/performance.js">
<div class = "welcome">
	<cfinclude template = "includes/welcome.cfm" />
</div>
<div class = "wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
		<div class = "side">
			<cf_heading headingTitle = "PERFORMANCE">
			<canvas id = "line-chart"></canvas>
		</div>
</div>

<cfinclude template = "includes/footer.cfm" />