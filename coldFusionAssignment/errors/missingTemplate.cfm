<!DOCTYPE html>
<html lang = "en" dir="ltr">
  <head>
    <meta charset = "utf-8">
    <title></title>
    <link rel = "stylesheet" href = "../coldFusionAssignment/CSS/errorStyle.css">
	<link rel = "stylesheet" href = "../CSS/errorStyle.css">
  </head>
  <body>
    <div class = "container">
      <h2>Oops! Page not found.</h2>
      <h3>404</h3>
      <p>We can't find the page you're looking for.</p>
      <a href = "../Index.cfm">Go back home</a>
    </div>
  </body>
</html>