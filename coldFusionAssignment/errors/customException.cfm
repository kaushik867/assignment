<!DOCTYPE html>
<html lang = "en" dir = "ltr">
  <head>
    <meta charset = "utf-8">
    <title></title>
    <link rel = "stylesheet" href = "../coldFusionAssignment/CSS/errorStyle.css">
	<link rel = "stylesheet" href = "../CSS/errorStyle.css">
  </head>
  <body>
    <div class = "container">
      <h2>Oops! Something went wrong</h2>
      <h3>500</h3>
      <p>Internal server error occured please try again.</p>
      <a href = "../Index.cfm">Go back home</a>
    </div>
  </body>
</html>