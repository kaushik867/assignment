<!---
  --- mycourse.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/19/20
  --->
<cfif !isUserInRole('teacher')>
	<cflocation url = "Index.cfm">
</cfif>

<cftry>
	<cfset  VARIABLES.myAssignment = application.dbOperation.assignmentByUser("name",session.stLoggedInUser.userId)>
	<cfif structKeyExists(form,'filterBtn') AND !structKeyExists(VARIABLES.myAssignment,'error')>
		<cfset  VARIABLES.myAssignment = application.dbOperation.assignmentByUser(form.selectItem,session.stLoggedInUser.userId)>
	</cfif>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>

<cfparam name = "pageNum" default = "1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 7>
<cfset VARIABLES.startRow = min( ( pageNum-1 ) * VARIABLES.maxRows+1, max( VARIABLES.myAssignment.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.myAssignment.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.myAssignment.recordCount/7 )>

<!---header--->
<cf_header>
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<cfinclude template = "includes/welcome.cfm" />
	</div>
<div class="wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
		<div class = "contentBox">
		<div id = "toolbar">
	<cfinclude template = "includes/filter.cfm">
</div>
</cfif>
		<div class = "showCourse" >
			<cfif VARIABLES.myAssignment.recordCount EQ 0>
				<h2> You did not added any assignment yet</h2>
			<cfelseif structKeyExists(VARIABLES.myAssignment,'error')>
				<h2> Somenthing went wrong please try again later</h2>
			<cfelse>
				<cf_heading headingTitle = "ASSIGNMENTS">
				<div class = "panel panel-default">
					<div class = "panel-body" >
						<table class = "table table-striped">
							<thead>
								<tr>
									<th> Assignment Name </th>
									<th> Description </th>
									<th> Last Update On </th>
									<th> Created by </th>
								</tr>
							</thead>
						<cfoutput query = "VARIABLES.myAssignment" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#">
							<tr>
								<td align = "left" class = "clickable" align = "left" onclick = "window.location.href='submitAssignment.cfm?assignmentId=#myAssignment.assignmentId#'"> #left(VARIABLES.myAssignment.name,20)# </td>
								<td align = "left" class = "clickable" align = "left" onclick = "window.location.href='submitAssignment.cfm?assignmentId=#myAssignment.assignmentId#'"> #left(VARIABLES.myAssignment.description,20)# </td>
 								<td align = "right" class = "hiddenAId"> #VARIABLES.myAssignment.assignmentId# </td>
								<td class = "hidden" class = "clickable" align = "left" onclick = "window.location.href='submitAssignment.cfm?assignmentId=#myAssignment.assignmentId#'"> #VARIABLES.myAssignment.courseId# </td>
								<td align = "left" class = "clickable" align = "left" onclick = "window.location.href='submitAssignment.cfm?assignmentId=#myAssignment.assignmentId#'">
								#dateFormat(VARIABLES.myAssignment.lastUpdate,'dd-mmm-yy')# #timeFormat(VARIABLES.myAssignment.lastUpdate,'hh:MM')# </td>
 								<td align = "left" class = "clickable" align = "left" onclick = "window.location.href='submitAssignment.cfm?assignmentId=#myAssignment.assignmentId#'"> #VARIABLES.myAssignment.firstName# #VARIABLES.myAssignment.lastName# </td>
								<td align="left"><a href="" class="assignmentDetails" data-target = "##assignmentDetails" data-toggle = "modal">More Details</a></td>
							</tr>
						</cfoutput>
						</table>
							<!-- end form-horizontal -->
					</div>
				</div>
			</cfif>
				<!-- end size -->
			<div class = "pagination">
				<cfoutput>
					<cfif url.pageNum GT "1" >
						<a href = "?pageNum=#url.pageNum-1#"><button type = "button" class = "btn btn-secondary"> <<--previous </button></a>
					</cfif>
					<cfif url.pageNum LT totalPages>
						<a href = "?pageNum=#url.pageNum+1#"><button type = "button" class = "btn btn-secondary"> Next--> </button></a>
					</cfif>
				</cfoutput>
			</div>
		</div>
		<cfif structKeyExists(session,'stLoggedInUser')>
		</div>
</div>
</cfif>
<cfinclude template = "includes/assignmentDetails.cfm">
<cfinclude template = "includes/footer.cfm" />