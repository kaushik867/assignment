<cf_header>
	<div class = "about">
		<div class = "content"><p>
			<span>Why E-Assignment?</span><br>
		    As the educational world is moving faster and becoming more competitive,
		    almost every university started to use an online submission system, or
		    newer technologies to facilitate their task, to have more time, and to
		    be in pace with this fast moving IT world.
		    Moreover, assignments that are large in terms of pages or volume could
		    easily discourage a student from submitting due to financial constraints
		    brought about by high cost of printing an assignment. All these problems
		    highlighted are the main reasons the researcher is developing an electronic
		    assignment submission system to curtail these challenges and make studying
		    more enjoyable in our tertiary institutions.
	    </p></div>
		<div class = "content">
		    <span>Objective</span>
		    <ul style="line-height:180%">
		        <li>To Create a database that will manage each student assignment submission and allow access by lecturer to access those files submitted by the student.</li>
		        <li>This proposed system is geared towards providing a system to assure equal opportunity and impartial review of student assignment submission.</li>
		      </ul>
		</div>

	    <div class = "content"><p>
		   <span> Assignment: </span> <br> a task or piece of work allocated to someone as part of
		   			a job or course of study.<br>
		    <span> Student: </span> <br> A student or pupil is a learner, or someone who attends an
		    		educational institution.<br>
		    <span> Online: </span> <br> online� indicates a state of connectivity.<br>
		    <span> Submission: </span> <br> the action of presenting a proposal, application, or other
		    		document for consideration or judgement.
		</div></p>
	</div>
<cfinclude template="includes/footer.cfm">