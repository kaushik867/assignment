<!---   --- Index.cfm -->
	<!--   --- -------------- -->
	<!--   --- -->
	<!--   --- author: kaushik -->
	<!--   --- date:   3/24/20 --->

<cfset application.dataInsertion.deleteFiles()>
<!---header--->
<cf_header>
<div class = "bs-example">
<!--- Button HTML (to Trigger Modal) --->
<cfif !structKeyExists(session,'stLoggedInUser')>
	<div id = "logIn" class = "overlay">
		<div class = "popup">
			<div class = "modal-header">
				<h3>
				LogIn</h5>
				<a class = "close" href="">&times;</a>
			</div>
			<!---LogIn form--->
			 <span class = "important">(*) astrick indicate mandatory fields</span>
			<form action = "Index.cfm" id = "logInForm"  method = "POST" name = "LogInForm">
				<div>
					<label for = "inputLogInEmail">
						User name <span class = "important">*</span>
					</label>
					<input type = "text" class = "input-control" name = "inputLogInUserName" placeholder = "user name">
					<span class = "errMsg <cfoutput>#uCase('errLogInUserName')#</cfoutput>" >
					</span>
				</div>
				<div>
					<label for = "inputLogInPassword">
						Password <span class = "important">*</span>
					</label>
					<input type = "password" class = "input-control" name = "inputLogInPassword" placeholder = "password">
					<span class = "errMsg <cfoutput>#uCase('errLogInPassword')#</cfoutput>" >
					</span>
				</div>
				<div class = "modal-footer">
					<span class = "errMsg <cfoutput>#uCase('error')#</cfoutput> " >
					</span>
					<button type = "button" class = "btn btn-secondary"  onclick = "window.location.href = ''">
						Cancel
					</button>
					<button type = "submit" id = "logInSubmitbtn" value = "submit" name = "logInSubmit" class = "btn btn-primary">
						Submit
					</button>
				</div>
			</form>
		</div>
	</div>
</cfif>
<!--- Modal HTML --->
<div id = "signUp" class = "overlay">
	<div class = "popup">
		<div class = "modal-header">
			<h3>
				SignUp
			</h3>
			<a class = "close" href="">
				&times;
			</a>
		</div>
		 <span class = "important">(*) astrick indicate mandatory fields</span>
		<!---Sign Up popup from --->
		<form id = "signUpForm" action = "#logIn" method = "POST" name = "registrationForm" >
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						*First name contains alphabet only with no special characters and spaces
						<br>
						* Length must be in between 1-30 characters long
					</span>
				</p>
				<label for = "inputFirstName">
					First Name <span class = "important">*</span>
				</label>
					<input type = "text" class = "form-control" name = "inputFirstName"  placeholder = "first name">
					<span class = "errMsg <cfoutput>#uCase('errFirstName')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						* Middle name contains alphabet only with no special characters and spaces
						<br>
						* Length must be in between 1-30 characters long
					</span>
				</p>
				<label for = "inputMiddleName">
					Middle Name <span class = "important">*</span>
				</label>
				<input type = "text" class = "form-control" name = "inputMiddleName"  placeholder = "middle name">
				<span class = "errMsg <cfoutput>#uCase('errMiddleName')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						* Last name contains alphabet only with no special characters and spaces
						<br>
						* Length must be in between 1-30 characters long
					</span>
				</p>
				<label for = "inputLastName">
					Last Name <span class = "important">*</span>
				</label>
				<input type = "text" class = "form-control" name = "inputLastName"  placeholder = "last name">
				<span class = "errMsg <cfoutput>#uCase('errLastName')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<label for = "gender">
					Gender <span class = "important">*</span>
				</label>
				<select class = "form-control" name = "inputGender" placeholder = "gender">
					<option value = "" selected>
						----select----
					</option>
					<option value = "M">
						Male
					</option>
					<option value = "F">
						Female
					</option>
					<option value = "O">
						Other
					</option>
				</select>
				<span class = "errMsg <cfoutput>#uCase('errGender')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						* Must be a valid email address
						<br>
						* Start with alphabet only
						<br>
						* Should contain one '@' symbol and one dot(.)
					</span>
				</p>
				<label for = "inputEmail">
					E-mail <span class = "important">*</span>
				</label>
				<input type = "text" class = "form-control" name = "inputEmail"  placeholder = "email">
				<span class = "errMsg <cfoutput>#uCase('errEmail')#</cfoutput>" id = "errEmail">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
					</span>
				</p>
				<label for = "dob">
					Date of Birth <span class = "important">*</span>
				</label>
				<input type = "text" class = "form-control datePicker" id = "dob" name = "inputDob" placeholder = "date of birth" autocomplete = "off">
				<span class = "errMsg  <cfoutput>#UCase('errDob')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<label for = "inputProfession">
					Profession <span class = "important">*</span>
				</label>
				<select class = "form-control" name = "inputProfession" placeholder = "profession">
					<option value = "" selected>
						----select----
					</option>
					<option value = "teacher">
						Teacher
					</option>
					<option value = "student">
						Student
					</option>
				</select>
				<span class = "errMsg <cfoutput>#uCase('errProfession')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						* User name can take any charater and symbols
						<br>
						* Length must be in between 1-20 characters long
					</span>
				</p>
				<label for = "inputUserName">
					User Name  <span class = "important">*</span>
				</label>
				<input type = "text" class = "form-control" name = "inputUserName"  placeholder = "user name">
				<span class = "errMsg <cfoutput>#uCase('errUserName')#</cfoutput>" id="errUserName">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						* Password length should be 8-15 character long
						<br>
						* Password must contain one numeric digit
						<br>
						* Must conatain one special character from any of these
						<br>
						&nbsp;&nbsp; '&','*','$','#','%','@'
					</span>
				</p>
				<label for = "inputPassword">
					Password  <span class = "important">*</span>
				</label>
				<input type = "password" class = "form-control" id = "inPass" name = "inputPassword" placeholder = "password">
				<span class = "errMsg <cfoutput>#uCase('errPassword')#</cfoutput>">
				</span>
			</div>
			<div class = "form-group">
				<p class = "info">
					<i class = "fa fa-info-circle">
					</i>
					<span class = "tool">
						* Confirm password must be same as password
					</span>
				</p>
				<label for = "inputConfirmPassword">
					Confirm Password  <span class = "important">*</span>
				</label>
				<input type = "password" class = "form-control" id = "conPass" name = "inputConfirmPassword" placeholder = "confirm password">
				<span class = "errMsg <cfoutput>#uCase('errConfirmPassword')#</cfoutput>">
				</span>
			</div>
			<div class = "modal-footer">
				<button type = "button" class = "btn btn-secondary"  onclick = "window.location.href = ''">
					Cancel
				</button>
				<button type = "submit"  id = "signUpSubmitButton" class = "btn btn-primary">
					Submit
				</button>
			</div>
		</form>
	</div>
</div>
<!--- <cfif structKeyExists(URL,'logout')> --->
<!--- 	<cfset createObject("component",'DD.coldFusionAssignment.components.userAuthenticate').doLogout() /> --->
<!--- 	<cflocation url = "Index.cfm"> --->
<!--- </cfif> --->
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<div class = "dropdown">
			<button id = "buttonShow">
				<i class = "fa fa-bars" aria-hidden="true">
				</i>
			</button>
			<!--- dropdown content will show--->
		</div>
		<cfinclude template = "includes/welcome.cfm">
	</div>
	<div id = "drop-content">
		<a href = "Index.cfm">
			<i class = "fa fa-home" aria-hidden = "true">
			</i>
			Home
		</a>
		<a href = "profile.cfm">
			<i class = "fa fa-user" aria-hidden = "true">
			</i>
			Profile
		</a>
		<a href = "dashboard.cfm">
			<i class = "fa fa-tachometer" aria-hidden = "true">
			</i>
			Dashboard
		</a>
		<a href = "course.cfm">
			<i class = "fa fa-graduation-cap" aria-hidden = "true">
			</i>
			Courses
		</a>
	</div>
	<div class = "bannerSection">
		<img src = "images/banners-03.jpg" width = "100%">
	</div>
	<div class = "regBtn">
	</div>
<cfelse>
	<div class = "bannerSection">
		<img src = "images/banners-02.png" width = "100%">
	</div>
	<div class = "regBtn">
		<a class = "btnLink registration" href = "#signUp">
			Register Now
		</a>
	</div>
</cfif>
<!---Footer--->
<cfinclude template = "includes/footer.cfm" />
