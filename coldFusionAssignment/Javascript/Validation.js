let obj = (function() {
      let _userNameValid = false;
      let _emailValid = false;
      let _oldPasswordValid = false;
      let _courseNameValid = false;
      let _dateValid = false;
      return {
       get userNameValid() { return _userNameValid; },
       set userNameValid(_val) { _userNameValid=_val; },
       get emailValid() { return _emailValid; },
       set emailValid(_val) { _emailValid=_val; },
       set logInPassword(_val) { _logInPassword=_val; },
       get oldPasswordValid() { return _oldPasswordValid; },
       set oldPasswordValid(_val) { _oldPasswordValid=_val; },
       get courseNameValid() { return _courseNameValid; },
       set courseNameValid(_val) { _courseNameValid=_val; },
       get dateValid() { return _dateValid; },
       set dateValid(_val) { _dateValid=_val; }
      }  
  })();

$(document).ready(function()
{	
	window.history.replaceState(null,null,window.location.href);																				//disabling all input field in profile page
	(function(){
		
		$("#updateForm,#updateCourseForm, #updateAssignment").find('[name]').each(function(index,value){
			$(this).prop("readonly",true);
			$(this).removeClass( "form-control");
			$(this).addClass("remove");
			$(this).css("border","0");	
		})
	})();
	
	$(".notify").on("click",function(){
		notification($(this),"components/dbOperation.cfc?method=updateStatus");
	});
			
	(function(){
		$("#updateCourseForm").find('[name]').each(function(index,value){
			$(this).prop("readonly",true);
			$(this).addClass("remove");
			$(this).css("border","0");	
		})
	})();
	
	$(".editBtnInput").on("click",function(){
		formId= $(this).closest('form').attr('id');
		$('#'+formId).find('[name]').each(function(index,value){
			$(this).prop("readonly",false);
			$(this).removeClass("remove");
			$(this).css("border","0");	
		})
	})
	
	//on click it make input type editabe
	$(".editBtn").on("click",function(){
		formId= $(this).closest('form').attr('id');
		$('#'+formId).find('[name]').each(function(index,value){
			$(this).prop("readonly",false);
			$(this).addClass( "form-control");
			$(this).removeClass("remove");
			$(this).css("border","0");	
		})
	})
	
	$("#dob").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: '0d',
		minDate: ""
	});
	$("#startDate").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: '1m',
		minDate: "0d"
	});
	
	
	$("#showMsg").on("click",function show()
	{
		if($('.notification').css('display') == 'none'){ 
			   $(".notification").show(); 
			} else { 
			   $('.notification').hide(); 
			}
	});
	
	$(".datePicker").on("change",function(){
		validateFields($(this));
	});
																					//clear all form data if modal is closed
	$('.modal').on('hidden.bs.modal', function(){
		if( $(this).find('form')[0]!=undefined)
		{
		    $(this).find('form')[0].reset();
		    $(this).find('.errMsg').html("");
		    $(this).find('.form-control').css("border","1px solid #ccc");
		    $(this).find('.input-control').css("border","1px solid #ccc");
		}
		$(this).find('p').html("");
	});
	
	$(".webPage").css('display', 'block');            								//display web page if js is enable
	
	$(document).on("blur",".input-control",function(){     							//validating logIn page on blur       
         validateFields($(this));
         });
    $(document).on("blur",".form-control",function(){								//validating registration form on blur
         validateAllFields($(this));
        });
    																				//client side validation at the time of sign up
	$("#signUpSubmitButton").on("click",checkValid);
																					//client side validation at the time of login
	$("#logInSubmitbtn").on("click",checkValidField);
																					//client side validation at the time of adding assignment
	$("#addAssignmentBtn").on("click",function(){
		obj.userNameValid=true;
		obj.emailValid=true;
		return checkValid();
	});
	
	$("#upload").on("click",function(){
		obj.userNameValid=true;
		obj.emailValid=true;
		return  checkValid();
	});
	
	$("#addReviewBtn").on("click",checkValidField);
																					//client side validation at the time of adding course
	$("#addCourseBtn").on("click",checkValidField);
	
	$("#updateCourseBtn").on("click",checkValidField)
		
																					//server side validation at the time of sign up
    $("#signUpForm").on("submit",function() {			
    	formName = $(this).attr('id');
    	data = {userNameValid: obj.userNameValid,emailValid: obj.emailValid, formName: formName, oldPasswordValid:obj.oldPasswordValid};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateUser",data);	
     	});
    																				//client side validation when updating profile
	$("#updateBtn").on("click",function(){
		obj.userNameValid = true;
		obj.emailValid = true;
		return checkValid;
	});
																					//server side validation at the time of updating form
    $("#updateForm").on("submit",function(){
		obj.userNameValid = true;
		obj.emailValid = true;
    	formName = $(this).attr('id');
    	data = {userNameValid: obj.userNameValid,emailValid: obj.emailValid, formName: formName, oldPasswordValid:obj.oldPasswordValid};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateUser",data);
    	});
    																				//server side validation at the time of adding assignment
    $("#addAssignmentForm").on("submit",function(){
		formName = $(this).attr('id');
    	data = {formName: formName};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateAssignment",data);
	});
    																				//validate assignment on server side and insert into database 
    $("#uploadAssignmentForm").on("submit",function(){
		formName = $(this).attr('id');
    	data = {formName: formName};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateUpload",data);
	});
    																				//this function is used to add review and validate server side review fields
    $("#addReviewForm").on("submit",function(){
		formName = $(this).attr('id');
    	data = {formName: formName};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateReview",data);
	});
    															//server side validation at the time of adding  course
    $("#addCourseForm").on("submit",function(){
    	formName = $(this).attr('id');
    	data = {courseNameValid: obj.courseNameValid, formName: formName};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateCourse",data);
    });
    
    $("#updateCourseForm").on("submit",function(){
    	formName = $(this).attr('id');
    	data = {courseNameValid: obj.courseNameValid, formName: formName};
    	return validateFieldAjax($(this),"components/validationReg.cfc?method=validateCourse",data);
    });
    											//validating dates
    $("#endDate").datepicker({
		onSelect: function () {
            var dateFrom = $('#startDate').datepicker('getDate');
            var dateTo = $(this).datepicker('getDate');
            if (dateFrom >= dateTo) {
                obj.dateValid = true;
                validateAllFields($('#endDate'));
            }
            else
            {
            	obj.dateValid = false;
                validateAllFields($('#endDate'));
            }
        },
		changeMonth: true,
		changeYear: true,
		maxDate: '2m',
		minDate: "0d"
		
	});																	
														//used to update the course
	$(".update").on("click", function(){
		var id = $(this).parents('tr').find(".hidden").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=getDetails",
 			dataType:'json',
 			data:{courseId:id} ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("#updateCourseForm input[name="+key+"]").val(value);
				$("#updateCourseForm textarea[name="+key+"]").val(value);
 				
 				});
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		})	
	});
																	// user for enrolling student
	$(".enroll").on("click", function(){
		var id = $(this).parents('tr').find(".hidden").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=getDetails",
 			dataType:'json',
 			data:{courseId:id} ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("#courseEnrollForm input[name="+key+"]").val(value);
 				});
 				if(result.RESPONSE)
 				{
	 				swal({
	 			        title: "Are you sure?",
	 			        text: "want to enroll in this course",
	 			        icon: "info",
	 			        buttons: true
	 			    }).then( function(isConfirm){
	 			        if (isConfirm)
	 			        	$("#yes").click(); 	
	 			    });
 				}
 				else
 				{
 					swal("Oops","Something went worong please try again","error");
 				}
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		}); 		
	});
	
	$(".courseDetails").on("click", function(){
		var id = $(this).parents('tr').find(".hidden").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=getCourseDetails",
 			dataType:'json',
 			data:{id:id} ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("."+key).append(value);
 				});		
 				if(result.ERROR)
 				{
 					swal("Oops","Something went worong please try again","error");
 				}
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		}); 		
	});
	
	$(".reviewDetails").on("click", function(){
		var id = $(this).parents('tr').find(".hidden").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=getReviewDetails",
 			dataType:'json',
 			data:{id:id} ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("."+key).append(value);
 				});
 				if(result.ERROR)
 				{
 					swal("Oops","Something went worong please try again","error");
 				}
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		}); 		
	});
	
	$(".assignmentDetails").on("click", function(){
		var id = $(this).parents('tr').find(".hiddenAId").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=getAssignmentDetails",
 			dataType:'json',
 			data:{id:id} ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("."+key).append(value);
 				});
 				if(result.ERROR)
 				{
 					swal("Oops","Something went worong please try again","error");
 				}
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		}); 		
	});
	
	// for upoad assignment by student
	$(".upload").on("click", function(){
		var aid = $(this).parents('tr').find(".hiddenAId").text();
		var eid = $(this).parents('tr').find(".hiddenEId").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=enrollmentDetails",
 			dataType:'json',
 			data:{assignmentId:aid, enrollmentId:eid} ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("#uploadAssignmentForm input[name="+key+"]").val(value);
 				});
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		}); 		
	});
	                                                     //this function is used to add review for students
	$(".addReview").on("click", function(){
		var submissionId = $(this).parents('tr').find(".hidden").text();
		var assignmentId = $(this).parents('tr').find(".hiddenAid").text();
		$.ajax({
 			type: 'POST',
 			url: "components/validationReg.cfc?method=submissionDetails",
 			dataType:'json',
 			data:{submissionId: submissionId, assignmentId: assignmentId } ,
 			cache:false,
 			success: function(result){
 				$.each(result,function(key,value){
				$("#addReviewForm input[name="+key+"]").val(value);
 				});
 			},	
	     	error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	 
 		}); 		
	});
	
	$(".withdraw").on("click", function(){                       //this function is used to withdaw the course
		var id = $(this).parents('tr').find(".hidden").text();
		swal({
		        title: "Are you sure?",
		        text: "want to delete in this course",
		        icon: "info",
		        buttons: true
		       
		    }).then( function(isConfirm){
		        if (isConfirm)
		        {
			        $.ajax({
			     		type: 'POST',
			     		url: "components/dataInsertion.cfc?method=withdrawCourse",
			     		dataType:'json',
			     		data:{courseId:id} ,
			     		cache:false,
			     		success: function(result){
			     				if(result.SUCCESS)
			     					{
			     					swal({
			     						  title: "Success!",
			     						  text: "Course deleted ",
			     						  icon: "success",
			     						  button: "Ok",
			     						}).then(function(){
			     							location.reload(true);
			     						})
			     					}
			     				else
			     					{
			     					swal({
			     						  title: "Error!",
			     						  text: "Please try later ",
			     						  icon: "error",
			     						  button: "Ok",
			     						})
			     					}
			     			},	
			    	     error: function(jqXHR, status, err ){
			    	     	ajaxFailure(jqXHR, status, err ,'Index.cfm')
			    	     }	 
			     	});
		       	}	
		    })
		 		
	});
	
	$(".error").on("click",function(){
		
			swal({
		         title: "Sorry",
		         text: "Please log in to continue",
		         icon: "warning"
		 }).then(
		         function () {
		        	
		                 window.location.href = "Index.cfm#logIn";
		         });	 
	});
																					//drop down menu in index page
	$("#buttonShow").on("click",function show()
	{
		if($('#drop-content').css('display') == 'none'){ 
			   $('#drop-content').show(); 
			} else { 
			   $('#drop-content').hide(); 
			}
	});
    
    $("#uploadFile").on("change",function(){
    	if(this.files.length>0)
    	{
    		var size = this.files[0].size/ 1024 /1024;
    		console.log(this.files[0].size);
    	}
    	validatePdf($(this),size);
    });
    $("#filterForm").on("submit",function(e){
    	 window.history.replaceState(null,null,window.location.href)
    })
    $("#courseEnrollForm").on("submit",function(){
    	formName = $(this).attr('id');
    	data = {formName: formName};
    	return validateFieldAjax($(this),"components/dataInsertion.cfc?method=enrollment",data);
    });
    																				// validating username exist or not
     	$("#signUpForm input[name=inputUserName]").on("change",function(){  
     		inputUserName=$("#signUpForm input[name=inputUserName]").val();
     		$.ajax({
     			type: 'POST',
     			url: "components/validationReg.cfc?method=validateUserName",
     			dataType:'json',
     			data:{inputUserName:inputUserName} ,
     			cache:false,
     			success: function(result){   				
     				obj.userNameValid=result.error;	
     				if(result.response)
     				{
     					swal("oops","Somthing went wrong please try again","error");
     				}
     				else if(!obj.userNameValid)
 					{
 						$("#errUserName").html("User name already exist !!");
 					}
     				else if(inputUserName.trim() != "")
 					{
     					$("form input[name=inputUserName]").css("border","1px solid #c0c0c0");
     					$("#errUserName").html("");
 					}
     				
     			},
     				error: function(jqXHR, status, err ){
    		     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
    		     	}	
     		})
     	});
     	
     																						//validation email is exist or not
     	$("#signUpForm input[name=inputEmail]").on("change",function(){  
     		 var emailRegex = /^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$/;
     		inputEmail = $("#signUpForm input[name=inputEmail]").val();
     		$.ajax({
     			type: 'POST',
     			url: "components/validationReg.cfc?method=validateEmail",
     			dataType:'json',
     			data:{inputEmail:inputEmail} ,
     			cache:false,
     			success: function(result){
     				obj.emailValid=result.error;
     				if(result.response)
     				{
     					swal("oops","Somthing went wrong please try again","error");
     				}	
     				else if(!obj.emailValid)
 					{
     					$("#errEmail").html("email already Exist !!");
 					}
     				else if(inputEmail.trim() != "" && emailRegex.test(inputEmail.trim()))
 					{
     					console.log(!emailRegex.test(inputEmail.trim()));
     					$("form input[name=inputEmail]").css("border","1px solid #c0c0c0");
     					$("#errEmail").html("");
 					}	
     			},	
		     	error: function(jqXHR, status, err ){
		     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
		     	}	 
     		})
     	});
     	
 																					//validating user name exist for log in or not
     	$("#addCourseForm input[name=inputCourseName]").on("change",function(){ 
     		inputCourseName = $("#addCourseForm input[name=inputCourseName]").val();
     		$.ajax({
     			type: 'POST',
     			url: "components/validationReg.cfc?method=validateCourseName",
     			dataType:'json',
     			data:{inputCourseName: inputCourseName} ,
     			cache:false,
     			success: function(result){
     				obj.courseNameValid = result.error;
     				
     				if(result.response)
     				{
     					swal("Oops","something went wrong plrase try again","error")
     				}
     				else if(!obj.courseNameValid)
 					{
     					$("#errCourseName").html("course name alredy exist !!");
 					}
     				else if(inputCourseName.trim() != "")
     				{
     					$("form input[name=inputCourseName]").css("border","1px solid #c0c0c0");
     					$("#errCourseName").html("");
     				}
     				
     			},
     			error: function(jqXHR, status, err ){
		     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
		     	}
     		})
     	});
     	
     	$("#updateCourseForm input[name=INPUTCOURSENAME]").on("change",function(){ 
     		inputCourseName = $("#updateCourseForm input[name=INPUTCOURSENAME]").val();
     		$.ajax({
     			type: 'POST',
     			url: "components/validationReg.cfc?method=validateCourseName",
     			dataType:'json',
     			data:{inputCourseName:inputCourseName} ,
     			cache:false,
     			success: function(result){
     				obj.courseNameValid=result.error;
     				
     				if(result.response)
     				{
     					swal("Oops","Somthing went worng please try later","error")
     				}
     				else if(!obj.courseNameValid)
 					{
     					$("#errCourseName").html("course name alredy exist !!");
 					}
     				else if(inputCourseName.trim() != "")
     				{
     					$("form input[name=INPUTCOURSENAME]").css("border","1px solid #c0c0c0");
     					$("#errCourseName").html("");
     				}
     				
     			},
     			error: function(jqXHR, status, err ){
		     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
		     	}
     		})
     	});
     	
     																							//validating user name exist for log in or not																					//validating user name exist for log in or not
     	$("#updateForm input[name=oldPassword]").on("blur",function(){ 
     		inputPassword = $("#updateForm input[name=oldPassword]").val();
     		$.ajax({
     			type: 'POST',
     			url: "components/validationReg.cfc?method=validateOldPassword",
     			dataType:'json',
     			data:{inputPassword:inputPassword} ,
     			cache:false,
     			success: function(result){
     				obj.oldPasswordValid=result.error;
     				if(result.response)
     				{
     					swal("Oops","Something went wrong please try agin later","error");
     				}
     			},
     			error: function(jqXHR, status, err ){
		     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
		     	}
     		})
     	});
     	
     																							//validating login for by server and client side validation
     	$("#logInForm").on("submit",function(event) { 		
 	     	var frmObj = $(this);
 	     	data={};
 	     	frmObj.find('[name]').each(function(index,value){
 	     	var frmObj=$(this),
 	     	name = frmObj.attr('name'),
 	     	value=frmObj.val();
 	     	data[name]=value;
 	     	});
	     	userLogIn(data);
	     	
	     	return false;  	
     	});
 })
    	
 																								//validate all field when button is clicked
function checkValid()
{
	
    var errors = [obj.userNameValid,obj.emailValid];
    $(".form-control").each(function(){
       errors.push(validateAllFields($(this)));
    });
    $(".form-control-file").each(function(){
    	if(this.files.length > 0)
    	{
    		var size = this.files[0].size/ 1024 /1024;
    	}
        errors.push(validatePdf($(this),size));
     });
   
    for(var index in errors)               //iterating error from array
    {
        if(errors[index] == false)
        {
            return false;                 //returning false if any error in input
        }        
    }   
    return true;   
}

																								//validating all field on blur and on button click
function validateAllFields(element)
{
    var nameRegex = /^[a-zA-Z]+[']?[a-zA-Z]+$/;
    var emailRegex = /^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$/;
    var phoneRegex = /^[^0-1][0-9]{9}$/;
    var passRegex = /^(?=.*[0-9])(?=.*[&*$#%@])[a-zA-z0-9&*$#%@]{8,15}$/;
    var formId = $(element).closest('form').attr('id');
    var value = element.val();
    if(element.val() == "" && element.attr('name') != "inputMiddleName")
    {	
    	if((element.attr('name') == "inputPassword" ||element.attr('name') == "inputConfirmPassword" ) && formId == "updateForm")
    		{
    		element.css("border","1px solid #c0c0c0");
            element.siblings('.errMsg').html("");
            return true;
    		}
    	else
    		{
    		element.css("border","1px solid #FF0B0B");
            element.siblings('.errMsg').html("Please provide a valid "+element.attr("placeholder") );
            return false;
    		}
    }
    else if((element.attr('name') == "inputFirstName" || element.attr('name') == "inputLastName") &&
                (!nameRegex.test(value) || value.trim().length > 30 ))                        
    {
        return pushError(element);
    }
    else if(element.attr('name') == 'inputEmail' && (!emailRegex.test(value) || value.trim().length > 40 || value.trim().length < 5))                
    {
    	return pushError(element);
    }
    else if(element.attr('name') == 'inputEmail' && !obj.emailValid)                
    {
    	return pushError(element,"already Exist !!");
    }
    else if(element.attr('name') == 'inputUserName' && (value.trim().length > 20 || value.trim().length < 1))         
    {
    	return pushError(element);
    } 
    else if(element.attr('name') == 'inputUserName' && !obj.userNameValid )         
    {
    	return pushError(element,"already exist !!");
    } 
    else if(element.attr('name') == 'inputPassword' && !passRegex.test(value) && formId != "updateForm")         
    {
        return pushError(element);
    }
     else if(element.attr('name') == 'inputConfirmPassword' &&  value != $("#inPass").val() && formId != "updateForm" )         
    {
          return pushError(element);   
    }
    else if(element.attr('name') == 'inputPassword' && !passRegex.test(value) && formId == "updateForm")         
    {
        return pushError(element);
    }
    else if(element.attr('name') == 'inputConfirmPassword' &&  value != $("#newPass").val() && formId == "updateForm" )         
    {
          return pushError(element);   
    }
    else if(element.attr('name') == 'oldPassword' &&  !passRegex.test(value) && !obj.oldPasswordValid )         
    {
          return pushError(element);   
    }
    else if(element.attr('name') == 'inputAssignmentName' &&  (value.trim().length > 50 || value.trim().length < 1))         
    {
          return pushError(element);   
    }
    else if(element.attr('name') == 'description' &&  (value.trim().length > 500 || value.trim().length < 1))         
    {
          return pushError(element);   
    }
    else if(element.attr('name') == 'inputEndDate' && obj.dateValid)
    {
    	return pushError($('#endDate'), "must be after start date")
    }
    else
    {
    	element.css("border","1px solid #cfcfcf");
        element.siblings('.errMsg').html("");
        return true;
    }
}
																									//it return false when any validating condition is not satisfied
function pushError(element,msg)
{
    if(msg == undefined)
    	{
    	element.css("border","1px solid #FF0B0B");
	    element.siblings('.errMsg').html("Please provide a valid "+element.attr("placeholder"));
	    return false;
    	}
    else
    	{
    	element.css("border","1px solid #FF0B0B");
    	element.siblings('.errMsg').html(element.attr("placeholder")+" "+msg);
        return false;
    	}
}

																									//validate middle name which is non- mandatory 
function validateMiddleName(element)
{
    var name_regex = /^[a-zA-Z]+[']?[a-zA-Z]+$/;
    var value = element.val().trim();
    if(value != "" && !name_regex.test(value))
    {
        return pushError(element);
    }
    else 
    {
    	element.css("border","1px solid #c0c0c0");
        element.siblings('.errMsg').html("");
        return true;
    }
}
																									//it validation form field on click
function checkValidField()
{
	var errors=[];
    $(".input-control").each(function(){
       errors.push(validateFields($(this)));
    });
   console.log(errors);
    for(var index in errors)               //iterating error from array
    {
        if(errors[index] == false)
        {
            return false;                 //returning false if any error in input
        }
    } 
    return true;
}
																									//it validate all the fields on click and on blur 
function validateFields(element)
{
	 var emailRegex = /^[\w$][\w+_\.-]*[^\.@][@][\w+_\.-]+[^\.-][.][a-zA-z]+$/;
	 var passRegex = /^(?=.*[0-9])(?=.*[&*$#%@])[a-zA-z0-9&*$#%@]{8,15}$/;
	 var marksRegex = /^([1-9][0-9]?|100)$/;
	 var value = element.val();
	 
	    if(value == "")
	    {
	        element.css("border","1px solid #FF0B0B");
	        element.siblings('.errMsg').html("please provide a valid "+element.attr("placeholder"));
	        return false;
	    }
	    else if(element.attr('name') == 'inputLogInPassword' && !passRegex.test(value))
	    {
	    	element.css("border","1px solid #FF0B0B");
	        element.siblings('.errMsg').html("please provide a valid "+element.attr("placeholder"));
	        return false;
	    }
	    else if((element.attr('name') == 'inputCourseName' || element.attr('name') == 'INPUTCOURSENAME') && (value.trim().length > 50 || value.trim().length < 1) )
	    {
	    	return pushError(element); 
	    }
	    else if((element.attr('name') == 'inputCourseName' || element.attr('name') == 'INPUTCOURSENAME') && !obj.courseNameValid  )
	    {
	    	return pushError(element,"already exist !!"); 
	    }
	    else if((element.attr('name') == 'inputCourseDescription' || element.attr('name') == 'INPUTCOURSEDESCRIPTION') && (value.trim().length > 500 || value.trim().length < 1))
	    {
	    	return pushError(element); 
	    }
	    else if(element.attr('name') == 'inputMarks'  && !marksRegex.test(value))
	    {
	    	return pushError(element); 
	    }
	    else if(element.attr('name') == 'inputReview'  && (value.trim().length > 500 || value.trim().length < 1))
	    {
	    	return pushError(element); 
	    }
	    else
	    {
	    	element.css("border","1px solid #c0c0c0");
	        element.siblings('.errMsg').html("");
	        return true;
	    }
}

function validatePdf(element,size)
{
	if(size)
	{
		var file = element.val();
		var len = file.length;
		var fileSize = size.toFixed(2);
		var ext = file.slice(len - 4, len);
		if(ext.toUpperCase() != ".PDF" )
		{
			return pushError(element,"file only pdf allowed ");
		}
		else if(fileSize > 5) 
		{
			return pushError(element,"file is too large");
		}
		else if(fileSize < 0.02)
		{
			return pushError(element,"file is too small ")
		}
		else
		{
			element.css("border","1px solid #c0c0c0");
		    element.siblings('.errMsg').html("");
		    return true;
		}
	}
	else
	{
		return pushError(element,"a valid file")
	}
}
																									// if ajax is failure then popup apper
function ajaxFailure(jqXHR, status, err ,loc)
{
	swal({
        title: "Sorry!",
        text: "Something went wrong please try again",
        icon: "warning"
}).then(function () {
	   $(".logInInput").val("");
             window.location.href = loc;
     });	  
}

																												//it is used to show pop up on success
function sucessFunction(msg,loc)
{
	swal({
         title: "Thank you",
         text: msg,
         icon: "success"
 }).then(
         function () {
                 window.location.href = loc;
         });	 
}

function validateFieldAjax(frmObj,ajaxUrl,data)
{
	formName = data.formName;
	$(".disable").prop( "disabled", false );
	var form = $('#'+formName)[0];
    var formData = new FormData(form);
    $.each(data,function(key,value){
    	formData.append(key, value);
    });
    $("#btnSubmit").prop("disabled", true);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: ajaxUrl,
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
 			success: function(result){  
 				try {
 						var errorMsg = $.parseJSON(result);
 						$.each(errorMsg,function(key,value){
 		 					$("."+key).html(value);
		 					$("."+key).prev().css("border","1px solid #FF0B0B");
 						})
 						 console.log(errorMsg);    		   //on successfull validation showing popup message
	 				if(formName == "updateForm" && errorMsg.SUCCESS)
	 				{		
	 	 		     	     		$(".alert-success").fadeIn('fast').delay(3000).fadeOut('fast');
	 	 		     	     		$("#updateForm").find('[name]').each(function(index,value){
	 	 		     	   			$(this).prop("readonly",true);
	 	 		   					$(this).removeClass( "form-control");
	 	 		     	     		$(this).addClass("remove");
	 	 		     	   			$(this).css("border","0"); 
	 	 		     	     		})
	 	 		     	     		location.reload(true); //loads from server
	 	 		     }
	 	     		else if(errorMsg.SUCCESS && obj.userNameValid && obj.emailValid && formName == "signUpForm")
	 	     		{
	 	     			$(".form-control").val("");
	 	   				 sucessFunction("Registraion success","Index.cfm#logIn")        
	 	     		}
	 	     		else if(errorMsg.SUCCESS && obj.courseNameValid && formName == "addCourseForm")
	 	     		{
	 	     			$(".input-control").val("");
	 	   				 sucessFunction("Course Added success","course.cfm")        
	 	     		}
	 	     		else if(errorMsg.SUCCESS && obj.courseNameValid && formName == "updateCourseForm")
	 	     		{
	 	     			$(".input-control").val("");
		   				 sucessFunction("Course updated success","mycourse.cfm")  
	 	     		}
	 	     		else if(errorMsg.SUCCESS && formName == "addAssignmentForm")
	 	     		{
	 	     			$(".form-control").val("");
	 	     			sucessFunction("Assignment added success","dashboard.cfm") 
	 	     		}
	 	     		else if(errorMsg.SUCCESS && formName == "courseEnrollForm")
	 	     		{
	 	     			sucessFunction("Enrolled success","course.cfm")
	 	     		}
	 	     		else if(errorMsg.SUCCESS && formName == "uploadAssignmentForm")
	 	     		{
	 	     			sucessFunction("uploaded Sucessfully","enrolledCourses.cfm");
	 	     		}
	 	     		else if(errorMsg.SUCCESS && formName == "addReviewForm")
	 	     		{	
	 	     			sucessFunction("Review Added Sucessfully","myAssignment.cfm");
	 	     		}
	 	     		else if(!errorMsg.RESPONSE)
	 	     		{
	 	     			swal("Oops","Something went wrong please try again","error");
	 	     		}
	 			}
	 			catch(err) 
	 			{
	 				console.log(err);
	 				window.location.href="errors/customException.cfm";
				}	 			  	     		
	 		},
 			error: function(jqXHR, status, err ){
 				$(".form-control").val("");
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}	
 		}); 
 		return false;
	}

function userLogIn(data)
{
	$.ajax({
			type: 'POST',
			url: "components/userAuthenticate.cfc?method=validateLogInUser",
			dataType:'json',
			data: data,
			cache:false,
			success: function(result){
				$.each(result,function(key,value){
	 				$("."+key).html(value);
 					$("."+key).prev().css("border","1px solid #FF0B0B");
				})
				if(result.SUCCESS && result.RESPONSE)   //on successfull validation showing popup message
				{
					$(".logInInput").val("");
					window.location.href="Index.cfm";
				}
				else if(!result.SUCCESS && !result.RESPONSE)
				{
					swal("Oops","Something went wrong please try later","error");
				}
	    			 
			},
     	error: function(jqXHR, status, err ){
     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
     	}
	})
}

function notification(element,url)
{
	historyId = element.parents('span').find('.historyId').val();
	console.log(historyId);
	$.ajax({
			type: 'POST',
			url: url,
			dataType:'json',
			data:{historyId:historyId} ,
			cache:false,
			success: function(result){
				if(!result.ISCOMMIT)
				{
					swal("oops", "Something went wrong","error");
				}
			},	
     	error: function(jqXHR, status, err ){
     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
     	}	 
});
}

