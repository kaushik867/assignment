$(document).ready(function(){
	
	$.ajax({
			url: "components/dbOperation.cfc?method=pieData1",
			dataType:'json',
			cache:false,
			success: function(result){
				if(Object.keys(result).length != 0)
				{
					if(result.ERROR)
					{
						var c = $("#pie-chart1");
						var ctx = c[0].getContext("2d");
						ctx.font = "20px Arial";
						ctx.fillText("Graph could not load",50,50);
					}
					else
					{
						new Chart($("#pie-chart1"), {
				     	    type: 'pie',
				     	    data: {
				     	      labels: ["Total assignment","submitted assignment", "pending assignment or expired"],
				     	      datasets: [{
				     	        label: "Population (millions)",
				     	        backgroundColor: ["#FF632F","#3cba9f","#c45850"],
				     	        data: [result.TOTALASSIGNMENT, result.SUBMITTEDASSIGNMENT, result.NOTSUBMITTED]
				     	      }]
				     	    },
				     	    options: {
				     	      title: {
				     	        display: true,
				     	      }
				     	    }
				     	});
					}
				}
			},
			error: function(jqXHR, status, err ){
     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
     	}
		});
	
	$.ajax({
			url: "components/dbOperation.cfc?method=barData1",
			dataType:'json',
			cache:false,
			success: function(result){
				if(Object.keys(result).length != 0)
				{
					if(result.ERROR)
					{
						var c = $(".bar-chart");
						var ctx = c[0].getContext("2d");
						ctx.font = "20px Arial";
						ctx.fillText("Graph could not load",50,50);
					}
					else
					{
						new Chart($(".bar-chart"), {
				     	    type: 'bar',
				     	    data: {
				     	      labels: [result.MONTH[1], result.MONTH[2], result.MONTH[3], result.MONTH[4], result.MONTH[5]],
				     	      datasets: [
				     	        {
				     	          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
				     	          data: [result.COURSE[1], result.COURSE[2], result.COURSE[3], result.COURSE[4], result.COURSE[5]]
				     	        }]
				     	    },
				     	    options: {
				     	      legend: { display: false },
				     	      title: {
				     	        display: true,
				     	      }
				     	    }
				     	});
					}	
				}
			},
			error: function(jqXHR, status, err ){
     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
     	}
		});
})