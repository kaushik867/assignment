$.ajax({
 			url: "components/dbOperation.cfc?method=linegraph",
 			dataType:'json',
 			cache:false,
 			success: function(result){
 				if(Object.keys(result).length != 0)
 				{ 
 					if(result.ERROR)
 					{
 						var c = $("#line-chart");
						var ctx = c[0].getContext("2d");
						ctx.font = "20px Arial";
						ctx.fillText("Graph could not load",50,50);
 					}
 					else
 					{
 						new Chart($("#line-chart"), {
 	 			     		  type: 'line',
 	 			     		  data: {
 	 			     		    labels: [result.ASGNNAME[1],result.ASGNNAME[2],result.ASGNNAME[3],result.ASGNNAME[4],result.ASGNNAME[5]
 	 			     		    		,result.ASGNNAME[6],result.ASGNNAME[7],result.ASGNNAME[8],result.ASGNNAME[9],result.ASGNNAME[10]],
 	 			     		    datasets: [{ 
 	 			     		        data: [result.MYMARKS[1],result.MYMARKS[2],result.MYMARKS[3],result.MYMARKS[4],result.MYMARKS[5]
 	 			     		        	  ,result.MYMARKS[6],result.MYMARKS[7],result.MYMARKS[8],result.MYMARKS[9],result.MYMARKS[10]],
 	 			     		        label: "My marks",
 	 			     		        borderColor: "#3e95cd",
 	 			     		        fill: false
 	 			     		      }, { 
 	 			     		        data: [result.MAXMARKS[1],result.MAXMARKS[2],result.MAXMARKS[3],result.MAXMARKS[4],result.MAXMARKS[5]
 	 			     		        	  ,result.MAXMARKS[6],result.MAXMARKS[7],result.MAXMARKS[8],result.MAXMARKS[9],result.MAXMARKS[10]],
 	 			     		        label: "Highest marks",
 	 			     		        borderColor: "#8e5ea2",
 	 			     		        fill: false
 	 			     		      }
 	 			     		    ]
 	 			     		  },
 	 			     		  options: {
 	 			     		    title: {
 	 			     		      display: true,
 	 			     		      text: 'Marks comparison last 10 assignments'
 	 			     		    }
 	 			     		  }
 	 			     		});
 					}
 				}
 			},
 			error: function(jqXHR, status, err ){
	     		ajaxFailure(jqXHR, status, err ,'Index.cfm')
	     	}
 		});
	