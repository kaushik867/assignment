<!---
  --- Application
  --- -----------
  ---
  --- author: kaushik
  --- date:   3/24/20
  --->
<cfcomponent output="false">
	<cfset this.name = 'eAssignment' >
	<cfset this.applicationTimeOut = createTimespan(1,0,0,0) />
	<cfset this.datasource = 'Demoproject' />
	<cfset this.sessionManagement = true />
	<cfset this.sessionTimeout = createTimespan(0,0,30,0) />
	<cfset this.loginStorage = "session" />

	<cffunction name="onApplicationStart" returntype="boolean" hint="Fires when the application is first created">
		<cfset application.validationReg = createObject("component",'DD.coldFusionAssignment.components.validationReg') />
		<cfset application.dbOperation = createobject("component",'DD.coldFusionAssignment.components.dbOperation') />
		<cfset application.dataInsertion = createobject("component",'DD.coldFusionAssignment.components.dataInsertion') />
		<cfreturn true />
	</cffunction>

	<cffunction name="onRequestStart" returntype="boolean" hint="Fires at first part of page processing">
		<cfargument name="targetPage" type="string" required="true" />
		<cfset LOCAL.nonLoggedInPages = ["/DD/coldFusionAssignment/index.cfm", "/DD/coldFusionAssignment/course.cfm",
										"/DD/coldFusionAssignment/components/userAuthenticate.cfc", "/DD/coldFusionAssignment/components/validationReg.cfc",
										"/DD/coldFusionAssignment/about.cfm","/DD/coldFusionAssignment/errors/customException.cfm",
										"/DD/coldFusionAssignment/errors/missingTemplate.cfm"]/>
		 <cfif !structKeyExists(session,'stLoggedInUser')>
		 <cfset LOCAL.redirect = true />
	        <cfloop array="#local.nonLoggedInPages#" index="pages">
	            <cfif pages EQ arguments.targetPage>
	                <cfset LOCAL.redirect = false />
	            </cfif>
	        </cfloop>
	        <cfif LOCAL.redirect>
	           <cflocation url="../coldFusionAssignment/index.cfm" />
	        </cfif>
    	</cfif>
		<!---handle some special URL parameters--->
		<cfif isDefined('url.restartApp')>
			<cfset this.onApplicationStart() />
		</cfif>

		<cfif structKeyExists(URL,'logout')>
			<cfset createObject("component",'DD.coldFusionAssignment.components.userAuthenticate').doLogout() />
		</cfif>

		<cfreturn true />
	</cffunction>

	<cffunction name="onMissingTemplate" returntype="boolean" hint="Fires when request page is not found">
		<cfargument name="targetPage" type="string" required=true/>
		<!--- Using a try block to catch errors. --->
		<cftry>
		<!--- Log all errors. --->
			 <cflog type="error" text="Missing template: #arguments.targetPage#">
			<!--- Display an error message. --->
			<cfinclude  template="../coldFusionAssignment/errors/missingTemplate.cfm">
			<cfreturn true />
			<cfcatch>
				<cfreturn false />
			</cfcatch>
		</cftry>
	</cffunction>

	<cffunction name="onError" returntype="void" hint="Fires when an exception occures that is not caught by a try/catch">
		<cfargument name="exception" type="string" required=true/>
		<cfargument name="eventname" type="string" required=true/>
		<!--- Using a try block to catch errors. --->
	<cftry>
		<cfsavecontent variable="errortext">
		<cfoutput>
		Message :#arguments.exception.message#
		details :#arguments.exception.detail#
		template :#arguments.exception.tagContext[1].template#
		line :#arguments.exception.tagContext[1].line#
		</cfoutput>
		</cfsavecontent>
			<!--- Log all errors. --->
		<cflog type="error" text=#errortext# />
		<cflocation url = "../coldFusionAssignment/errors/customException.cfm">
	<cfcatch>
		<cflog text = "message: #cfcatch.message# template: #cfcatch.TagContext[1].template# line no: #cfcatch.TagContext[1].line#">
		<cflocation url = "../coldFusionAssignment/errors/customException.cfm">
	</cfcatch>
	</cftry>
	</cffunction>


</cfcomponent>

