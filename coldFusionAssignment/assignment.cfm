<!---
	--- assignment.cfm
	--- --------------
	---
	--- author: kaushik
	--- date:   4/26/20
	--->
<cfif !structKeyExists(session,'stLoggedInUser') AND  isUserInRole('student')>
	<cflocation url = "Index.cfm##logIn">
</cfif>
<cfparam name = "url.CourseId" default = "1">
<cfif url.courseId NEQ "">
	<cftry>
		<cfset  VARIABLES.myAssignment = application.dbOperation.assignmentDetails(url.courseId)>
		<cfset  VARIABLES.user = application.dbOperation.uploadAssignmentUser(VARIABLES.myAssignment.assignmentId) />
		<cfset  VARIABLES.enrollment = application.dbOperation.enrollmentDetails(url.courseId,session.stLoggedInUser.userId)>
		<cfset  VARIABLES.submit = application.dbOperation.checkSubmission(VARIABLES.myAssignment.assignmentId,VARIABLES.enrollment.enrollmentId) />
	<cfcatch>
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
	</cftry>
</cfif>

<cfparam name = "pageNum" default = "1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 7>
<cfset VARIABLES.startRow = min( ( pageNum-1 ) * VARIABLES.maxRows+1, max( myAssignment.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.myAssignment.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.myAssignment.recordCount/7 )>
<cf_header>
<div class = "welcome">
	<cfinclude template = "includes/welcome.cfm" />
</div>
<div class="wrapper">
	<cfinclude template= "includes/sidebar.cfm" />
	<div class = "contentBox">
		<div class = "showCourse" >
			<cfif structKeyExists(url,"courseId") AND VARIABLES.myAssignment.recordCount EQ 0>
				<h2>
					Please try Again there is No availabe assignment
				<h2>
			<cfelseif VARIABLES.myAssignment.recordCount EQ 0 >
				<h2>
					Please try Again there is No availabe course
				<h2>
			<cfelseif structkeyexists(VARIABLES.myAssignment,'error') OR structkeyexists(VARIABLES.enrollment,'error') OR structkeyexists(VARIABLES.user,'error') OR structkeyexists(VARIABLES.submit,'error') >
				<h2>
					Something went wrong please try again later
				<h2>
			<cfelse>
				<cf_heading headingTitle = "ASSIGNMENTS">
				<div class = "panel panel-default">
					<div class = "panel-body" >
						<table class = "table table-striped">
							<thead>
								<cfoutput>
									<tr>
										<th align="left"> #VARIABLES.myAssignment.name# </th>
										<th align="left"> Description </th>
										<th> updated on </th>
										<th> Start on </th>
										<th> End on </th>
										<th> Upload by </th>
									</tr>
							</thead>
							</cfoutput>
							<cfoutput query = "VARIABLES.myAssignment" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#">
								<cfset  VARIABLES.user = application.dbOperation.uploadAssignmentUser(myAssignment.assignmentId) />
								<cfset  VARIABLES.submit = application.dbOperation.checkSubmission(myAssignment.assignmentId,enrollment.enrollmentId) />
								<tr>
									<td align = "left"> #left(VARIABLES.myAssignment.assignmentName,20)# </td>
									<td align = "left"> #left(VARIABLES.myAssignment.asgnDesc,20)# </td>
									<td align = "right" class = "hiddenAId"> #VARIABLES.myAssignment.assignmentId# </td>
									<td align = "right" class = "hiddenEId"> #VARIABLES.enrollment.enrollmentId# </td>
									<td align = "left"> #dateformat(VARIABLES.myAssignment.lastUpdate,'dd-mmm-yy')# </td>
									<td align = "left"> #dateformat(VARIABLES.myAssignment.startDate,'dd-mmm-yy')# </td>
									<td align = "left"> #dateformat(VARIABLES.myAssignment.endDate,'dd-mmm-yy')# </td>
									<td align = "left"> #VARIABLES.user.firstName# #VARIABLES.user.lastName# </td>
									<td align="left"><a href="" class="assignmentDetails" data-target = "##assignmentDetails" data-toggle = "modal">More Details</a></td>
									<td align = "right">
										<cfif VARIABLES.submit.recordCount NEQ 0>
											<button type = "button" class = "btn btn-success">
												uploaded
											</button>
											</a>
										<cfelseif datecompare(dateFormat(now(),'mm-dd-yyyy'),dateFormat(VARIABLES.myAssignment.endDate,'mm-dd-yyyy')) GTE 1 >
											<button type = "button" class = "btn btn-danger">
												Expired
											</button>
											</a>
										<cfelseif datecompare(dateFormat(now(),'mm-dd-yyyy'),dateFormat(VARIABLES.myAssignment.startDate,'mm-dd-yyyy')) GTE 0 >
											<a class = "btn download" href = "download.cfm?assignmentId=#VARIABLES.myAssignment.assignmentId#">
												<button type = "button" class = "btn btn-primary">
													download
												</button>
											</a>
											<a class = "btn upload" data-target = "##uploadAssignmentDialog" data-toggle="modal">
												<button type = "button" class = "btn btn-primary">
													upload
												</button>
											</a>
										<cfelseif datecompare(dateFormat(now(),'mm-dd-yyyy'),dateFormat(VARIABLES.myAssignment.startDate,'mm-dd-yyyy')) LT 0 >
											<button type = "button" class = "btn btn-warning">
												Availabe soon
											</button>
											</a>
										</cfif>
									</td>
								</tr>
							</cfoutput>
						</table>
						<!-- end form-horizontal -->
					</div>
				</div>
				<!-- end size -->
			</cfif>
			<div class = "pagination">
				<cfoutput>
					<cfif url.pageNum GT "1" >
						<a href="?pageNum=#url.pageNum-1#">
							<button type="button" class="btn btn-secondary">
								<<--previous
							</button>
						</a>
					</cfif>
					<cfif url.pageNum LT totalPages>
						<a href="?pageNum=#url.pageNum+1#">
							<button type="button" class="btn btn-secondary">
								Next-->
							</button>
						</a>
					</cfif>
				</cfoutput>
			</div>
		</div>
	</div>
</div>
<cftry>
	<cfinclude template="includes/uploadAssignment.cfm">
<cfcatch>
	<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
</cfcatch>
</cftry>

<cfinclude template = "includes/assignmentDetails.cfm" />
<cfinclude template = "includes/footer.cfm" />
