<!---
  --- enrolledcourse.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/27/20
  --->
<cfif !structKeyExists(session,'stLoggedInUser') OR  !isUserInRole('student')>
	<cflocation url = "Index.cfm">
</cfif>
<cftry>
	<cfset  VARIABLES.myCourses = application.dbOperation.enrolledCourses(session.stLoggedInUser.userId)>
	<cfif structKeyExists(form,'filterBtn') AND !structkeyExists(VARIABLES.myCourses,'error')>
		<cfset  VARIABLES.myCourses = application.dbOperation.enrolledCourses(session.stLoggedInUser.userId,form.selectItem)>
	</cfif>
	<cfcatch type = "any">
		<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
	</cfcatch>
</cftry>

<cfparam name = "pageNum" default = "1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 7>
<cfset VARIABLES.startRow = min( ( pageNum-1 ) * VARIABLES.maxRows+1, max( VARIABLES.myCourses.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.myCourses.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.myCourses.recordCount/7 )>

<!---header--->
<cf_header>
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<cfinclude template = "includes/welcome.cfm" />
	</div>
<div class = "wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
		<div class = "contentBox">
</cfif>
<cfinclude template = "includes/filter.cfm">

		<div class = "showCourse" >
			<cfif VARIABLES.myCourses.recordCount EQ 0>
				<h2> You are not enroll in any course
			<cfelseif structkeyExists(VARIABLES.myCourses,'error')>
				<h2>
					Something went wrong please try later
				</h2>
			<cfelse>
				<cf_heading headingTitle = "ENROLLMENTS">
				<div class = "panel panel-default">
					<div class = "panel-body" >
						<table class = "table table-striped">
							<thead>
								<tr>
									<th align = "left"> Course Name </th>
									<th align = "left"> Description </th>
									<th align = "right"> last Update On </th>
									<th align = "right"> Created By </th>
								</tr>
							</thead>
							<cfoutput query = "VARIABLES.myCourses" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#">
							<!--- <cfset VARIABLES.submit = application.dbOperation.checkExist(myCourses.courseId) > --->
							<tr>
								<td class = "clickable" align = "left" onclick = "window.location.href='assignment.cfm?courseId=#myCourses.courseId#'"> #left(VARIABLES.myCourses.name,20)# </td>
								<td class = "clickable" align = "left" onclick = "window.location.href='assignment.cfm?courseId=#myCourses.courseId#'"> #left(VARIABLES.myCourses.description,20)# </td>
								<td class = "hidden">#VARIABLES.myCourses.courseId#</td>
								<td class = "clickable" align = "left" onclick = "window.location.href='assignment.cfm?courseId=#myCourses.courseId#'"> #dateFormat(VARIABLES.myCourses.createdTime,'dd-mmm-yy')# #timeFormat(VARIABLES.myCourses.createdTime,'hh:MM')# </td>
								<td class = "clickable" align = "left" onclick = "window.location.href='assignment.cfm?courseId=#myCourses.courseId#'"> Created By: #VARIABLES.myCourses.firstName# #VARIABLES.myCourses.lastName# </td>
								<td align="left"><a href="" class="courseDetails" data-target = "##details" data-toggle = "modal">More Details</a></td>
								<td align = "right">
								<button type = "button" class = "btn btn-primary withdraw">
								withdraw</button></td>
							</tr>
							</cfoutput>
						</table>
							<!-- end form-horizontal -->
					</div>
				</div>
			</cfif>
					<!-- end size -->

			<div class = "pagination">
				<cfoutput>
					<cfif url.pageNum GT "1" >
						<a href = "?pageNum=#url.pageNum-1#"><button type = "button" class = "btn btn-secondary"> <<--previous </button></a>
					</cfif>
					<cfif url.pageNum LT totalPages>
						<a href = "?pageNum=#url.pageNum+1#"><button type = "button" class = "btn btn-secondary"> Next--> </button></a>
					</cfif>
				</cfoutput>
			</div>
		</div>
		<cfif structKeyExists(session,'stLoggedInUser')>
		</div>
</div>
</cfif>
<cfinclude template = "includes/courseDetails.cfm">
<cfinclude template = "includes/footer.cfm" />