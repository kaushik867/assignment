<!---
  --- submitAssignment.cfm
  --- --------------
  ---
  --- author: kaushik
  --- date:   4/27/20
  --->

<cfif !structKeyExists(session,'stLoggedInUser') OR !isUserInRole('teacher')>
	<cflocation url = "Index.cfm">
</cfif>
<cftry>
<cfset VARIABLES.subAssignment = application.dbOperation.submitAssignment(url.assignmentId)>
<cfcatch type = "any">
	<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
</cfcatch>
</cftry>

<cfparam name = "pageNum" default = "1">
<cfset url.pageNum = pageNum />
<cfset VARIABLES.maxRows = 7>
<cfset VARIABLES.startRow = min( ( pageNum-1 ) * VARIABLES.maxRows+1, max( VARIABLES.subAssignment.recordCount,1 ) )>
<cfset VARIABLES.totalPages = ceiling( VARIABLES.subAssignment.recordCount/VARIABLES.maxRows )>
<cfset VARIABLES.loopCount = round( VARIABLES.subAssignment.recordCount/7 )>

<!---header--->
<cf_header>
<cfif structKeyExists(session,'stLoggedInUser')>
	<div class = "welcome">
		<cfinclude template = "includes/welcome.cfm" />
	</div>
<div class = "wrapper">
	<cfinclude template = "includes/sidebar.cfm" />
		<div class = "contentBox">
</cfif>
<cfinclude template = "includes/filter.cfm">
		<div class = "showCourse" >
			<cfif VARIABLES.subAssignment.recordCount EQ 0>
				<h2> You did not have any submitted assignment yet</h2>
			<cfelseif structKeyExists(VARIABLES.subAssignment,'error')>
				<h2> Something went wrong please try again later</h2>
			<cfelse>
				<div class = "panel panel-default">
					<div class = "panel-body" >
						<table class = "table table-striped">
							<thead>
								<tr>
									<th> Assignment name </th>
									<th> submitted by </th>
									<th> Submitted On </th>
								</tr>
							</thead>
							<cfoutput query = "VARIABLES.subAssignment" startrow = "#VARIABLES.startRow#" maxrows = "#VARIABLES.maxRows#">
							<tr>
								<td align = "left"> #VARIABLES.subAssignment.name# </td>
								<td align = "left"> #VARIABLES.subAssignment.firstName# #subAssignment.lastName# </td>
								<td align = "left" width="20%"> #dateFormat(VARIABLES.subAssignment.submissionTime,'dd-mmm-yy')# #timeFormat(VARIABLES.subAssignment.submissionTime,'hh:MM')# </td>
								<td class = "hidden"> #VARIABLES.subAssignment.submissionId# </td>
								<td class = "hiddenAid"> #VARIABLES.subAssignment.assignmentId# </td>
								<td align = "right">
								 <a class = "btn" href = "download.cfm?submissionId=#VARIABLES.subAssignment.submissionId#"><button type = "button" class = "btn btn-primary">
 									download </button></a>
								 <cfif #VARIABLES.subAssignment.marks# EQ "">
									<a class = "btn addReview" data-target = "##addReviewDialog" data-toggle = "modal"><button type = "button" class = "btn btn-primary">
									Add review </button></a>
								<cfelse>
									<button type = "button" class = "btn btn-success">
									Done </button></a>
								</cfif>
								</td>
							</tr>
							</cfoutput>
						</table>
							<!-- end form-horizontal -->
					</div>
				</div>
			</cfif>
				<!-- end size -->
			<div class = "pagination">
				<cfoutput>
					<cfif url.pageNum GT "1" >
						<a href = "?pageNum=#url.pageNum-1#"><button type = "button" class = "btn btn-secondary"> <<--previous </button></a>
					</cfif>
					<cfif url.pageNum LT totalPages>
						<a href = "?pageNum=#url.pageNum+1#"><button type = "button" class = "btn btn-secondary"> Next--> </button></a>
					</cfif>
				</cfoutput>
			</div>
		</div>
		<cfif structKeyExists(session,'stLoggedInUser')>
		</div>
	</div>
</cfif>
<cftry>
	<cfinclude template = "includes/addReview.cfm" />
<cfcatch>
	<cflog text = "message: #cfcatch.message# template: #cfcatch.tagContext[1].template# line no: #cfcatch.tagContext[1].line#" />
</cfcatch>
</cftry>


<cfinclude template = "includes/footer.cfm" />